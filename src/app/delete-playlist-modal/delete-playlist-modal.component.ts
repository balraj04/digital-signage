import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-delete-playlist-modal',
  templateUrl: './delete-playlist-modal.component.html',
  styleUrls: ['./delete-playlist-modal.component.css']
})
export class DeletePlaylistModalComponent implements OnInit {

  constructor(
  	public dialogRef: MatDialogRef<DeletePlaylistModalComponent>) { }

  ngOnInit() {
  }

  onBtnClick(flag): void {
    this.dialogRef.close(flag);
  }
}
