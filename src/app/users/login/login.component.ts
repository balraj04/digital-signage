import { Component, OnInit, Output, Injectable, ViewChild, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { AuthenticationService } from '../../services/index';
import { environment } from '../../../environments/environment';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public LoginForm: FormGroup;
  errorMsg: any;
  returnUrl: string;

  // Snackbar Posittion
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  constructor(
    private router: Router,
    private route: ActivatedRoute,
  	private authenticationService: AuthenticationService,
  	private fb: FormBuilder,
    private snackBar: MatSnackBar
  	) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) { 
        this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.LoginForm = this.fb.group({
      username: this.fb.control('',[Validators.required]),
      password: this.fb.control('',[Validators.required])
    });
  }

  Login() {
    
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '';

    let LoginInfo = {username:this.LoginForm.get('username').value,password:this.LoginForm.get('password').value}
    this.authenticationService.Login(LoginInfo)
     .subscribe(response =>
       {
         if(response.status == 200){
             this.snackBar.open('Logged in successfully !', 'Success', {
                duration: 4000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
                panelClass: ['cust-snackbar', 'submitted'],
              });
             this.router.navigate([this.returnUrl]);
           
         }
         else {
           this.snackBar.open('Invalid Username/Password', 'Error', {
              duration: 4000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
              panelClass: ['cust-snackbar', 'not-submitted'],
           });
         }
       },
       error => this.errorMsg = error);
  }

}
