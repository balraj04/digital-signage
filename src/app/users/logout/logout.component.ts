import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from "@angular/router";
import { AuthenticationService } from '../../services/index';
import { LogoutModalComponent } from '../../users/logout/logout-modal/logout-modal.component';
@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  @ViewChild('openModal') openModal:ElementRef;
  private previousUrl: any = "/";
  constructor(
  	private router: Router,
  	private authenticationService: AuthenticationService) { }
  ngOnInit() {
  	this.authenticationService.Logout().subscribe(response =>{
  		if(response.logout===true)
  		{
  			this.router.navigate(['login']);  			
  		}
  	});
  }
}
