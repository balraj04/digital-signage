import { Component, OnInit, Inject } from '@angular/core';
import { Router } from "@angular/router";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, PageEvent } from '@angular/material';
import { AuthenticationService } from '../../../services/index';

@Component({
  selector: 'app-logout-modal',
  templateUrl: './logout-modal.component.html',
  styleUrls: ['./logout-modal.component.css']
})
export class LogoutModalComponent implements OnInit {

  constructor(
  	private router: Router,
  	private authenticationService: AuthenticationService,
  	public dialogRef: MatDialogRef<LogoutModalComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogdata: any) { }

  ngOnInit() {}

  Logout(logtype:any) {
  	
  	if(logtype == 'Y')
  	{
      this.dialogRef.close();
  		this.authenticationService.Logout().subscribe(response =>{
	  		if(response.logout===true)
	  		{
	  			this.router.navigate(['login']);  			
	  		}
	  	});
  	}
  	else
  	{
      this.dialogRef.close();       
  	}
  	
  }

}
