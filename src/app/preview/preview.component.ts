import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, PageEvent } from '@angular/material';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';
import { Router, ActivatedRoute } from "@angular/router";
import { CommonBridgeService, PrintService, SharedService } from './../services/index';
import { PlaylistInfo, TempWeatherInfo, TempAnnouncementInfo } from './../interfaces/index';
import { environment } from '../../environments/environment';
import { PreviewDialogComponent } from './preview-dialog/preview-dialog.component';

const LIMIT = environment.limit;

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.css']
})
export class PreviewComponent implements OnInit {

  public previewPlaylistForm: FormGroup;

  title: string;
  private selectedType: number = 2;
  private selectedPlaylist: any;
  playlists: [];
  errorMsg: any;
  playlistInfo: PlaylistInfo;

  // MatPaginator Inputs
  length: number = 0;
  pageSize = LIMIT;
  playlistPage: number = 1;
  slidePage: number = 1;

  // MatPaginator Output
  pageEvent: PageEvent;

  tempWeather: TempWeatherInfo = {
    city_name: '',
    temp: 0,
    logo: '',
    footerColor: '',
    footerBgcolor: '',
    custom_city_name: ''
  };

  tempAnnouncement: TempAnnouncementInfo = {
    text: '',
    scrolldelay: 0,
    announcementColor: '',
    announcementBgcolor: ''
  };

  // Snackbar Posittion
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private printService: PrintService,
    private commonBridgeService: CommonBridgeService,
    public dialog: MatDialog,
    protected sharedService: SharedService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private snackBar: MatSnackBar) {
  	  this.title = 'Preview';
  }

  ngOnInit() {
    this.selectedPlaylist = (this.activatedRoute.snapshot.paramMap.get('playlist')) ? +this.activatedRoute.snapshot.paramMap.get('playlist') : '';
    let d = new Date();
    this.previewPlaylistForm = this.fb.group({
        prev_dt: this.fb.control(d,[Validators.required]),
        playlist_id: this.fb.control(this.selectedPlaylist, [Validators.required]),
        prev_type: this.fb.control('',[Validators.required])
    });
    this.getPlaylists(this.playlistPage, LIMIT);
  }

  getNext(event: PageEvent) {
    this.playlists = [];
    this.playlistPage = event.pageIndex + 1;
    this.getPlaylists(this.playlistPage, LIMIT);
  }

  getPlaylists(page, limit){
    this.commonBridgeService.getPlaylist()
      .subscribe(response =>
                            {
                              if(response.message == 'success'){
                                this.playlists = response.playlists.data;
                                this.length = response.playlists.total;
                              }
                            },
                error => this.errorMsg = error);
  }

  getPlaylistsInfo(form_data){
    let sel_dt = this.sharedService.dateToTimestamp(form_data.prev_dt);
    this.commonBridgeService.getPlaylistInfo(+form_data.playlist_id, sel_dt)
      .subscribe(response =>
        {
          if(response.status == 200){
            this.playlistInfo = response;
            if(this.playlistInfo.playlist.slides.length){
              this.configComponent(form_data.prev_type);
            }
            else{
              this.snackBar.open('No slides available', 'Error', {
                duration: 4000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
                panelClass: ['cust-snackbar', 'not-submitted'],
              });
            }
          }
        },
      error => this.errorMsg = error);
  }

  configComponent(prev_type){
    if(this.playlistInfo.playlist.footer_text == 'Y' || this.playlistInfo.playlist.scroll_announcement == 'Y'){
      this.getFooterSettings(prev_type);
    }
    else{
      this.loadComponent(prev_type);
    }
  }

  loadComponent(prev_type){
    let infos = this.printService.getComponents(this.playlistInfo.playlist.slides);
    if(infos.length){
      for (var i = infos.length - 1; i >= 0; i--) {
        this.tempAnnouncement.text = (this.playlistInfo.playlist.scroll_announcement == 'Y') ? this.playlistInfo.playlist.announcements : '';
        infos[i].content_data['community_footer'] = {'status': this.playlistInfo.playlist.footer_text, 'content': this.tempWeather};
        infos[i].content_data['scroll_announcement'] = {'status': this.playlistInfo.playlist.scroll_announcement, 'content': this.tempAnnouncement};
      }
      const dialogRef = this.dialog.open(PreviewDialogComponent, {
        width: '50%',
        data: { infos: infos, prev_type: prev_type },
        panelClass: 'print_modal'
      });
    }
    else{
      this.snackBar.open('No slides available or temporarily hidden', 'Error', {
        duration: 4000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        panelClass: ['cust-snackbar', 'not-submitted'],
      });
    }
  }

  getFooterSettings(prev_type){
    this.commonBridgeService.getSettings()
      .subscribe(response =>
                            {
                              if(response.status == 200){
                                if(response.settings[0]){
                                  if(response.settings[0].footer_logo){
                                    this.tempWeather.logo = response.settings[0].footer_logo;
                                  }
                                  if(response.settings[0].footerColor){
                                    this.tempWeather.footerColor = response.settings[0].footerColor;
                                  }
                                  if(response.settings[0].footerBgcolor){
                                    this.tempWeather.footerBgcolor = response.settings[0].footerBgcolor;
                                  }
                                  if(response.settings[0].announcementColor){
                                    this.tempAnnouncement.announcementColor = response.settings[0].announcementColor;
                                  }
                                  if(response.settings[0].announcementBgcolor){
                                    this.tempAnnouncement.announcementBgcolor = response.settings[0].announcementBgcolor;
                                  }
                                  if(response.settings[0].scrolldelay){
                                    this.tempAnnouncement.scrolldelay = response.settings[0].scrolldelay;
                                  }
                                  if(response.settings[0].city_name){
                                    this.tempWeather.custom_city_name = response.settings[0].city_name;
                                  }
                                  if(response.settings[0].app_id && response.settings[0].city_id){
                                    this.getWeatherInfo(response.settings[0].app_id, response.settings[0].city_id, prev_type, response.settings[0].weatherUnit);
                                  }
                                  else{
                                    this.loadComponent(prev_type);
                                  }
                                }
                              }
                            },
                error => this.errorMsg = error);
  }

  getWeatherInfo(app_id, city_id, prev_type, unit){
    this.commonBridgeService.getWeatherInfo(app_id, city_id, unit)
      .subscribe(response =>
                            {
                              if(response.cod == 200){
                                this.tempWeather.temp = response.main.temp;
                                this.tempWeather.city_name = response.name;
                                this.loadComponent(prev_type);
                              }
                            },
                error => {
                  this.loadComponent(prev_type);
                  this.errorMsg = error;
                });
  }

  onSubmit(form_data): void {
    this.getPlaylistsInfo(form_data);
  }

  onChangeDT(type): void {
    let d = new Date();
    if(type == 'tomorrow'){
      d.setDate(d.getDate() + 1)
    }
    this.previewPlaylistForm.controls['prev_dt'].setValue(d);
  }

  setType(e: number): void
  {
    this.selectedType = e;
  }

  setPlaylist(e: number): void
  {
    this.selectedPlaylist = e;
  }

  issetPlaylist(): boolean   
  {  
    if (!this.selectedPlaylist) {
      return false;
    }
    return true;
  }

  trackByPlaylist(index: number, playlist: any): number {
    return playlist.playlist_id;
  }
}