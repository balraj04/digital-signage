import { Component, OnInit, Inject, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TemplatesComponent, PrintPreviewItems } from './../../interfaces/index';
import { PrintService } from './../../services/index';
import { environment } from '../../../environments/environment';
declare var $ : any;

@Component({
  selector: 'app-preview-dialog',
  templateUrl: './preview-dialog.component.html',
  styleUrls: ['./preview-dialog.component.css']
})
export class PreviewDialogComponent implements OnInit {

	@ViewChild('processContainer', { read: ViewContainerRef }) container;
  @ViewChild('announcementContainer', { read: ViewContainerRef }) announcementContainer;
  @ViewChild('footerContainer', { read: ViewContainerRef }) footerContainer;

	previewData: PrintPreviewItems;
  currentAdIndex = 0;
  interval: any;
  mills: number = 1000;
  screenHeight: any;
  tempSlides: any[];
  tempThumb: [];
  scroll_announcement: any;
  community_footer: any;
  mediaUrl = environment.mediaUrl;
  dateTemp : any = Math.random();
  public blurDefault = environment.blurDefault;
  public noimageDefault = environment.noimageDefault;

  // Footer and Announcement Flags
  hasFooter: boolean = false;
  hasAnnouncement: boolean = false;

  editorBlock: any;
  editorHeight: any;
  editorWidth: any;
  scaleBlock: any;
  scaleCount:any;

  showCustomPrev = false;
  custFooterValue: any;

  constructor(
    private printService: PrintService,
    private resolver: ComponentFactoryResolver,
    public dialogRef: MatDialogRef<PreviewDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: PrintPreviewItems) {
  	this.previewData = data;
  }

  ngOnInit() {
    this.screenHeight = window.innerHeight;
    if(this.previewData.prev_type == 1){
      this.scroll_announcement = this.previewData.infos[0].content_data.scroll_announcement;
      this.community_footer = this.previewData.infos[0].content_data.community_footer;
      this.loadCarouselComponent();
    } else if (this.previewData.prev_type == 3) {
      this.loadThumbs();
    } else {
      this.showCustomPrev = true;
      this.loadComponent();
    }
  }

  ngAfterViewInit() {
    this.scaleBlock  = document.getElementsByClassName('print_modal1');

    this.editorWidth = window.innerWidth;
    this.editorHeight = window.innerHeight;
    this.scaleCount = Math.min(this.editorWidth / 640, this.editorHeight / 360);
    
    let translateCount = (( this.scaleCount - 1 ) * 50);

    if(this.scaleCount)
    {
      this.scaleBlock[0].style.transform = 'translate(-50%, -50%) scale('+this.scaleCount+')';
    }
  }

  loadCarouselComponent(){
    if(this.scroll_announcement.status == 'Y'){
      this.hasAnnouncement = true;
      const template_arr: Array<{template_id: string, content: any, status: string}> = [{'template_id': 'tmpl-announcement', 'content': this.scroll_announcement.content, 'status': 'A'}];
      let infos = this.printService.getComponents(template_arr);
      if(infos.length){
        let announceItem = infos[0];
        const factory = this.resolver.resolveComponentFactory(announceItem.component);
        this.announcementContainer.clear();
        let componentRef = this.announcementContainer.createComponent(factory);
        let tempContent = (announceItem.content_data.content) ? announceItem.content_data.content : {};
        let content = tempContent;
        (<any>componentRef.instance).scroll_text = content;
      }
    }

    if(this.community_footer.status == 'Y'){
      this.hasFooter = true;
      const template_arr: Array<{template_id: string, content: any, status: string}> = [{'template_id': 'tmpl-footer', 'content': this.community_footer.content, 'status': 'A'}];
      let infos = this.printService.getComponents(template_arr);
      if(infos.length){
        let footerItem = infos[0];
        const factory = this.resolver.resolveComponentFactory(footerItem.component);
        this.footerContainer.clear();
        let componentRef = this.footerContainer.createComponent(factory);
        let tempContent = (footerItem.content_data.content) ? footerItem.content_data.content : {};
        let content = tempContent;
        (<any>componentRef.instance).community_footer = content;
      }
    }
    
    this.tempSlides = this.previewData.infos.map(x => { 
      x.content_data.duration = x.content_data.duration * 1000;
      return x.content_data
    });

    setTimeout(()=>{
      this.startCarousal();
    }, 1000);
  }

  loadThumbs(){
    this.tempThumb = this.previewData.infos.map(x => {
      return x.content_data.cover_image
    });
  }

  loadComponent(){
    this.container.clear();
    for (let adItem of this.previewData.infos) {
      let tempContent = (adItem.content_data.content) ? {...(JSON.parse(adItem.content_data.content))} : {};
      let tmpIsValide = (adItem.content_data.content) ? 'yes' : 'no';
      if (adItem.content_data.template_id == 'tmpl-event') {
        if (tempContent.event_id || tempContent.title) {
          tmpIsValide = 'yes';
          tempContent['event_start_date'] = tempContent.event_start_date;
          tempContent['event_start_time'] = tempContent.event_start_time;
          tempContent['event_end_date'] = tempContent.event_end_date;
          tempContent['event_end_time'] = tempContent.event_end_time;
          tempContent['event_location'] = tempContent.event_location;
          tempContent['event_title'] = tempContent.event_title;
          tempContent['event_desc'] = tempContent.event_desc;
          tempContent['event_id'] = tempContent.event_id;
          tempContent['styled']= false;
        }
        else {
          tmpIsValide = 'yes';
          tempContent['event_start_date'] = adItem.content_data.event_start_date;
          tempContent['event_start_time'] = adItem.content_data.event_start_time;
          tempContent['event_end_date'] = adItem.content_data.event_end_date;
          tempContent['event_end_time'] = adItem.content_data.event_end_time;
          tempContent['event_location'] = adItem.content_data.event_location;
          tempContent['event_title'] = adItem.content_data.event_title;
          tempContent['event_desc'] = adItem.content_data.event_desc;
          tempContent['event_id'] = adItem.content_data.event_id;
          tempContent['styled']= false;
          tempContent['leftcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#fafafa", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
          tempContent['rightcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#cacaca", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
          tempContent['leftcolimage'] = "";
          tempContent['rightcolimage'] = "";
        }        
      }
      else if (adItem.content_data.template_id == 'tmpl-event-widget') {
        if (tempContent) {
          if(adItem.content_data.eventslist){
            tempContent['eventslist'] = adItem.content_data.eventslist;
          }
          if(tempContent.widget_type){
            tempContent['widget_type'] = tempContent.widget_type;
          }
          tempContent['leftcolimage'] = (adItem.content_data.content.leftcolimage != null && adItem.content_data.content.leftcolimage != undefined) ? adItem.content_data.content.leftcolimage : '';
          tempContent['rightcolimage'] = (adItem.content_data.content.rightcolimage != null && adItem.content_data.content.rightcolimage != undefined) ? adItem.content_data.content.rightcolimage : '';
        }
        else {
          tempContent = {};
          tempContent['eventslist'] = [];
          tempContent['styled']= false;
          tempContent['leftcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#fafafa", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
          tempContent['rightcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#cacaca", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
          tempContent['leftcolimage'] = "";
          tempContent['rightcolimage'] = "";
        } 
      }
      else if (adItem.content_data.template_id == 'tmpl-announcement-widget') {
        if (tempContent) {

          let announInfo = adItem.content_data.content ? JSON.parse(adItem.content_data.content) : [];
          tempContent['announcements'] = announInfo.announcements ? announInfo.announcements : [];
          tempContent['leftcolimage'] = (adItem.content_data.content.leftcolimage != null && adItem.content_data.content.leftcolimage != undefined) ? adItem.content_data.content.leftcolimage : '';
          tempContent['rightcolimage'] = (adItem.content_data.content.rightcolimage != null && adItem.content_data.content.rightcolimage != undefined) ? adItem.content_data.content.rightcolimage : '';
        }
        else {
          tempContent = {};
          tempContent['announcements'] = [];
          tempContent['styled']= false;
          tempContent['leftcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#fafafa", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
          tempContent['rightcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#cacaca", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
          tempContent['leftcolimage'] = "";
          tempContent['rightcolimage'] = "";
        } 
      }
      this.custFooterValue = adItem.content_data.community_footer.status;
      const factory = this.resolver.resolveComponentFactory(adItem.component);
      let componentRef = this.container.createComponent(factory);
      let content = tempContent;
      let is_valide = tmpIsValide;
      (<TemplatesComponent>componentRef.instance).templatedata = content;
      (<TemplatesComponent>componentRef.instance).is_valide = is_valide;
      (<TemplatesComponent>componentRef.instance).type = 'print';
      (<TemplatesComponent>componentRef.instance).community_footer = adItem.content_data.community_footer;
      (<TemplatesComponent>componentRef.instance).scroll_announcement = adItem.content_data.scroll_announcement;
    }
  }

  isSelected(i){
    return (i == this.currentAdIndex) ? 'show display_video' : 'hide hidden_video' ;
  }

  prevSlide(){
    if(this.currentAdIndex <= 0){
      this.currentAdIndex = this.tempSlides.length;
    }
    this.currentAdIndex --;
    this.startCarousal();
  }

  nextSlide(){
    let tmpcount = this.tempSlides.length;
    tmpcount --;
    if(this.currentAdIndex >= tmpcount){
      this.currentAdIndex = 0;
      this.startCarousal();
    }
    else{
      this.currentAdIndex ++;
      this.startCarousal();
    }
  }

  startCarousal(){
    let ref = this;
    setTimeout(function() {
      ref.show_hide_video();
    }, 1000);

    clearInterval(this.interval);
    let mills = this.tempSlides[this.currentAdIndex].duration;
    this.interval = setInterval(() => {
        this.nextSlide();
      }, mills);
  }

  onClose(): void {
    clearInterval(this.interval);
    this.dialogRef.close();
  }

  trackBySlide(index: number, playlist: any): number {
    return playlist.slide_id;
  }

  ngOnDestroy(){
    clearInterval(this.interval);
  }

  show_hide_video() {
     $('div.slide-item.hidden_video').find('video').map(function () {
      this.pause();
     });
     $('div.slide-item.display_video').find('video').map(function () {
      this.currentTime = 0;
      this.play();
     });
     $('div.slide-item.hidden_video').find('iframe.youtube_video_play').map(function () {        
      let url = $(this).attr('src');
      if(url.indexOf("autoplay")>0){
        let new_url = url.substring(0, url.indexOf("?"));
        $(this).attr('src', new_url);
      }
      $(this).attr('allow', '');
     });

     $('div.slide-item.display_video').find('iframe.youtube_video_play').each(function(){         
       let url = $(this).attr('src');
        if(url.indexOf("autoplay")>0){
          let new_url = url.substring(0, url.indexOf("?"));
          $(this).attr('src', new_url);
          $(this).attr('src', url+'?autoplay=1');
          $(this).attr('allow', 'autoplay');
        }
        else{
          $(this).attr('src', url+'?autoplay=1');
          $(this).attr('allow', 'autoplay');
        }
      });

     $('div.slide-item.display_video').find('iframe.ppt_pdf_play').map(function () {
       let url = $(this).attr('src');
       $(this).attr('src', url);
     });
   }

}