import { Component, OnInit, Input } from '@angular/core';
import { TempWeatherInfo, TempAnnouncementInfo, PreviewAnnouncementInfo } from '../../interfaces/index';
import { CommonBridgeService, PrintService, SharedService } from '../../services/index';
import { environment } from './../../../environments/environment';
import { ISubscription } from "rxjs/Subscription";

@Component({
  selector: 'app-preview-footer',
  templateUrl: './preview-footer.component.html',
  styleUrls: ['./preview-footer.component.css']
})

export class PreviewFooterComponent implements OnInit {

	private subscription: ISubscription;

	@Input() footerStatus: any = "T";
	errorMsg: any;
	now: number;
	default_logo: string = environment.spDefaultLogo;
	mediaUrl = environment.mediaUrl;
	dateTemp : any = Math.random();
	interval: any;
	tempWeather: TempWeatherInfo = {
		city_name: '',
		temp: 0,
		logo: '',
		footerColor: '',
		footerBgcolor: '',
		custom_city_name: ''
	};

	tempAnnouncement: PreviewAnnouncementInfo = {
		text: '',
		scrolldelay: 0,
		announcementColor: '',
		announcementBgcolor: '',
		announcements:''
	};
    constructor(private sharedService: SharedService,private commonBridgeService: CommonBridgeService) {
		this.subscription = this.sharedService.FooterDisplay.subscribe(
        (data: any) => { 
        	this.footerStatus = data || 'T';
        });
        this.now = Date.now();
	    this.interval = setInterval(() => {
	      this.now = Date.now();
	    }, 10000);
    }

	ngOnInit() {
		this.getFooterSettings();
	}
	getFooterSettings(){
	    this.commonBridgeService.getSettings(true)
	      .subscribe(response =>
	                            {
	                              if(response.status == 200){
	                                if(response.settings[0]){
	                                  if(response.settings[0].footer_logo){
	                                    this.tempWeather.logo = response.settings[0].footer_logo;
	                                  }
	                                  if(response.settings[0].footerColor){
	                                    this.tempWeather.footerColor = response.settings[0].footerColor;
	                                  }
	                                  if(response.settings[0].footerBgcolor){
	                                    this.tempWeather.footerBgcolor = response.settings[0].footerBgcolor;
	                                  }
	                                  if(response.settings[0].announcementColor){
	                                    this.tempAnnouncement.announcementColor = response.settings[0].announcementColor;
	                                  }
	                                  if(response.settings[0].announcementBgcolor){
	                                    this.tempAnnouncement.announcementBgcolor = response.settings[0].announcementBgcolor;
	                                  }
	                                  if(response.settings[0].scrolldelay){
	                                    this.tempAnnouncement.scrolldelay = response.settings[0].scrolldelay;
	                                  }
	                                  if(response.settings[0].announcements){
	                                    this.tempAnnouncement.announcements = response.settings[0].announcements;
	                                  }
	                                  if(response.settings[0].city_name){
	                                    this.tempWeather.custom_city_name = response.settings[0].city_name;
	                                  }
	                                  if(response.settings[0].app_id && response.settings[0].city_id){
	                                    this.getWeatherInfo(response.settings[0].app_id, response.settings[0].city_id, response.settings[0].weatherUnit);
	                                  }
	                                  else{
	                                    
	                                  }
	                                }
	                              }
	                            },
	                error => this.errorMsg = error);
	}

	getWeatherInfo(app_id, city_id, unit){
	    this.commonBridgeService.getWeatherInfo(app_id, city_id, unit)
		    .subscribe(response => {
	          if(response.cod == 200){
	            this.tempWeather.temp = response.main.temp;
	            this.tempWeather.city_name = response.name;            
	          }
	        },
			error => {this.errorMsg = error;});
	}

	ngOnDestroy() {
  		this.subscription.unsubscribe();
		clearInterval(this.interval);
	}

}
