import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewFooterComponent } from './preview-footer.component';

describe('PreviewFooterComponent', () => {
  let component: PreviewFooterComponent;
  let fixture: ComponentFixture<PreviewFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
