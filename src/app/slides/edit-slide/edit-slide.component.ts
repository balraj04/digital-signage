import { Component, ViewChild, ViewContainerRef, OnInit, ComponentFactoryResolver, ComponentRef, ComponentFactory, ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray, NgForm } from '@angular/forms';
import { CdkDragDrop, copyArrayItem } from '@angular/cdk/drag-drop';
import { Observable } from 'rxjs/Observable';
import { ISubscription } from "rxjs/Subscription";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, PageEvent } from '@angular/material';
import { SharedService, PrintService, CommonBridgeService } from '../../services';
import { PrintDialogComponent } from './../../print/print-dialog/print-dialog.component';
import { PreviewDialogComponent } from './../../preview/preview-dialog/preview-dialog.component';
import { DragModalComponent } from './../../modal-dialog/drag-modal/drag-modal.component';
import { SaveTemplatesModalComponent } from './../../modal-dialog/save-templates-modal/save-templates-modal.component';
//import * as htmlToImage from 'html-to-image';
import * as html2canvas from 'html2canvas';
import * as APPCONSTANTS from '../../app.constants';
import { environment } from '../../../environments/environment';
import { MatOption } from '@angular/material';

@Component({
  selector: 'app-edit-slide',
  templateUrl: './edit-slide.component.html',
  styleUrls: ['./edit-slide.component.css']
})
export class EditSlideComponent implements OnInit {
  recursiverows: FormArray;

  curr_template_id : string;

  public nextphase: boolean = false;
  slide_id: number;
  idErrorMsg: any;
  private sub: any;
  slideInfo: any;

  private TEMPLATES  : any;
  public SlideForm: FormGroup;
  public playlists: any = [];
  public announcements : any =[];
  public eventslist : any = {
      todays:[],
      weekly:[]
  };
  private errorMsg: any;  
  public content: any; 
  private subscription: ISubscription;
  public tempTemplate_Id: any;
  public apiCallActive: boolean = false;
  public AnnouncementList: any = [];
  public frameArea:any=[];
  public savedTemplateData : any = [];
  min_dt: any;
  max_dt: any;

  min_dtOnEvent : any;
  max_dtOnEvent : any;

  all_days = [];
  selected_days = [];
  is_all_day: string = 'N';

  mediaUrl = environment.mediaUrl;

  /*========================
    SAVE TEMPLATE
  ========================*/
  length: number = 0;
  pageSize = environment.limit;
  page: number = 1;

  // saveTemplatePage: number = 1;
  // saveTemplateLimit: number = 5; // environment.limit;
  saveTemplateSearchkey: string ='';
  saveTemplateSortby: string ='created';

  // MatPaginator Output
  pageEvent: PageEvent;

  /*========================
    SAVE TEMPLATE
  ========================*/

  componentRef: any;
  @ViewChild('messagecontainer', { read: ViewContainerRef }) entry: ViewContainerRef;
  @ViewChild('screen') screen: ElementRef;
  @ViewChild('allSelected') private allSelected: MatOption;

  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';  

  constructor(
    private commonBridgeService: CommonBridgeService, 
    private printService: PrintService,
  	private sharedService: SharedService, 
  	private resolver: ComponentFactoryResolver, 
  	private fb: FormBuilder, 
  	private router : Router, 
  	private route : ActivatedRoute,
    public snackBar: MatSnackBar,
    private _location: Location,
    public dialog: MatDialog) {

      this.all_days = [
        {
          id: 0,
          value: 'SU'
        },
        {
          id: 1,
          value: 'MO'
        },
        {
          id: 2,
          value: 'TU'
        },
        {
          id: 3,
          value: 'WE'
        },
        {
          id: 4,
          value: 'TH'
        },
        {
          id: 5,
          value: 'FR'
        },
        {
          id: 6,
          value: 'SA'
        }
      ];
      this.recursiverows = this.fb.array([]);

      this.TEMPLATES = APPCONSTANTS.TEMPLATES_CONST;
      this.subscription = this.sharedService.SlideData.subscribe((data: any) => {
            
            this.content = data;
      });    
      
  }

  onAddRow() {
    this.recursiverows.push(this.createItemFormGroup());
    this.SlideForm.addControl('recursiverows', this.recursiverows);    
  }

  onRemoveRow(rowIndex:number){
    this.recursiverows.removeAt(rowIndex);
  }

  makeScheudlingBlank() {    
    let PostData      = this.SlideForm.value;
    if(PostData.recursiverows){
      for (var i = PostData.recursiverows.length - 1; i >= 0; i--) {
        this.recursiverows.removeAt(i);
      }
    }
  }

  makeScheudlingAdd(){
    if (this.curr_template_id != 'tmpl-event') {
      this.makeScheudlingBlank();
      let PostData  = this.SlideForm.value;
      var i = PostData.recursiverows ? PostData.recursiverows.length : 0;
      if (i === 0 || i === undefined ) {
        this.onAddRow();
      }
      this.SlideForm.get('daysOfWeek').clearValidators();
    }
  }

  makeScheudlingAuto(){
     this.makeScheudlingBlank();
     var i = 0;
     if (i === 0) {
       this.onAddRow();
     }
     this.SlideForm.get('daysOfWeek').setValidators([Validators.required]);
  }

  createItemFormGroup(): FormGroup {
    return this.fb.group({
      start_datetime: this.fb.control(null, Validators.required), 
      end_datetime: this.fb.control(null, Validators.required),
      min_datetime: this.fb.control(null), 
      max_datetime: this.fb.control(null)
    });
  }  

  ngOnInit() {   
    this.getAllSavedTemplates();
  	//Declare Form Group
  	this.SlideForm = this.fb.group({
        slide_id: this.fb.control(''),
        title: this.fb.control('',[Validators.required]),
        description: this.fb.control(''),
        playlist_id: this.fb.control([],[Validators.required]),
        template_id: this.fb.control(''),
        layout_id: this.fb.control('template_one'),
        content: this.fb.control(''),
        duration: this.fb.control('10',[Validators.required]),
        customduration: this.fb.control('1'),
        customdurationsec: this.fb.control('0'),
        scheduled: this.fb.control('N',[Validators.required]),
        schedule_type: this.fb.control('M'),
        start_datetime: this.fb.control(''),
        end_datetime: this.fb.control(''),
        schedulingdates: this.fb.array([]),
        daysOfWeek:this.fb.control([]),
        status: this.fb.control('A'),
        creator: this.fb.control('1'),
        modifier: this.fb.control('1'),
        leftcolimage: this.fb.control(''),
        rightcolimage: this.fb.control(''),
        cover_image: this.fb.control(''),
        submit: this.fb.control(false),
        showfooter: this.fb.control("T"),
      });

    this.SlideForm.addControl('recursiverows', this.recursiverows);
    this.CheckRequiredFields();

  	// Get URL ID send Call getSlideInfo API
    this.sub = this.route.params.subscribe(params => {
       this.slide_id = +params['id'];       
       // Call getSlideInfo API
       this.commonBridgeService.getSlideInfo(this.slide_id.toString())
      	.subscribe(response => {
          if(response.status === 200){            
            this.tempTemplate_Id = response.slide.template_id;
            this.curr_template_id = response.slide.template_id;
            // Create PlayList Array 
          	let playlistArr_id = [];
          	for (var i = response.slide.playlists.length - 1; i >= 0; i--) {
          		response.slide.playlists[i].playlist_id;
          		playlistArr_id = [ ...playlistArr_id, response.slide.playlists[i].playlist_id];
          	}

            let daysofweekArr = (response.slide.daysofweek) ? response.slide.daysofweek.split(",") : [];
            for(var i=0; i<daysofweekArr.length;i++) daysofweekArr[i] = +daysofweekArr[i];

            let tmpleftcolimage = '';
            let tmprightcolimage = '';
            let tempContentData = response.slide.content;

            if (response.slide.template_id == 'tmpl-event') {
              if (tempContentData) {
                tempContentData['event_start_date'] = response.slide.event_start_date ? response.slide.event_start_date : '';
                tempContentData['event_start_time'] = response.slide.event_start_time ? response.slide.event_start_time : '';
                tempContentData['event_end_date'] = response.slide.event_end_date ? response.slide.event_end_date : '';
                tempContentData['event_end_time'] = response.slide.event_end_time ? response.slide.event_end_time : '';
                tempContentData['event_location'] = response.slide.event_location ? response.slide.event_location : (response.slide.custom_location) ? response.slide.custom_location : '';
                tempContentData['event_title'] = response.slide.event_title ? response.slide.event_title : '';
                tempContentData['event_desc'] = response.slide.event_desc ? response.slide.event_desc : '';
                tempContentData['event_id'] = response.slide.event_id ? response.slide.event_id : 0;
                tempContentData['layout_id'] = response.slide.layout_id ? response.slide.layout_id : '';
                tmpleftcolimage = (response.slide.content.leftcolimage != null && response.slide.content.leftcolimage != undefined) ? response.slide.content.leftcolimage : '';
                tmprightcolimage = (response.slide.content.rightcolimage != null && response.slide.content.rightcolimage != undefined) ? response.slide.content.rightcolimage : '';
              }
              else {
                tempContentData = {};
                tempContentData['event_start_date'] = (response.slide.event_start_date !== null && response.slide.event_start_date !== undefined) ? response.slide.event_start_date : '';
                tempContentData['event_start_time'] = (response.slide.event_start_time !== null && response.slide.event_start_date !== undefined) ? response.slide.event_start_time : '';
                tempContentData['event_end_date'] = (response.slide.event_end_date !== null && response.slide.event_end_date !== undefined) ? response.slide.event_end_date : '';
                tempContentData['event_end_time'] = (response.slide.event_end_time !== null && response.slide.event_end_time !== undefined) ? response.slide.event_end_time : '';
                tempContentData['event_location'] = response.slide.event_location ? response.slide.event_location : (response.slide.custom_location) ? response.slide.custom_location : '';
                tempContentData['event_title'] = (response.slide.event_title !== null && response.slide.event_title !== undefined)  ? response.slide.event_title : '';
                tempContentData['event_desc'] = (response.slide.event_desc !== null && response.slide.event_desc !== undefined) ? response.slide.event_desc : '';
                tempContentData['layout_id'] = (response.slide.layout_id !== null && response.slide.layout_id !== undefined) ? response.slide.layout_id : '';
                tempContentData['event_id'] = (response.slide.event_id !== null && response.slide.event_id !== undefined)  ? response.slide.event_id : 0;
                tempContentData['styled']= false;
                tempContentData['leftcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#fafafa", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
                tempContentData['rightcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#cacaca", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
                tempContentData['leftcolimage'] = "";
                tempContentData['rightcolimage'] = "";

                tmpleftcolimage = '';
                tmprightcolimage = '';
              }        
            }
            else if (response.slide.template_id == 'tmpl-event-widget') {
              if (tempContentData) {
                tempContentData['eventslist'] = response.slide.eventslist ? response.slide.eventslist : [];
                tmpleftcolimage = (response.slide.content.leftcolimage != null && response.slide.content.leftcolimage != undefined) ? response.slide.content.leftcolimage : '';
                tmprightcolimage = (response.slide.content.rightcolimage != null && response.slide.content.rightcolimage != undefined) ? response.slide.content.rightcolimage : '';
              }
              else {
                tempContentData = {};
                tempContentData['eventslist'] = [];
                tempContentData['styled']= false;
                tempContentData['leftcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#fafafa", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
                tempContentData['rightcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#cacaca", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
                tempContentData['leftcolimage'] = "";
                tempContentData['rightcolimage'] = "";

                tmpleftcolimage = '';
                tmprightcolimage = '';
              } 
            }
            else if (response.slide.template_id == 'tmpl-announcement-widget') {
              if (tempContentData) {
                tempContentData['announcements'] = response.slide.announcements ? response.slide.announcements : [];
                tmpleftcolimage = (response.slide.content.leftcolimage != null && response.slide.content.leftcolimage != undefined) ? response.slide.content.leftcolimage : '';
                tmprightcolimage = (response.slide.content.rightcolimage != null && response.slide.content.rightcolimage != undefined) ? response.slide.content.rightcolimage : '';
              }
              else {
                tempContentData = {};
                tempContentData['announcements'] = [];
                tempContentData['styled']= false;
                tempContentData['leftcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#fafafa", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
                tempContentData['rightcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#cacaca", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
                tempContentData['leftcolimage'] = "";
                tempContentData['rightcolimage'] = "";

                tmpleftcolimage = '';
                tmprightcolimage = '';
              } 
            }
            else {
              tmpleftcolimage = (response.slide.content.leftcolimage != null && response.slide.content.leftcolimage != undefined) ? response.slide.content.leftcolimage : '';
              tmprightcolimage = (response.slide.content.rightcolimage != null && response.slide.content.rightcolimage != undefined) ? response.slide.content.rightcolimage : ''
            } 
            
          	this.sharedService.changeEditData(tempContentData);

            let tmpStratDate = response.slide.start_datetime == '0' ? '' : new Date(+response.slide.start_datetime);           
            let tmpEndtDate = response.slide.end_datetime == '0' ? '' : new Date(+response.slide.end_datetime);           

            let customdurationVal = response.slide.customduration ? response.slide.customduration : '';
            
            let durationVal;
            let total: number = 0;
            let minutes: number = 0;
            let seconds: number = 0;
            if (response.slide.duration == '10' || response.slide.duration == '20' || response.slide.duration == '30') {
              durationVal = response.slide.duration;
              // customDuration = '';
            }
            else {
              durationVal = 'custom';
              total = response.slide.duration;
              minutes = Math.floor(total / 60);
              seconds = total - minutes * 60;
              minutes = (minutes) ? minutes : 0;
              seconds = (seconds) ? seconds : 0;
            }
            //Declare Form Group
      			this.SlideForm = this.fb.group({
              slide_id: this.fb.control(response.slide.slide_id),
      				title: this.fb.control(response.slide.title,[Validators.required]),
      				description: this.fb.control(response.slide.description),
      				playlist_id: this.fb.control(playlistArr_id,[Validators.required]),
      				template_id: this.fb.control(response.slide.template_id),
              layout_id: this.fb.control(response.slide.layout_id ? response.slide.layout_id : this.SlideForm.get('layout_id').value),
      				content: this.fb.control(tempContentData),
      				duration: this.fb.control(durationVal,[Validators.required]),
              customduration: this.fb.control(minutes.toString()),
              customdurationsec: this.fb.control(seconds.toString()),
      				scheduled: this.fb.control(response.slide.scheduled,[Validators.required]),
              schedule_type: this.fb.control(response.slide.schedule_type),
              daysOfWeek: this.fb.control(daysofweekArr),
      				start_datetime: this.fb.control(tmpStratDate),
      				end_datetime: this.fb.control(tmpEndtDate),
      				status: this.fb.control(response.slide.status),
      				creator: this.fb.control(response.slide.creator),
      				modifier: this.fb.control(response.slide.modifier),
      				leftcolimage: this.fb.control(tmpleftcolimage),
      				rightcolimage: this.fb.control(tmprightcolimage),
      				cover_image: this.fb.control(response.slide.cover_image),
      				submit: this.fb.control(false),
              showfooter: this.fb.control("T"),
      			});
      			this.onLoadDrop(response.slide.template_id);
            this.min_dt = this.SlideForm.controls['start_datetime'].value;
            this.max_dt = this.SlideForm.controls['end_datetime'].value;

            if(response.slide.schedule_type == 'A'){
              this.createItemFormGroupTemp(tmpStratDate, tmpEndtDate);
              if(daysofweekArr){
                this.SlideForm.get('daysOfWeek').setValidators([Validators.required]);
              }
            }

            if(response.slide.schedule_type == 'M'){
              response.slide.scheduling.forEach(obj=> {
                let tmpStratDate = obj.start_datetime == '0' ? '' : new Date(+obj.start_datetime);
                let tmpEndtDate = obj.end_datetime == '0' ? '' : new Date(+obj.end_datetime);
                this.createItemFormGroupTemp(tmpStratDate, tmpEndtDate);
             })
            }
          }
        },
	    error => this.idErrorMsg = error);
    });

    //Get Playlist Dropdown Api
    this.commonBridgeService.getDroplist('playlists')
    .subscribe(response =>{
      if(response.message == 'success'){
        this.playlists = response.playlists;
      }
    },
    error => this.errorMsg = error);
  }

  getNext(event: PageEvent) {
    // this.saveTemplatePage = event.pageIndex + 1;
    // this.getSlides(this.page, LIMIT);
    this.page = event.pageIndex + 1;
    this.getAllSavedTemplates();
  }

  createItemFormGroupTemp(start, end) {
    this.recursiverows.push(
      this.fb.group({
        start_datetime: this.fb.control(start, Validators.required), 
        end_datetime: this.fb.control(end, Validators.required),
        min_datetime: this.fb.control(start), 
        max_datetime: this.fb.control(end)
      })
    );
    this.SlideForm.addControl('recursiverows', this.recursiverows);
  }

  setFooterStatus() {      
    this.sharedService.FooterDisplay.emit(this.SlideForm.controls['showfooter'].value);
  }

  updateItemFormGroup(min_dt, max_dt): FormGroup {    
    return this.fb.group({      
      start_datetime: this.fb.control(min_dt, Validators.required), 
      end_datetime: this.fb.control(max_dt, Validators.required),
      min_datetime: this.fb.control(min_dt), 
      max_datetime: this.fb.control(max_dt)
    });
  }

  changeMinDTimeOnEvent() {
    this.min_dtOnEvent = this.SlideForm.get('start_datetime').value;
  }

  changeMaxDTimeOnEvent() {
    this.max_dtOnEvent = this.SlideForm.get('end_datetime').value;
  }

  changeMinDTime(row, index){
    let min_dt = row.controls['start_datetime'].value ? row.controls['start_datetime'].value : null;
    let max_dt = row.controls['end_datetime'].value ? row.controls['end_datetime'].value : null;
    this.onRemoveRow(index);
    this.recursiverows.insert(index, this.updateItemFormGroup(min_dt, max_dt));
   
    // console.log('min_dt', min_dt);
    // console.log('this.recursiverows', this.recursiverows);
    /*if(this.SlideForm.controls['start_datetime'].value){
      this.min_dt = this.SlideForm.controls['start_datetime'].value;
    }*/
  }

  changeMaxDTime(row, index){
    let min_dt = row.controls['start_datetime'].value ? row.controls['start_datetime'].value : null;
    let max_dt = row.controls['end_datetime'].value ? row.controls['end_datetime'].value : null;
    this.onRemoveRow(index);
    this.recursiverows.insert(index, this.updateItemFormGroup(min_dt, max_dt));
    // console.log('max_dt', max_dt);
    // console.log('this.recursiverows', this.recursiverows);
    /*if(this.SlideForm.controls['end_datetime'].value){
      this.max_dt = this.SlideForm.controls['end_datetime'].value;      
    }*/
  }
  /*changeMinDTime(){
    if(this.SlideForm.controls['start_datetime'].value){
      this.min_dt = this.SlideForm.controls['start_datetime'].value;
    }
  }

  changeMaxDTime(){
    if(this.SlideForm.controls['end_datetime'].value){
      this.max_dt = this.SlideForm.controls['end_datetime'].value;
    }
  }*/

  backClicked() {
    this._location.back();
  }
  
  onLoadDrop(event) {    
  	this.entry.clear();
    this.SlideForm.controls['template_id'].setValue(event);
    const factory = this.resolver.resolveComponentFactory(this.TEMPLATES[event]);
    const componentRef = this.entry.createComponent(factory);  
  };

  drop(event: CdkDragDrop<string[]>) {
    var savedTemp = event.previousContainer.element.nativeElement.className.includes("savedTemplateClass");
    if (savedTemp) {
      let classes = event.previousContainer.element.nativeElement.className;
      var checkTempl = "tmpl-"
      var templ_id = checkTempl + classes.slice(classes.indexOf(checkTempl) + checkTempl.length);
      
      if(this.content!==undefined) //If Slide Addedd First Time
      {
        const dialogRef = this.dialog.open(DragModalComponent, {
          width: '50%'
        });

        dialogRef.afterClosed().subscribe(result => {
          if(result.dragType=='Y')
          {
            this.entry.clear();
            this.sharedService.changeEditData(null);
            this.SlideForm.controls['template_id'].setValue(templ_id);
            this.SlideForm.controls['duration'].setValue('10');
            this.SlideForm.controls['customduration'].setValue('1');
            this.SlideForm.controls['customdurationsec'].setValue('0');
            this.curr_template_id = templ_id;

            var indx = event.previousContainer.id.split('_');
            var savedIndx = parseInt(indx[1]);
            this.sharedService.changeEditData(JSON.parse(this.savedTemplateData[savedIndx].content));
            
            const factory = this.resolver.resolveComponentFactory(this.TEMPLATES[templ_id]);
            const componentRef = this.entry.createComponent(factory);
            // this.content = this.savedTemplateData[savedIndx].content;  
          }
        });
      }
      else
      {
        this.entry.clear();
        this.sharedService.changeEditData(null);
        this.SlideForm.controls['template_id'].setValue(templ_id);
        this.SlideForm.controls['duration'].setValue('10');
        this.SlideForm.controls['customduration'].setValue('1');
        this.SlideForm.controls['customdurationsec'].setValue('0');
        this.curr_template_id = templ_id;

        var indx = event.previousContainer.id.split('_');
        var savedIndx = parseInt(indx[1]);
        this.sharedService.changeEditData(JSON.parse(this.savedTemplateData[savedIndx].content));
        
        const factory = this.resolver.resolveComponentFactory(this.TEMPLATES[templ_id]);
        const componentRef = this.entry.createComponent(factory);
        // this.content = this.savedTemplateData[savedIndx].content;
      }     
    }
    else { 
        if(this.content!==undefined) //If Slide Addedd First Time
        {
          const dialogRef = this.dialog.open(DragModalComponent, {
            width: '50%'
          });

          dialogRef.afterClosed().subscribe(result => {
            if(result.dragType=='Y')
            {
              this.entry.clear();
       
              this.sharedService.changeEditData(null);
              this.SlideForm.controls['template_id'].setValue(event.previousContainer.id);
              this.SlideForm.controls['duration'].setValue('10');
              this.SlideForm.controls['customduration'].setValue('1');
              this.SlideForm.controls['customdurationsec'].setValue('0');
              this.curr_template_id = event.previousContainer.id;
              const factory = this.resolver.resolveComponentFactory(this.TEMPLATES[event.previousContainer.id]);
              const componentRef = this.entry.createComponent(factory);
            }
          });
        }
        else
        {
              this.entry.clear();
       
              this.sharedService.changeEditData(null);
              this.SlideForm.controls['template_id'].setValue(event.previousContainer.id);
              this.SlideForm.controls['duration'].setValue('10');
              this.SlideForm.controls['customduration'].setValue('1');
              this.SlideForm.controls['customdurationsec'].setValue('0');
              this.curr_template_id = event.previousContainer.id;
              const factory = this.resolver.resolveComponentFactory(this.TEMPLATES[event.previousContainer.id]);
              const componentRef = this.entry.createComponent(factory);
        } 
  	}
  }
  hideIframeArea(event:any){
    this.frameArea = document.getElementsByClassName('iframearea');
    if(this.frameArea.length>0)
    {
      this.frameArea[0].style = 'display:none;';      
    }
  }
  showIframeArea(event:any){
    this.frameArea = document.getElementsByClassName('iframearea');
    if(this.frameArea.length>0)
    {
      this.frameArea[0].style = 'display:block;';
    }
  }

  // GET SAVE TEMPLATE API CALL
  getAllSavedTemplates(){
  //   pageSize
  // page
    this.commonBridgeService.getSavedTemplateInfo(this.page, this.pageSize, this.saveTemplateSearchkey, this.saveTemplateSortby)
      .subscribe(response =>
                  {
                    if(response.status == 200){                      
                      if(response.templates){
                        // console.log('response ==>>', response.templates.data);
                        this.savedTemplateData = response.templates.data;
                        this.length = response.templates.total;
                      }
                    }
                  },
      error => this.errorMsg = error); 
  }

  // ADD SAVE TEMPLATE API CALL
  saveAsTemplateClicked(content) {

    let  ModelData = {
        template_name: ''        
    };
    let templateName = '';
    const dialogRef = this.dialog.open(SaveTemplatesModalComponent, {
      disableClose: true,
      width: '50%',
      data: ModelData,
      panelClass: 'background_modal'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){        
        if (result.templateName !== '' && result.deletetemplate != 'N') {          
            templateName = result.templateName;
            
            let savedTemplateFormsData = this.SavedTemplateFormData(content, this.curr_template_id, templateName);
           
            //Set Cover Image
            this.getCoverImage().then((response: any) => {
              savedTemplateFormsData.append('cover_image',response);
              this.commonBridgeService.saveAsTemplate(savedTemplateFormsData)
               .subscribe(response =>{
                 if(response.message == 'success'){
                   this.getAllSavedTemplates();
                   this.snackBar.open('Template saved successfully !', 'Success', {
                      duration: 4000,
                      horizontalPosition: this.horizontalPosition,
                      verticalPosition: this.verticalPosition,
                      panelClass: ['cust-snackbar', 'submitted'],
                   });
                   // this.router.navigate(['/slides']);                   
                 }
                 else {
                  this.snackBar.open('Template fail to save !', 'Error', {
                    duration: 4000, 
                    horizontalPosition: this.horizontalPosition,
                    verticalPosition: this.verticalPosition,
                    panelClass: ['cust-snackbar', 'not-submitted'],
                  });
                }
               },
               error => this.errorMsg = error);  
            });
        }
      }
    });    
  }

  //CREATE FORM DATA FOR SAVED TEMPLATE
  SavedTemplateFormData(content, template_id, templateName){
    let templateFormInfo = new FormData();    
    templateFormInfo.append('template_id', template_id);    
    templateFormInfo.append('cover_image',"");
    templateFormInfo.append('content',JSON.stringify(content));
    templateFormInfo.append('status','A');
    templateFormInfo.append('name', templateName);
    return templateFormInfo;
  }

  //COPY SLIDE DATA WITH TEMPLATE CONTENT DATA
  copySlideTemplate()
  {    
    this.apiCallActive = true;
    let ProcessData = this.SetFormData('copy');
    //Set Cover Image
    this.getCoverImage().then((response: any) => {
      ProcessData.append('cover_image',response);
      ProcessData.append('offset', new Date().getTimezoneOffset().toString());
      ProcessData.append('event_id', this.content.event_id);
      if (this.content.thumbimg) {
        ProcessData.append('thumbimg', this.content.thumbimg);
      }     
      this.commonBridgeService.saveSlide(ProcessData)
       .subscribe(response =>{
         if(response.message == 'success'){
           this.SlideForm.controls['submit'].reset(false);
           this.snackBar.open('Slide copied successfully !', 'Success', {
              duration: 4000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
              panelClass: ['cust-snackbar', 'submitted'],
           });
           this.apiCallActive = false;

           if(response.slide.slide_id){
             this.router.navigate(['/edit-slide', response.slide.slide_id]);
           }          
         }
         else {
          this.apiCallActive = false;
          this.snackBar.open('Slide fail to save !', 'Error', {
            duration: 4000, 
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            panelClass: ['cust-snackbar', 'not-submitted'],
          });
        }
       },
       error => this.errorMsg = error);     
    });
  }

  //SAVE SLIDE DATA WITH TEMPLATE CONTENT DATA
  SaveSlide($event){
   if(this.SlideForm.get('submit').value==false)
   {
     $event.preventDefault();
   }
   else
   {
     this.apiCallActive = true;
     let ProcessData = this.SetFormData();
      
      
      //Set Cover Image
      this.getCoverImage().then((response: any) => {
        ProcessData.append('cover_image',response);
        ProcessData.append('offset', new Date().getTimezoneOffset().toString());
        ProcessData.append('event_id', this.content.event_id);
        if (this.content.thumbimg) {
          ProcessData.append('thumbimg', this.content.thumbimg);
        }
       this.commonBridgeService.updateSlide(ProcessData)
       .subscribe(response =>{
         this.apiCallActive = false;
         if(response.message == 'success'){
           this.SlideForm.controls['submit'].reset(false);
          this.snackBar.open('Slide updated successfully !', 'Success', {
            duration: 4000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            panelClass: ['cust-snackbar', 'submitted'],
          });
         }
         else {        
          this.snackBar.open('Slide fail to update !', 'Error', {
            duration: 4000, 
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            panelClass: ['cust-snackbar', 'not-submitted'],
          });
        }
       },
       error => this.errorMsg = error);
      });
   }
 }

  SetFormData(type = null) {
    let slideFormInfo = new FormData();
    let PostData      = this.SlideForm.value;
    
    let leftcolimageTemp = this.content ? this.content.leftcolimage : PostData.leftcolimage;
    let rightcolimageTemp = this.content ? this.content.rightcolimage : PostData.rightcolimage;

    let scheudling: any;
    let dates = [];
    let is_all_day = (PostData.daysOfWeek.length >= this.all_days.length) ? 'Y' : 'N';
    let daysOfWeek = (PostData.schedule_type == 'A') ? PostData.daysOfWeek : '';


    if (this.curr_template_id == 'tmpl-event') {
      let startTS = this.dateToTimestamp(PostData.start_datetime);
      let endTS = this.dateToTimestamp(PostData.end_datetime);

      if (isNaN(startTS)) { startTS = 0; }
      if (isNaN(endTS)) { endTS = 0; }      

      /*let tempDates = { start_datetime: startTS, end_datetime: endTS };
      dates.push(tempDates);
      scheudling = {dates:dates};*/
      slideFormInfo.append('start_datetime', startTS.toString());
      slideFormInfo.append('end_datetime', endTS.toString());
    }
    else {
      if (PostData.scheduled == 'Y') {
        for (var i = 0; i <= PostData.recursiverows.length - 1; i++) {
          
          let startTS = this.dateToTimestamp(PostData.recursiverows[i].start_datetime);
          let endTS = this.dateToTimestamp(PostData.recursiverows[i].end_datetime);

          if (isNaN(startTS)) { startTS = 0; }
          if (isNaN(endTS)) { endTS = 0; }

          let tempDates = { start_datetime: startTS, end_datetime: endTS };

          dates.push(tempDates);      
        }
      }
      scheudling = {dates:dates};

    }

    slideFormInfo.append('daysofweek',daysOfWeek);
    slideFormInfo.append('is_all_day',is_all_day);
    slideFormInfo.append('schedulingdates', JSON.stringify(scheudling));
    
    //design slide data
    if(PostData.template_id == 'tmpl-video'){
      slideFormInfo.append('duration',this.content.duration);
    }
    else{
      // slideFormInfo.append('duration',PostData.duration);
      if (PostData.duration === 'custom') {
        // code...
        const minseconds: number = Math.floor(PostData.customduration * 60);
        const seconds: number = Math.floor(PostData.customdurationsec);
        const total: number = minseconds + seconds;
        slideFormInfo.append('duration', total.toString());
      }
      else {
        slideFormInfo.append('duration',PostData.duration);
      }
    }

    slideFormInfo.append('slide_id',PostData.slide_id);
    if(type == 'copy'){
      slideFormInfo.append('title','Copy of '+PostData.title);
    }
    else{
      slideFormInfo.append('title',PostData.title);
    }
    slideFormInfo.append('description',PostData.description);
    slideFormInfo.append('scheduled',PostData.scheduled);

    slideFormInfo.append('schedule_type',PostData.schedule_type);
    
    // slideFormInfo.append('start_datetime', startTS.toString());
    // slideFormInfo.append('end_datetime', endTS.toString());

    slideFormInfo.append('playlist_id',PostData.playlist_id);
    slideFormInfo.append('template_id',PostData.template_id);
    slideFormInfo.append('layout_id',this.content.layout_id ? this.content.layout_id : PostData.layout_id);
    slideFormInfo.append('status',PostData.status);    
    slideFormInfo.append('leftcolimage', leftcolimageTemp);
    slideFormInfo.append('rightcolimage', rightcolimageTemp);

    slideFormInfo.append('content',JSON.stringify(this.content));

    

   return slideFormInfo;
  }

  dateToTimestamp(dt){    
    let millitime = new Date(dt).setSeconds(0);
    return millitime;
  }

  //Get Screen Shot Of Generated Template
  getCoverImage(){
    return new Promise((resolve, reject) => {
      html2canvas(this.screen.nativeElement,{
        windowWidth:1000,
        windowHeight:2000,
        logging:true,
        useCORS:true,
        proxy:environment.mediaUrl+"html2canvasproxy.php"}).then(canvas => {
        let dataUrl = canvas.toDataURL();
        resolve(dataUrl);
      }).catch(e => {
        console.log(e);
      });
    });
  }

   openPrintDialog(){
    let template_id = this.SlideForm.controls['template_id'].value;
    if(template_id){
      let printInfo = this.loadComponent(template_id);
      
      const dialogRef = this.dialog.open(PrintDialogComponent, {
        width: '50%',
        data: { infos: printInfo },
        panelClass: 'print_modal'
      });
    }
    else{
      this.snackBar.open('No template available', 'Error', {
        duration: 4000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        panelClass: ['cust-snackbar', 'not-submitted'],
      });
    }
  }

  loadComponent(template_id){
    let content = this.content;
    const template_arr: Array<{template_id: string, content: any, status: string}> = [{'template_id': template_id, 'content': JSON.stringify(content), 'status': 'A'}];
    let infos = this.printService.getComponents(template_arr);

    for (var i = infos.length - 1; i >= 0; i--) {
      infos[i].content_data['community_footer'] = {'status': this.SlideForm.controls['showfooter'].value, 'content': {}};
      infos[i].content_data['scroll_announcement'] = {'status': this.SlideForm.controls['showfooter'].value, 'content': ''};
    }
    return infos;
  }

  openPreviewDialog(){
    let template_id = this.SlideForm.controls['template_id'].value;
    if(template_id){
      let printInfo = this.loadComponent(template_id);
      const dialogRef = this.dialog.open(PreviewDialogComponent, {
        width: '50%',
        data: { infos: printInfo, prev_type: 2 },
        panelClass: 'print_modal'
      });
    }
    else{
      this.snackBar.open('No template available', 'Error', {
        duration: 4000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        panelClass: ['cust-snackbar', 'not-submitted'],
      });
    }
  }

  //scheduled required on codition
  CheckRequiredFields() {
    this.SlideForm.get('scheduled').valueChanges.subscribe((scheduled: string) => {
      if (scheduled === 'Y' && this.curr_template_id == 'tmpl-event') {
        this.SlideForm.get('start_datetime').setValidators([Validators.required]);
        this.SlideForm.get('end_datetime').setValidators([Validators.required]);
      }
      else if (scheduled === 'N') {
        this.SlideForm.get('start_datetime').clearValidators();
        this.SlideForm.get('end_datetime').clearValidators();
      }
      this.SlideForm.get('start_datetime').updateValueAndValidity();
      this.SlideForm.get('end_datetime').updateValueAndValidity();
    });
  }
  //trackBy
  trackByPlaylist(index: number, playlist: any): number {
  return playlist.playlist_id;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();     
  }
  //*MAS Crescente Code
  UncheckAllDays(all){ 
      if (this.allSelected.selected){  
          this.allSelected.deselect();
          return false;
      }
      if(this.SlideForm.controls.daysOfWeek.value.length==this.all_days.length){
          this.allSelected.select();        
      }  
  }
  
  checkAllDays() {
    if (this.allSelected.selected) {  
      this.SlideForm.controls.daysOfWeek.setValue(this.all_days.map(item => item.id));
      this.allSelected.select();
    } else {
      this.SlideForm.controls.daysOfWeek.setValue([]);
    }
  }

  TrashTemplate(template_id){

      let  ModelData = {
          delete_template: 1        
      };
      let templateName = '';
      const dialogRef = this.dialog.open(SaveTemplatesModalComponent, {
        disableClose: true,
        width: '50%',
        data: ModelData,
        panelClass: 'background_modal'
      });

      dialogRef.afterClosed().subscribe(result => {

          if(result.deletetemplate == 'Y'){
              this.commonBridgeService.deleteTemplate({temp_id:template_id})
               .subscribe(response =>{
                 if(response.delete==1)
                 {
                    this.snackBar.open('Template removed successfully !', 'Success', {
                      duration: 4000,
                      horizontalPosition: this.horizontalPosition,
                      verticalPosition: this.verticalPosition,
                      panelClass: ['cust-snackbar', 'submitted'],
                    });
                    this.getAllSavedTemplates();
                 }
                 else
                 {
                   this.snackBar.open('Unable to remove template !', 'Error', {
                      duration: 4000,
                      horizontalPosition: this.horizontalPosition,
                      verticalPosition: this.verticalPosition,
                      panelClass: ['cust-snackbar', 'not-submitted'],
                    });
                 }
              });
          } 

      });  
  }

}