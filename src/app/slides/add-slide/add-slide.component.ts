import { Component, ViewChild, ViewContainerRef, OnInit, ComponentFactoryResolver, ComponentRef, ComponentFactory, ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, PageEvent } from '@angular/material';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';
import { Router, ActivatedRoute } from "@angular/router";
// import { FormControl, FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray, NgForm } from '@angular/forms';
import { CdkDragDrop, CdkDragEnter, CdkDragExit, copyArrayItem } from '@angular/cdk/drag-drop';
import { Observable } from 'rxjs/Observable';
import { SharedService, PrintService, CommonBridgeService } from '../../services';
import { PrintDialogComponent } from './../../print/print-dialog/print-dialog.component';
import { PreviewDialogComponent } from './../../preview/preview-dialog/preview-dialog.component';
import { DragModalComponent } from './../../modal-dialog/drag-modal/drag-modal.component';
import { SaveTemplatesModalComponent } from './../../modal-dialog/save-templates-modal/save-templates-modal.component';
//import * as htmlToImage from 'html-to-image';
import * as html2canvas from 'html2canvas';
import * as APPCONSTANTS from '../../app.constants';
import { environment } from '../../../environments/environment';
import { MatOption } from '@angular/material';

@Component({
  selector: 'app-add-slide',
  templateUrl: './add-slide.component.html',
  styleUrls: ['./add-slide.component.css']
})
export class AddSlideComponent implements OnInit {

  recursiverows: FormArray;
  curr_template_id : string;

  public nextphase: boolean = false;
  private TEMPLATES  : any;
  public SlideForm: FormGroup;
  public playlists: any = [];
  public announcements : any =[];
  public eventslist : any =[];
  private errorMsg: any;
  public content: any; 
  private selectedPlaylist: any = null;
  public apiCallActive: boolean = false;
  public frameArea:any=[];
  public savedTemplateData : any = [];
  // public saveErr: Boolean = false;
  
  min_dt: any;
  max_dt: any;

  all_days = [];
  selected_days = [];
  is_all_day: string = 'N';

  mediaUrl = environment.mediaUrl;
  /*========================
    SAVE TEMPLATE
  ========================*/
  length: number = 0;
  pageSize = environment.limit;
  page: number = 1;

  saveTemplateSearchkey: string ='';
  saveTemplateSortby: string ='created';

  /*========================
    SAVE TEMPLATE
  ========================*/

  componentRef: any;
  @ViewChild('messagecontainer', { read: ViewContainerRef }) entry: ViewContainerRef;
  @ViewChild('screen') screen: ElementRef;
  @ViewChild('allSelected') private allSelected: MatOption;

  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private commonBridgeService: CommonBridgeService, 
    private printService: PrintService,
    private sharedService: SharedService, 
    private resolver: ComponentFactoryResolver, 
    private fb: FormBuilder, 
    private router : Router, 
    public snackBar: MatSnackBar,
    private activatedRoute: ActivatedRoute,
    private _location: Location,
    public dialog: MatDialog) {

      this.all_days = [
        {
          id: 0,
          value: 'SU'
        },
        {
          id: 1,
          value: 'MO'
        },
        {
          id: 2,
          value: 'TU'
        },
        {
          id: 3,
          value: 'WE'
        },
        {
          id: 4,
          value: 'TH'
        },
        {
          id: 5,
          value: 'FR'
        },
        {
          id: 6,
          value: 'SA'
        }
      ];
      this.recursiverows = this.fb.array([]);

      this.TEMPLATES = APPCONSTANTS.TEMPLATES_CONST;
      this.sharedService.SlideData.subscribe(
        (data: any) => {          
          this.content = data;
        });
  }

  onAddRow() {
    this.recursiverows.push(this.createItemFormGroup());
  }

  onRemoveRow(rowIndex:number){
    this.recursiverows.removeAt(rowIndex);
  }

  makeScheudlingBlank() {    
    let PostData      = this.SlideForm.value;    
    for (var i = PostData.recursiverows.length - 1; i >= 0; i--) {     
      this.recursiverows.removeAt(i);
    }    
  }

  makeScheudlingAdd(){
     this.makeScheudlingBlank();
     let PostData  = this.SlideForm.value;
     var i = PostData.recursiverows ? PostData.recursiverows.length : 0;
     if (i === 0) {
       this.onAddRow();
     }
  }

  makeScheudlingAuto(){
     this.makeScheudlingBlank();
     var i = 0;
     if (i === 0) {
       this.onAddRow();
     }
  }

  createItemFormGroup(): FormGroup {
    return this.fb.group({
      start_datetime: this.fb.control(null, Validators.required), 
      end_datetime: this.fb.control(null, Validators.required),
      min_datetime: this.fb.control(null), 
      max_datetime: this.fb.control(null)
    });
  }

  //load form fields
  ngOnInit() {

      this.getAllSavedTemplates();

      this.selectedPlaylist = +this.activatedRoute.snapshot.paramMap.get('playlist');
      
      //Get Playlist Dropdown Api
      this.commonBridgeService.getDroplist('playlists')
      .subscribe(response =>{
        if(response.message == 'success'){
          this.playlists = response.playlists;          
        }
      },
      error => this.errorMsg = error);

      let selectedPlaylistTemp = this.selectedPlaylist ? [this.selectedPlaylist] : [];
      
      //Declare Form Group   start_datetime   end_datetime
      this.SlideForm = this.fb.group({
        title: this.fb.control('',[Validators.required]),
        description: this.fb.control(''),
        playlist_id: this.fb.control(selectedPlaylistTemp, [Validators.required]),
        template_id: this.fb.control('',[Validators.required]),
        content: this.fb.control(''),
        duration: this.fb.control('10',[Validators.required]),
        customduration: this.fb.control('1'),
        customdurationsec: this.fb.control('0'),
        scheduled: this.fb.control('N',[Validators.required]),
        schedule_type: this.fb.control('M'),
        // start_datetime: this.fb.control(''), 
        // end_datetime: this.fb.control(''),
        schedulingdates: this.fb.array([]),
        daysOfWeek:this.fb.control([]),
        status: this.fb.control('A'),
        creator: this.fb.control('1'),
        modifier: this.fb.control('1'),
        leftcolimage: this.fb.control(''),
        rightcolimage: this.fb.control(''),
        cover_image: this.fb.control(''),
        submit: this.fb.control(false),
        showfooter: this.fb.control("T"),
      });

      this.SlideForm.addControl('recursiverows', this.recursiverows);
      // this.onAddRow();
      this.CheckRequiredFields();      
  }

  getNext(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.getAllSavedTemplates();
  }

  setFooterStatus() {      
      this.sharedService.FooterDisplay.emit(this.SlideForm.controls['showfooter'].value);
  }

  updateItemFormGroup(min_dt, max_dt): FormGroup {    
    return this.fb.group({      
      start_datetime: this.fb.control(min_dt, Validators.required), 
      end_datetime: this.fb.control(max_dt, Validators.required),
      min_datetime: this.fb.control(min_dt), 
      max_datetime: this.fb.control(max_dt)
    });
  }

  changeMinDTime(row, index){
    let min_dt = row.controls['start_datetime'].value ? row.controls['start_datetime'].value : null;
    let max_dt = row.controls['end_datetime'].value ? row.controls['end_datetime'].value : null;
    this.onRemoveRow(index);
    this.recursiverows.insert(index, this.updateItemFormGroup(min_dt, max_dt));
   
    // console.log('min_dt', min_dt);
    // console.log('this.recursiverows', this.recursiverows);
    /*if(this.SlideForm.controls['start_datetime'].value){
      this.min_dt = this.SlideForm.controls['start_datetime'].value;
    }*/
  }

  changeMaxDTime(row, index){
    let min_dt = row.controls['start_datetime'].value ? row.controls['start_datetime'].value : null;
    let max_dt = row.controls['end_datetime'].value ? row.controls['end_datetime'].value : null;
    this.onRemoveRow(index);
    this.recursiverows.insert(index, this.updateItemFormGroup(min_dt, max_dt));
    // console.log('max_dt', max_dt);
    // console.log('this.recursiverows', this.recursiverows);
    /*if(this.SlideForm.controls['end_datetime'].value){
      this.max_dt = this.SlideForm.controls['end_datetime'].value;      
    }*/
  }

  backClicked() {
    this._location.back();
  }
  

  drop(event: CdkDragDrop<string[]>) { 
    var savedTemp = event.previousContainer.element.nativeElement.className.includes("savedTemplateClass");
    if (savedTemp) {
      let classes = event.previousContainer.element.nativeElement.className;
      var checkTempl = "tmpl-"
      var templ_id = checkTempl + classes.slice(classes.indexOf(checkTempl) + checkTempl.length);
      
      if(this.content!==undefined) //If Slide Addedd First Time
      {
        const dialogRef = this.dialog.open(DragModalComponent, {
          width: '50%'
        });

        dialogRef.afterClosed().subscribe(result => {
          if(result.dragType=='Y')
          {
            this.entry.clear();
            this.sharedService.changeEditData(null);
            this.SlideForm.controls['template_id'].setValue(templ_id);
            this.SlideForm.controls['duration'].setValue('10');
            this.curr_template_id = templ_id;

            var indx = event.previousContainer.id.split('_');
            var savedIndx = parseInt(indx[1]);
            this.sharedService.changeEditData(JSON.parse(this.savedTemplateData[savedIndx].content));
            
            const factory = this.resolver.resolveComponentFactory(this.TEMPLATES[templ_id]);
            const componentRef = this.entry.createComponent(factory);
            // this.content = this.savedTemplateData[savedIndx].content;  
          }
        });
      }
      else
      {
        this.entry.clear();
        this.sharedService.changeEditData(null);
        this.SlideForm.controls['template_id'].setValue(templ_id);
        this.SlideForm.controls['duration'].setValue('10');
        this.curr_template_id = templ_id;

        var indx = event.previousContainer.id.split('_');
        var savedIndx = parseInt(indx[1]);
        this.sharedService.changeEditData(JSON.parse(this.savedTemplateData[savedIndx].content));
        
        const factory = this.resolver.resolveComponentFactory(this.TEMPLATES[templ_id]);
        const componentRef = this.entry.createComponent(factory);
        // this.content = this.savedTemplateData[savedIndx].content;
      }     
    }
    else {      
      if(this.content!==undefined) //If Slide Addedd First Time
      {
        const dialogRef = this.dialog.open(DragModalComponent, {
          width: '50%'
        });

        dialogRef.afterClosed().subscribe(result => {
          if(result.dragType=='Y')
          {
            this.entry.clear();
            this.sharedService.changeEditData(null);
            this.SlideForm.controls['template_id'].setValue(event.previousContainer.id);
            
            this.SlideForm.controls['duration'].setValue('10');
            this.curr_template_id = event.previousContainer.id;
            const factory = this.resolver.resolveComponentFactory(this.TEMPLATES[event.previousContainer.id]);
            const componentRef = this.entry.createComponent(factory);  
          }
        });
      }
      else
      {
        this.entry.clear();
        this.sharedService.changeEditData(null);
        this.SlideForm.controls['template_id'].setValue(event.previousContainer.id);
        this.SlideForm.controls['duration'].setValue('10');

        this.curr_template_id = event.previousContainer.id;
        const factory = this.resolver.resolveComponentFactory(this.TEMPLATES[event.previousContainer.id]);
        const componentRef = this.entry.createComponent(factory);
      }
    }
  }

  hideIframeArea(event:any){
    this.frameArea = document.getElementsByClassName('iframearea');
    if(this.frameArea.length>0)
    {
      this.frameArea[0].style = 'display:none;';      
    }
  }

  showIframeArea(event:any){
    this.frameArea = document.getElementsByClassName('iframearea');
    if(this.frameArea.length>0)
    {
      this.frameArea[0].style = 'display:block;';
    }
  }

  // GET SAVE TEMPLATE API CALL
  getAllSavedTemplates(){
    this.commonBridgeService.getSavedTemplateInfo(this.page, this.pageSize, this.saveTemplateSearchkey, this.saveTemplateSortby)
      .subscribe(response =>
                  {
                    if(response.status == 200){                      
                      if(response.templates){
                        this.savedTemplateData = response.templates.data;
                        this.length = response.templates.total;
                      }
                    }
                  },
      error => this.errorMsg = error); 
  }

  // ADD SAVE TEMPLATE API CALL
  saveAsTemplateClicked(content) {

    let  ModelData = {
        template_name: '',
        delete_template: 0       
    };
    let templateName = '';
    const dialogRef = this.dialog.open(SaveTemplatesModalComponent, {
      disableClose: true,
      width: '50%',
      data: ModelData,
      panelClass: 'background_modal'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){        
        if (result.templateName !== '') {          
            templateName = result.templateName;
            let savedTemplateFormsData = this.SavedTemplateFormData(content, this.curr_template_id, templateName);
            
            //Set Cover Image
            this.getCoverImage().then((response: any) => {
              savedTemplateFormsData.append('cover_image',response);
              this.commonBridgeService.saveAsTemplate(savedTemplateFormsData)
               .subscribe(response =>{
                 if(response.message == 'success'){
                   this.getAllSavedTemplates();
                   this.snackBar.open('Template saved successfully !', 'Success', {
                      duration: 4000,
                      horizontalPosition: this.horizontalPosition,
                      verticalPosition: this.verticalPosition,
                      panelClass: ['cust-snackbar', 'submitted'],
                   });
                   // this.router.navigate(['/slides']);                   
                 }
                 else {
                  this.snackBar.open('Template fail to save !', 'Error', {
                    duration: 4000, 
                    horizontalPosition: this.horizontalPosition,
                    verticalPosition: this.verticalPosition,
                    panelClass: ['cust-snackbar', 'not-submitted'],
                  });
                }
               },
               error => this.errorMsg = error);  
            });
        }
      }
    });    
  }

  //CREATE FORM DATA FOR SAVED TEMPLATE
  SavedTemplateFormData(content, template_id, templateName){
    let templateFormInfo = new FormData();    
    templateFormInfo.append('template_id', template_id);    
    templateFormInfo.append('cover_image',"");
    templateFormInfo.append('content',JSON.stringify(content));
    templateFormInfo.append('status','A');
    templateFormInfo.append('name', templateName);
    return templateFormInfo;
  }

  //SAVE SLIDE DATA WITH TEMPLATE CONTENT DATA
  SaveSlide($event){
   if(this.SlideForm.get('submit').value==false)
   {
     $event.preventDefault();
   }
   else
   {
      this.apiCallActive = true;
      let ProcessData = this.SetFormData();
      //Set Cover Image
      this.getCoverImage().then((response: any) => {
        ProcessData.append('cover_image',response);
        if (this.content.thumbimg) {
          ProcessData.append('thumbimg', this.content.thumbimg);
        }
       this.commonBridgeService.saveSlide(ProcessData)
       .subscribe(response =>{
         if(response.message == 'success'){
           this.SlideForm.controls['submit'].reset(false);
           this.snackBar.open('Slide saved successfully !', 'Success', {
              duration: 4000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
              panelClass: ['cust-snackbar', 'submitted'],
           });
           this.apiCallActive = false;

           if(this.selectedPlaylist){
             this.router.navigate(['/edit-playlist', +this.selectedPlaylist]);
           }
           else{
             this.router.navigate(['/slides']);
           }
         }
         else {
          this.apiCallActive = false;
          this.snackBar.open('Slide fail to save !', 'Error', {
            duration: 4000, 
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            panelClass: ['cust-snackbar', 'not-submitted'],
          });
        }
       },
       error => this.errorMsg = error);
      });
   }
 }

  SetFormData(){

    let slideFormInfo = new FormData();
    let PostData      = this.SlideForm.value;

    let scheudling: any;
    let dates = [];
    for (var i = 0; i <= PostData.recursiverows.length - 1; i++) {
      let startTS = this.dateToTimestamp(PostData.recursiverows[i].start_datetime);
      let endTS = this.dateToTimestamp(PostData.recursiverows[i].end_datetime);

      if (isNaN(startTS)) {
        startTS = 0;
      }
      if (isNaN(endTS)) {
        endTS = 0;
      }

      let tempDates = { 
        start_datetime: startTS,
        end_datetime: endTS
      };

      dates.push(tempDates);      
    }

    let is_all_day = 'N';

    if(PostData.daysOfWeek.length >= this.all_days.length){
      is_all_day = 'Y';
    }
    else{
      is_all_day = 'N';
    }

    scheudling = {dates:dates};

    //design slide data
    if(PostData.template_id == 'tmpl-video'){
      slideFormInfo.append('duration',this.content.duration);
    }
    else{
      if (PostData.duration === 'custom') {
        // code...
        const minseconds: number = Math.floor(PostData.customduration * 60);
        const seconds: number = Math.floor(PostData.customdurationsec);
        const total: number = minseconds + seconds;
        slideFormInfo.append('duration', total.toString());
      }
      else {
        slideFormInfo.append('duration',PostData.duration);
      }
    }
    slideFormInfo.append('title',PostData.title);
    slideFormInfo.append('description',PostData.description);
    slideFormInfo.append('scheduled',PostData.scheduled);

    slideFormInfo.append('schedule_type',PostData.schedule_type);
    slideFormInfo.append('daysofweek',PostData.daysOfWeek);
    slideFormInfo.append('is_all_day',is_all_day);
    slideFormInfo.append('schedulingdates', JSON.stringify(scheudling));
    // slideFormInfo.append('start_datetime', startTS.toString());
    // slideFormInfo.append('end_datetime', endTS.toString());    

    slideFormInfo.append('playlist_id',PostData.playlist_id);
    slideFormInfo.append('template_id',PostData.template_id);
    slideFormInfo.append('status',PostData.status);
    slideFormInfo.append('leftcolimage',this.content.leftcolimage);
    slideFormInfo.append('rightcolimage',this.content.rightcolimage);
    
    slideFormInfo.append('cover_image',"");

    slideFormInfo.append('content',JSON.stringify(this.content));
   return slideFormInfo;
  }

  dateToTimestamp(dt){    
    let millitime = new Date(dt).setSeconds(0);
    return millitime;
  }

  //Get Screen Shot Of Generated Template
  getCoverImage(){
    return new Promise((resolve, reject) => {
      html2canvas(this.screen.nativeElement,{
        windowWidth:1000,
        windowHeight:2000,
        logging:true,
        useCORS:true,
        proxy:environment.mediaUrl+"html2canvasproxy.php"}).then(canvas => {
        let dataUrl = canvas.toDataURL();
        resolve(dataUrl);
      }).catch(e => {
        console.log(e);
      });
    });
  }

  openPrintDialog(){
    let template_id = this.SlideForm.controls['template_id'].value;
    if(template_id){
      let content = {...this.content};      

      let printInfo = this.loadComponent(template_id,content);
      
      const dialogRef = this.dialog.open(PrintDialogComponent, {
        width: '50%',
        data: { infos: printInfo },
        panelClass: 'print_modal'
      });

      /*dialogRef.afterClosed().subscribe(result => {
        this.dialog.destroy();
      });*/
    }
    else{
      this.snackBar.open('No template available', 'Error', {
        duration: 4000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        panelClass: ['cust-snackbar', 'not-submitted'],
      });
    }
  }

  loadComponent(template_id, content){
    const template_arr: Array<{template_id: string, content: any, status: string}> = [{'template_id': template_id, 'content': JSON.stringify(content), 'status': 'A'}];
    let infos = this.printService.getComponents(template_arr);

    for (var i = infos.length - 1; i >= 0; i--) {
      infos[i].content_data['community_footer'] = {'status': this.SlideForm.controls['showfooter'].value, 'content': {}};
      infos[i].content_data['scroll_announcement'] = {'status': this.SlideForm.controls['showfooter'].value, 'content': ''};
    }
    return infos;
  }

  openPreviewDialog(){
    let template_id = this.SlideForm.controls['template_id'].value;
    if(template_id){
      let content = this.content;     

      let printInfo = this.loadComponent(template_id,content);      
      const dialogRef = this.dialog.open(PreviewDialogComponent, {
        width: '50%',
        data: { infos: printInfo, prev_type: 2 },
        panelClass: 'print_modal'
      });
    }
    else{
      this.snackBar.open( 'No template available', 'Error', {
        duration: 4000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        panelClass: ['cust-snackbar', 'not-submitted'],
      });
    }
  }
  
  //scheduled required on codition
  CheckRequiredFields() {
    /*this.SlideForm.get('scheduled').valueChanges.subscribe((scheduled: string) => {
      if (scheduled === 'Y') {
        this.SlideForm.get('start_datetime').setValidators([Validators.required]);
        this.SlideForm.get('end_datetime').setValidators([Validators.required]);
      }
      else if (scheduled === 'N') {
        this.SlideForm.get('start_datetime').clearValidators();
        this.SlideForm.get('end_datetime').clearValidators();
      }
      this.SlideForm.get('start_datetime').updateValueAndValidity();
      this.SlideForm.get('end_datetime').updateValueAndValidity();
    });*/
  }
  //trackBy
  trackByPlaylist(index: number, playlist: any): number {
    return playlist.playlist_id;
  }

  trackByDay(index: number, all_days: any): number {
    return all_days.id;
  }

  //*MAS Crescente Code
  UncheckAllDays(all){ 
      if (this.allSelected.selected){  
          this.allSelected.deselect();
          return false;
      }
      if(this.SlideForm.controls.daysOfWeek.value.length==this.all_days.length){
          this.allSelected.select();        
      }  
  }
  checkAllDays() {
    if (this.allSelected.selected) {  
      this.SlideForm.controls.daysOfWeek.setValue(this.all_days.map(item => item.id));
      this.allSelected.select();
    } else {
      this.SlideForm.controls.daysOfWeek.setValue([]);
    }
  }

  TrashTemplate(template_id){

      let  ModelData = {
          delete_template: 1        
      };
      let templateName = '';
      const dialogRef = this.dialog.open(SaveTemplatesModalComponent, {
        disableClose: true,
        width: '50%',
        data: ModelData,
        panelClass: 'background_modal'
      });

      dialogRef.afterClosed().subscribe(result => {

          if(result.deletetemplate == 'Y'){
              this.commonBridgeService.deleteTemplate({temp_id:template_id})
               .subscribe(response =>{
                 if(response.delete==1)
                 {
                    this.snackBar.open('Template removed successfully !', 'Success', {
                      duration: 4000,
                      horizontalPosition: this.horizontalPosition,
                      verticalPosition: this.verticalPosition,
                      panelClass: ['cust-snackbar', 'submitted'],
                    });
                    this.getAllSavedTemplates();
                 }
                 else
                 {
                   this.snackBar.open('Unable to remove template !', 'Error', {
                      duration: 4000,
                      horizontalPosition: this.horizontalPosition,
                      verticalPosition: this.verticalPosition,
                      panelClass: ['cust-snackbar', 'not-submitted'],
                    });
                 }
              });
          } 

      });  
  }
}