import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, PageEvent } from '@angular/material';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { CommonBridgeService } from './../services/common-bridge.service';
import { HideSlideModalComponent } from './../hide-slide-modal/hide-slide-modal.component';
import { DeleteSlideModalComponent } from './../delete-slide-modal/delete-slide-modal.component';
import { environment } from '../../environments/environment';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

const LIMIT = environment.limit;

@Component({
  selector: 'app-slides',
  templateUrl: './slides.component.html',
  styleUrls: ['./slides.component.css']
})
export class SlidesComponent implements OnInit {

    public blurDefault = environment.blurDefault;
    public noimageDefault = environment.noimageDefault;
    public offset = environment.imgoffset;
    mediaUrl = environment.mediaUrl;
    title: string;
	  slide_thumb: string;
    dateTemp : any = Math.random();
    slides = [];
    errorMsg: any;
    
    // MatPaginator Inputs
    length: number = 0;
    pageSize = LIMIT;
    page: number = 1;

    // MatPaginator Output
    pageEvent: PageEvent;

    // Snackbar Posittion
    horizontalPosition: MatSnackBarHorizontalPosition = 'end';
    verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private commonBridgeService: CommonBridgeService,
    private _sanitizer: DomSanitizer,
    private router : Router, 
    private route : ActivatedRoute,
    public dialog: MatDialog,
    private snackBar: MatSnackBar) {
  	this.title = 'Slides';
  	this.slide_thumb = environment.noimageDefault;
  }

  ngOnInit() {
    this.getSlides(this.page, LIMIT);
  }

  getNext(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.getSlides(this.page, LIMIT);
  }

  getSlides(page, limit){
    this.commonBridgeService.getSlidesList(null, page.toString(),limit.toString(),'created')
      .subscribe(response =>
                            {
                              if(response.message == 'success'){
                                this.slides = response.slides.data;
                                this.length = response.slides.total;
                              }
                            },
                error => this.errorMsg = error);
  } 

  copySlide(slide_id){
    this.commonBridgeService.copySlide(slide_id)
       .subscribe(response =>{
         if(response.status == 200){
           
           this.snackBar.open('Slide copied successfully !', 'Success', {
              duration: 4000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
              panelClass: ['cust-snackbar', 'submitted'],
           });
           this.router.navigate(['/edit-slide', response.slide_id]);                   
         }
         else {
          this.snackBar.open('Slide fail to copy !', 'Error', {
            duration: 4000, 
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            panelClass: ['cust-snackbar', 'not-submitted'],
          });
        }
       },
       error => this.errorMsg = error);
  }

  openDelteDialog(slide_id): void {
    const dialogRef = this.dialog.open(DeleteSlideModalComponent, {
      width: '50%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result == 'Y'){
        this.deleteSlide(slide_id);
      }
    });
  }

  deleteSlide(slide_id){
    this.commonBridgeService.deleteSlide(slide_id)
     .subscribe(response =>
       {
         if(response.status == 200){
           if(response.delete){
             this.snackBar.open('Slide deleted successfully !', 'Success', {
                duration: 4000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
                panelClass: ['cust-snackbar', 'submitted'],
              });
              let updateItem = this.slides.find(this.findIndexToUpdate, slide_id);
              let index = this.slides.indexOf(updateItem);
              if (index !== -1) {
                this.slides.splice(index, 1);
              }
           }
           else {
              this.snackBar.open('Fail to delete slide !', 'Error', {
                duration: 4000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
                panelClass: ['cust-snackbar', 'not-submitted'],
              });
           }
         }
         else {
           this.snackBar.open('Fail to delete slide !', 'Error', {
              duration: 4000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
              panelClass: ['cust-snackbar', 'not-submitted'],
           });
         }
       },
       error => this.errorMsg = error);
  }

  openHideShowDialog(slide_id, status): void {
    const dialogRef = this.dialog.open(HideSlideModalComponent, {
      width: '50%',
      data: { status: status }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result == 'Y'){
        this.hideShowSlide(slide_id, status);
      }
    });
  }

  hideShowSlide(slide_id, status){
    let newStatus = (status == 'I') ? 'A' : 'I';
    this.commonBridgeService.hideShowSlide(slide_id, newStatus)
     .subscribe(response =>
       {
         if(response.status == 200){
           if(response.update){
             this.updateSlideStatus(slide_id, newStatus);
             this.snackBar.open('Slide status updated successfully !', 'Success', {
                duration: 4000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
                panelClass: ['cust-snackbar', 'submitted'],
              });
           }
           else {
             this.snackBar.open('Fail to update slide status !', 'Error', {
                duration: 4000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
                panelClass: ['cust-snackbar', 'not-submitted'],
             });
           }
         }
         else {
           this.snackBar.open('Fail to update slide status !', 'Error', {
              duration: 4000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
              panelClass: ['cust-snackbar', 'not-submitted'],
           });
         }
       },
       error => this.errorMsg = error);
  }

  updateSlideStatus(slide_id, status){
    let updateItem = this.slides.find(this.findIndexToUpdate, slide_id);
    let index = this.slides.indexOf(updateItem);
    if (index !== -1) {
      this.slides[index].status = status;
    }
  }

  findIndexToUpdate(newItem) { 
    return newItem.slide_id === this;
  }
  //trackBy
  trackBySlide(index: number, slide: any): number {
  return slide.slide_id;
  }

}
