import {Component, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { SharedService,CommonBridgeService } from '../services';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';

const LIMIT = environment.limit;

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent {
  	public blurDefault = environment.blurDefault;
  	noMedia = environment.noimageDefault;
  	public offset = environment.imgoffset;
  	public SettingConForm: FormGroup;
	mediaUrl = environment.mediaUrl;
	dateTemp : any = Math.random();
	previewImage: any="";
	private errorMsg: any;
	public ex_footer_logo: string = '';
  	AccUserImageFile: File;
  	
  	@ViewChild('AccUserImg') AccUserImage;

	// Snackbar Posittion
	horizontalPosition: MatSnackBarHorizontalPosition = 'end';
	verticalPosition: MatSnackBarVerticalPosition = 'top';

  	constructor(
  		private sharedService: SharedService,
  		private fb: FormBuilder, 
  		public snackBar: MatSnackBar, 
  		private commonBridgeService: CommonBridgeService) {
    	this.getSettingData();
  	}


  ngOnInit() {
  	this.SettingConForm = this.fb.group({
      Cityid: this.fb.control(''),
      cityName: this.fb.control(''),
      Appid: this.fb.control(''),
      Image: this.fb.control(null),
      footerColor: this.fb.control(''),
      footerBgcolor: this.fb.control(''),
      announcementColor: this.fb.control(''),
      announcementBgcolor: this.fb.control(''),
      scrolldelay: this.fb.control(0),
      diningAnnPostid: this.fb.control(''),
      weatherUnit: this.fb.control(''),
      autoRefersh: this.fb.control('No'),
      customSecond: this.fb.control(0),
    });
  }

  	getSettingData() {
  		this.commonBridgeService.getSettings()
    		.subscribe(response => {
          if(response.status === 200){    
          	if (response.settings[0]) {
          	    this.ex_footer_logo = response.settings[0].footer_logo;
          	    this.dateTemp = Math.random();
	            
	            this.SettingConForm = this.fb.group({
			      Cityid: this.fb.control(response.settings[0].city_id),
			      cityName: this.fb.control(response.settings[0].city_name),
			      Appid: this.fb.control(response.settings[0].app_id),
			      Image: this.fb.control(null),
			      footerColor: this.fb.control(response.settings[0].footerColor),
			      footerBgcolor: this.fb.control(response.settings[0].footerBgcolor),
			      announcementColor: this.fb.control(response.settings[0].announcementColor),
			      announcementBgcolor: this.fb.control(response.settings[0].announcementBgcolor),
			      scrolldelay: this.fb.control(response.settings[0].scrolldelay),
			      diningAnnPostid: this.fb.control(response.settings[0].diningAnnPostid),
			      weatherUnit: this.fb.control(response.settings[0].weatherUnit),
			      autoRefersh: this.fb.control(response.settings[0].autoRefersh),
			      customSecond: this.fb.control(response.settings[0].customSecond),
			    });    	
          	}           
          }
        },
	    error => this.errorMsg = error);
  	}
  	//set Preview Image when user select input file
	setPreviewImage(fileInput:any) {
		if (fileInput.target.files && fileInput.target.files[0]) {	      
	      this.sharedService.returnBase64Image(fileInput.target.files[0]).subscribe((response: any) => {
	        this.previewImage = response;
	      });
	    }
	}

  	public CreateSettingContent(value) {
	    const Image = this.AccUserImage.nativeElement;
	    if (Image.files && Image.files[0]) {
	      this.AccUserImageFile = Image.files[0];
	    }
	    else {
	    	this.AccUserImageFile = null;
	    }
	    const ImageFiles : File = this.AccUserImageFile;

	    const formData = new FormData();
	    formData.append( 'city_id', value.Cityid);
	    formData.append( 'city_name', value.cityName);
	    formData.append( 'app_id', value.Appid);
	    formData.append( 'footer_logo', ImageFiles);
	    formData.append( 'footerColor', value.footerColor);
	    formData.append( 'footerBgcolor', value.footerBgcolor);
	    formData.append( 'announcementColor', value.announcementColor);
	    formData.append( 'announcementBgcolor', value.announcementBgcolor);
	    formData.append( 'ex_footer_logo', this.ex_footer_logo); 
	    formData.append( 'scrolldelay', value.scrolldelay);
	    formData.append( 'diningAnnPostid', value.diningAnnPostid);
	    formData.append( 'weatherUnit', value.weatherUnit);
	    formData.append( 'autoRefersh', value.autoRefersh);
	    formData.append( 'customSecond', value.customSecond);

	    this.commonBridgeService.saveSettings(formData).subscribe(response =>{
	         if(response.message == 'success'){
	         	this.SettingConForm = this.fb.group({
			      Cityid: this.fb.control(''),
			      cityName: this.fb.control(''),
			      Appid: this.fb.control(''),
			      Image: this.fb.control(null),
			      footerColor: this.fb.control(''),
			      footerBgcolor: this.fb.control(''),
			      announcementColor: this.fb.control(''),
			      announcementBgcolor: this.fb.control(''),
			      scrolldelay: this.fb.control(0),
			      diningAnnPostid: this.fb.control(''),
			      weatherUnit: this.fb.control(''),
		       	  autoRefersh: this.fb.control('No'),
			      customSecond: this.fb.control(0),
			    });
	         	this.ex_footer_logo ='';
	         	this.getSettingData();
	           	this.snackBar.open('Settings saved successfully !', 'Success', {
					duration: 4000,
					horizontalPosition: this.horizontalPosition,
					verticalPosition: this.verticalPosition,
					panelClass: ['cust-snackbar', 'submitted'],
	           });
	        } else {
	          	this.snackBar.open('Settings fail to save !', 'Error', {
					duration: 4000, 
					horizontalPosition: this.horizontalPosition,
					verticalPosition: this.verticalPosition,
					panelClass: ['cust-snackbar', 'not-submitted'],
	          	});
	        }
       	},
       	error => this.errorMsg = error);    
  	}
}