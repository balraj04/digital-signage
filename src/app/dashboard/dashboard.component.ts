import { Component, OnInit } from '@angular/core';
import { CommonBridgeService } from './../services/index';
import { ISubscription } from "rxjs/Subscription";
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public subscription:ISubscription;  
    title: string;
    errorMsg: any;
    playlists: number;
    slides: number;
    templates: number;

  constructor(
    private commonBridgeService: CommonBridgeService) {
  	this.title = 'Digital Signage';
  }
  ngOnInit() {
  	this.subscription = this.commonBridgeService.getDashboardDetails()
      .subscribe(response =>
                            {
                              if(response.message == 'success'){
                                this.playlists = response.playlists;
                                this.slides = response.slides;
                                this.templates = response.templates;
                              }
                            },
                error => this.errorMsg = error);
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
