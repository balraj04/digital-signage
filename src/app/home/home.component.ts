import { Component, OnInit, Inject, ViewChild, ViewContainerRef, ComponentFactoryResolver, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonBridgeService, PrintService } from './../services/index';
import { PlaylistInfo, TempWeatherInfo, TempAnnouncementInfo } from './../interfaces/index';
import { TemplatesComponent } from './../interfaces/templates';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';
import { ConnectionService } from 'ng-connection-service';
declare var $ :any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @ViewChild('processContainer', { read: ViewContainerRef }) container;
  @ViewChild('announcementContainer', { read: ViewContainerRef }) announcementContainer;
  @ViewChild('footerContainer', { read: ViewContainerRef }) footerContainer;

  title: string;
  private sub: any;
  slug: string;
  errorMsg: any;
  playlistInfo: PlaylistInfo = {
    playlist: {
      name: '',
      assign_calendar: '',
      scroll_announcement: '',
      footer_text: '',
      slides: []
    }
  } as any;
  tempSlides: any[];

  customeSecond : number;
  autoRefersh : string;

  currentAdIndex = 0;
  defaultInterval: any = 10000;
  interval: any;

  tempWeather: TempWeatherInfo = {
    city_name: '',
    temp: 0,
    logo: '',
    footerColor: '',
    footerBgcolor: '',
    custom_city_name: ''
  };

  tempAnnouncement: TempAnnouncementInfo = {
    text: '',
    scrolldelay: 0,
    announcementColor: '',
    announcementBgcolor: ''
  };

  @Input()
  divClass: string;

  // Snackbar Posittion
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  
  // Footer and Announcement Flags
  hasFooter: boolean = false;
  hasAnnouncement: boolean = false;

  editorBlock: any;
  editorHeight: any;
  editorWidth: any;
  scaleBlock: any;
  scaleCount:any;

  status = 'ONLINE';
  isConnected = true;

  constructor(
    private commonBridgeService: CommonBridgeService,
    private printService: PrintService,
    private route: ActivatedRoute,
    private resolver: ComponentFactoryResolver,
    private snackBar: MatSnackBar,
    private connectionService: ConnectionService) {
    this.title = "Digital Signage";
    this.tempSlides = [];

    this.connectionService.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
      if (this.isConnected) {
        this.status = "ONLINE";
        // setTimeout(function(){ window.location.reload(); }, 2000);
      }
      else {
        this.status = "OFFLINE";
      }
    })
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
       this.slug = params['slug'];       
       this.getPlaylistsInfo(this.slug);
    });
  }

  onResize(event) {
    this.scaleApply();
  }

  ngAfterViewInit() {
    this.scaleApply();
  }

  scaleApply(){
    this.scaleBlock  = document.getElementsByClassName('print_modal1');

    this.editorWidth = window.innerWidth;
    this.editorHeight = window.innerHeight;
    this.scaleCount = Math.min(this.editorWidth / 640, this.editorHeight / 360);
    
    let translateCount = (( this.scaleCount - 1 ) * 50);
    if(this.scaleCount)
    {

      // this.scaleBlock[0].style.transform = 'translate(-50%, '+translateCount+'%) scale('+this.scaleCount+')';
      this.scaleBlock[0].style.transform = 'translate(-50%, -50%) scale('+this.scaleCount+')';
    }
  }

  getPlaylistsInfo(slug){
    clearInterval(this.interval);
    let datetime = new Date().getTime();
    this.commonBridgeService.getPlaylistInfo(slug, datetime, true)
      .subscribe(response =>
                            {
                              if(response.status == 200){

                                this.hasAnnouncement = false;
                                this.hasFooter = false;
                                this.playlistInfo = response;
                                if(this.playlistInfo.playlist && this.playlistInfo.playlist.slides.length){
                                  this.snackBar.dismiss();
                                  this.configComponent();
                                  
                                }
                                else{
                                  this.snackBar.open('No slides available', 'Error', {
                                    horizontalPosition: this.horizontalPosition,
                                    verticalPosition: this.verticalPosition,
                                    panelClass: ['cust-snackbar', 'not-submitted'],
                                  });

                                  setTimeout(()=>{
                                    this.getPlaylistsInfo(this.slug);
                                  }, 10000);
                                }
                              }
                            },
                error => this.errorMsg = error);
  }

  configComponent(){
    this.getFooterSettings();
    /*if(this.playlistInfo.playlist.footer_text == 'Y' || this.playlistInfo.playlist.scroll_announcement == 'Y'){
      this.getFooterSettings();
    }
    else{
      this.loadCarouselComponent();
    }*/
  }

  getFooterSettings(){
    this.commonBridgeService.getSettings(true)
      .subscribe(response =>
                            {
                              if(response.status == 200){
                                if(response.settings[0]){
                                  if(response.settings[0].footer_logo){
                                    this.tempWeather.logo = response.settings[0].footer_logo;
                                  }
                                  if(response.settings[0].footerColor){
                                    this.tempWeather.footerColor = response.settings[0].footerColor;
                                  }
                                  if(response.settings[0].footerBgcolor){
                                    this.tempWeather.footerBgcolor = response.settings[0].footerBgcolor;
                                  }
                                  if(response.settings[0].announcementColor){
                                    this.tempAnnouncement.announcementColor = response.settings[0].announcementColor;
                                  }
                                  if(response.settings[0].announcementBgcolor){
                                    this.tempAnnouncement.announcementBgcolor = response.settings[0].announcementBgcolor;
                                  }
                                  if(response.settings[0].scrolldelay){
                                    this.tempAnnouncement.scrolldelay = response.settings[0].scrolldelay;
                                  }
                                  if(response.settings[0].city_name){
                                    this.tempWeather.custom_city_name = response.settings[0].city_name;
                                  }
                                  if(response.settings[0].app_id && response.settings[0].city_id){
                                    this.getWeatherInfo(response.settings[0].app_id, response.settings[0].city_id, response.settings[0].weatherUnit);
                                  }
                                  if (response.settings[0].autoRefersh && response.settings[0].customSecond) {
                                    this.customeSecond = parseInt(response.settings[0].customSecond);
                                    this.autoRefersh = response.settings[0].autoRefersh;
                                  }
                                  else{
                                    this.loadCarouselComponent();
                                  }
                                }
                              }
                            },
                error => this.errorMsg = error);
  }

  getWeatherInfo(app_id, city_id, unit){
    this.commonBridgeService.getWeatherInfo(app_id, city_id, unit)
      .subscribe(response =>
                            {
                              if(response.cod == 200){
                                this.tempWeather.temp = response.main.temp;
                                this.tempWeather.city_name = response.name;
                                this.loadCarouselComponent();
                              }
                            },
                error => {
                  this.loadCarouselComponent();
                  this.errorMsg = error;
                });
  }

  loadCarouselComponent(){
    this.tempSlides = this.playlistInfo.playlist.slides.filter(slide => slide.status === 'A');
    if (this.tempSlides.length) {
      if(this.playlistInfo.playlist.scroll_announcement == 'Y'){
        this.hasAnnouncement = true;
        this.tempAnnouncement.text = this.playlistInfo.playlist.announcements;
        const template_arr: Array<{template_id: string, content: any, status: string}> = [{'template_id': 'tmpl-announcement', 'content': this.tempAnnouncement, 'status': 'A'}];
        let infos = this.printService.getComponents(template_arr);
        if(infos.length){
          let announceItem = infos[0];
          const factory = this.resolver.resolveComponentFactory(announceItem.component);
          this.announcementContainer.clear();
          let componentRef = this.announcementContainer.createComponent(factory);
          let tempContent = (announceItem.content_data.content) ? announceItem.content_data.content : {};
          let content = tempContent;
          (<any>componentRef.instance).scroll_text = content;
        }
      }

      if(this.playlistInfo.playlist.footer_text == 'Y'){        
        this.hasFooter = true;
        const template_arr: Array<{template_id: string, content: any, status: string}> = [{'template_id': 'tmpl-footer', 'content': this.tempWeather, 'status': 'A'}];
        let infos = this.printService.getComponents(template_arr);
        if(infos.length){
          let footerItem = infos[0];
          const factory = this.resolver.resolveComponentFactory(footerItem.component);
          this.footerContainer.clear();
          let componentRef = this.footerContainer.createComponent(factory);
          let tempContent = (footerItem.content_data.content) ? footerItem.content_data.content : {};
          let content = tempContent;
          (<any>componentRef.instance).community_footer = content;
        }
      }

      this.tempSlides = this.tempSlides.map(x => { 
        x.duration = x.duration * 1000;
        return x
      });

      setTimeout(()=>{
        this.startCarousal();
      }, 1000);
    }
    else {
      this.snackBar.open('No slides available', 'Error', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        panelClass: ['cust-snackbar', 'not-submitted'],
      });
      setTimeout(()=>{
        this.getPlaylistsInfo(this.slug);
      }, 10000);
    }
  }

  isSelected(i){
    return (i == this.currentAdIndex) ? 'show display_video' : 'hide hidden_video' ;
  }

  prevSlide(){
    if(this.currentAdIndex <= 0){
      this.currentAdIndex = this.tempSlides.length;
    }
    this.currentAdIndex --;
    this.startCarousal();
  }

  nextSlide(){
    let tmpcount = this.tempSlides.length;
    tmpcount --;
    if(this.currentAdIndex >= tmpcount){
      this.currentAdIndex = 0;
      if(!this.isConnected){
        this.startCarousal();
      }
      else{
        this.playlistInfo.playlist.slides = [];
        this.tempSlides = [];
        this.getPlaylistsInfo(this.slug);
      }
    }
    else{
      this.currentAdIndex ++;
      this.startCarousal();
    }
  }

  startCarousal(){
    let ref = this;
    setTimeout(function() {
      ref.show_hide_video();
    }, 1000);

    clearInterval(this.interval);    
    if (this.tempSlides.length > 1) {
      let mills = this.tempSlides[this.currentAdIndex].duration;
      this.interval = setInterval(() => {
          this.nextSlide();
      }, mills);
    }
    else {
      if (this.autoRefersh == 'Yes') {
        let mills = this.customeSecond * 1000;
        this.interval = setInterval(() => {
            this.nextSlide();
        }, mills);
      }
    }
    /*let mills = this.tempSlides[this.currentAdIndex].duration;
    this.interval = setInterval(() => {
        this.nextSlide();
    }, mills); */
  }

  trackBySlide(index: number, playlist: any): number {
    return playlist.slide_id;
  }

  ngOnDestroy(){
    clearInterval(this.interval);
  }

  show_hide_video() {
     $('div.slide-item.hidden_video').find('video').map(function () {
      this.pause();
     });
     $('div.slide-item.display_video').find('video').map(function () {
      this.currentTime = 0;
      this.play();
     });
     $('div.slide-item.hidden_video').find('iframe.youtube_video_play').map(function () {        
      let url = $(this).attr('src');
      if(url.indexOf("autoplay")>0){
        let new_url = url.substring(0, url.indexOf("?"));
        $(this).attr('src', new_url);
      }
      $(this).attr('allow', '');
     });

     $('div.slide-item.display_video').find('iframe.youtube_video_play').each(function(){         
       let url = $(this).attr('src');
        if(url.indexOf("autoplay")>0){
          let new_url = url.substring(0, url.indexOf("?"));
          $(this).attr('src', new_url);
          $(this).attr('src', url+'?autoplay=1');
          $(this).attr('allow', 'autoplay');
        }
        else{
          $(this).attr('src', url+'?autoplay=1');
          $(this).attr('allow', 'autoplay');
        }
      });
     /*var is = true;
     $('div.slide-item.display_video').find('iframe').each(function(){
       $('#mainComponent').addClass('noScale');
       is = false;
     });
     if(is){
       $('div.slide-item.hidden_video').find('iframe').each(function(){
         $('#mainComponent').removeClass('noScale')
       });
     }*/
   }
}
