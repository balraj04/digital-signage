import { Component, OnInit, Input, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { PrintService } from './../../services/index';
import { TemplatesComponent } from './../../interfaces/templates';

@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.css']
})
export class SlideComponent implements OnInit {

	@Input() slidedata: any;
	@ViewChild('processContainer', { read: ViewContainerRef }) container;
	tempArr = [];

  constructor(
  	private printService: PrintService,
  	private resolver: ComponentFactoryResolver,) { }

  ngOnInit() {
  	this.tempArr.push(this.slidedata);
  	this.loadCarouselComponent();
  }

  loadCarouselComponent(){
  	let infos = this.printService.getComponents(this.tempArr);
  	this.loadTemplateComponents(infos[0]);
  }

  loadTemplateComponents(adItem){
    if(adItem){
      let tempContent = (adItem.content_data.content) ? {...(JSON.parse(adItem.content_data.content))} : {};
      let tmpIsValide = (adItem.content_data.content) ? 'yes' : 'no';
      if (adItem.content_data.template_id == 'tmpl-event') {
        if (tempContent.event_id || tempContent.title) {
          tmpIsValide = 'yes';
          tempContent['event_start_date'] = adItem.content_data.event_start_date;
          tempContent['event_start_time'] = adItem.content_data.event_start_time;
          tempContent['event_end_date'] = adItem.content_data.event_end_date;
          tempContent['event_end_time'] = adItem.content_data.event_end_time;
          tempContent['event_location'] = adItem.content_data.event_location;
          tempContent['event_title'] = adItem.content_data.event_title;
          tempContent['event_desc'] = adItem.content_data.event_desc;
          tempContent['event_id'] = adItem.content_data.event_id;
          tempContent['layout_id'] = adItem.content_data.layout_id ? adItem.content_data.layout_id : '';
          tempContent['styled']= false;
        }
        else {
          tmpIsValide = 'yes';
          tempContent['event_start_date'] = adItem.content_data.event_start_date;
          tempContent['event_start_time'] = adItem.content_data.event_start_time;
          tempContent['event_end_date'] = adItem.content_data.event_end_date;
          tempContent['event_end_time'] = adItem.content_data.event_end_time;
          tempContent['event_location'] = adItem.content_data.event_location;
          tempContent['event_title'] = adItem.content_data.event_title;
          tempContent['event_desc'] = adItem.content_data.event_desc;
          tempContent['event_id'] = adItem.content_data.event_id;
          tempContent['layout_id'] = adItem.content_data.layout_id ? adItem.content_data.layout_id : '';
          tempContent['styled']= false;
          tempContent['leftcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#fafafa", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "" };
          tempContent['rightcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#cacaca", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "" };
          tempContent['leftcolimage'] = "";
          tempContent['rightcolimage'] = "";
        }        
      }
      else if (adItem.content_data.template_id == 'tmpl-event-widget') {
        if (tempContent) {
          if(adItem.content_data.eventslist){
            tempContent['eventslist'] = adItem.content_data.eventslist;
          }
          if(tempContent.widget_type){
            tempContent['widget_type'] = tempContent.widget_type;
          }
          tempContent['leftcolimage'] = (adItem.content_data.content.leftcolimage != null && adItem.content_data.content.leftcolimage != undefined) ? adItem.content_data.content.leftcolimage : '';
          tempContent['rightcolimage'] = (adItem.content_data.content.rightcolimage != null && adItem.content_data.content.rightcolimage != undefined) ? adItem.content_data.content.rightcolimage : '';
        }
        else {
          tempContent = {};
          tempContent['eventslist'] = [];
          tempContent['styled']= false;
          tempContent['leftcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#fafafa", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
          tempContent['rightcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#cacaca", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
          tempContent['leftcolimage'] = "";
          tempContent['rightcolimage'] = "";
        } 
      }
      else if (adItem.content_data.template_id == 'tmpl-announcement-widget') {
        if (tempContent) {
          tempContent['announcements'] = adItem.content_data.announcements ? adItem.content_data.announcements : [];
          tempContent['leftcolimage'] = (adItem.content_data.content.leftcolimage != null && adItem.content_data.content.leftcolimage != undefined) ? adItem.content_data.content.leftcolimage : '';
          tempContent['rightcolimage'] = (adItem.content_data.content.rightcolimage != null && adItem.content_data.content.rightcolimage != undefined) ? adItem.content_data.content.rightcolimage : '';
        }
        else {
          tempContent = {};
          tempContent['announcements'] = [];
          tempContent['styled']= false;
          tempContent['leftcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#fafafa", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
          tempContent['rightcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#cacaca", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
          tempContent['leftcolimage'] = "";
          tempContent['rightcolimage'] = "";
        } 
      }

      const factory = this.resolver.resolveComponentFactory(adItem.component);
      this.container.clear();
      let componentRef = this.container.createComponent(factory);
      let content = tempContent;
      let is_valide = tmpIsValide;

      (<TemplatesComponent>componentRef.instance).templatedata = content;
      (<TemplatesComponent>componentRef.instance).is_valide = is_valide;
      (<TemplatesComponent>componentRef.instance).type = 'preview';
      (<TemplatesComponent>componentRef.instance).community_footer = adItem.content_data.community_footer;
      (<TemplatesComponent>componentRef.instance).scroll_announcement = adItem.content_data.scroll_announcement;
    }
  }

}
