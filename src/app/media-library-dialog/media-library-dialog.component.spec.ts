import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaLibraryDialogComponent } from './media-library-dialog.component';

describe('MediaLibraryDialogComponent', () => {
  let component: MediaLibraryDialogComponent;
  let fixture: ComponentFixture<MediaLibraryDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaLibraryDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaLibraryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
