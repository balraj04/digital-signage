import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, PageEvent } from '@angular/material';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonBridgeService } from './../services/common-bridge.service';
import { SharedService } from './../services/shared.service';
import { environment } from '../../environments/environment';

const LIMIT = environment.limit;

@Component({
  selector: 'app-media-library-dialog',
  templateUrl: './media-library-dialog.component.html',
  styleUrls: ['./media-library-dialog.component.css']
})
export class MediaLibraryDialogComponent implements OnInit {

  imgoffset = environment.imgoffset;
  blurDefault = environment.blurDefault;
  public mediaForm: FormGroup;
  errorMsg: any;
  medias: [];
  dateTemp : any = Math.random();
  mediaUrl = environment.mediaUrl;
  media_thumb: string;
  imgUrl : String;

  // MatPaginator Inputs
  length: number = 0;
  pageSize = LIMIT;
  page: number = 1;

  // MatPaginator Output
  pageEvent: PageEvent;

  @ViewChild('fileInput') el:ElementRef;

  constructor(
  	private commonBridgeService: CommonBridgeService,
    private sharedService: SharedService,
  	private _sanitizer: DomSanitizer,
  	private fb: FormBuilder,
  	public dialogRef: MatDialogRef<MediaLibraryDialogComponent>,
  	@Inject(MAT_DIALOG_DATA) public data: DialogData) {
  	this.media_thumb = environment.noimageDefault;
  }

  ngOnInit() {
  	this.mediaForm = this.fb.group({
      bgmediatype: this.fb.control('new_upload'),
      media: this.fb.control(''),
      newmedia: this.fb.control('', [Validators.required])
    });
    this.CheckRequiredFields();
  }

  CheckRequiredFields() {
    this.mediaForm.get('bgmediatype').valueChanges.subscribe((backtype: string) => {
      if (backtype === 'new_upload') {
        if(!this.imgUrl){
          this.mediaForm.get('newmedia').setValidators([Validators.required]);
        }
        this.mediaForm.get('media').clearValidators();
      }
      else if (backtype === 'ext_img') {
        this.getMedia(this.page, LIMIT);
        this.mediaForm.get('media').setValidators([Validators.required]);
        this.mediaForm.get('newmedia').clearValidators();
      }
      this.mediaForm.get('media').updateValueAndValidity();
      this.mediaForm.get('newmedia').updateValueAndValidity();
    });
  }

  //set Preview Image when user select input file
  setPreviewImage(fileInput:any) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      this.sharedService.returnBase64Image(fileInput.target.files[0]).subscribe((response: any) => {
        this.imgUrl = response;
      });
      this.mediaForm.get('newmedia').clearValidators();
      this.mediaForm.get('newmedia').updateValueAndValidity();
    }
  }

  getNext(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.getMedia(this.page, LIMIT);
  }

  getMedia(page, limit){
    this.medias = [];
    let type = 'slides';
    this.commonBridgeService.getMedia(page.toString(), limit.toString(), type)
      .subscribe(response =>
        {
          if(response.message == 'success'){
            this.medias = response.media.data;
            this.length = response.media.total;
          }
        },
        error => this.errorMsg = error);
  }

  submitMedia() {
    if(this.mediaForm.get('bgmediatype').value=='new_upload' && (this.el.nativeElement.files.length>0)) {
      let slideFormInfo = new FormData();
        slideFormInfo.append('type','slides');
        slideFormInfo.append('image',this.el.nativeElement.files[0]);
        this.commonBridgeService.saveMedia(slideFormInfo)
           .subscribe(response =>{
              if(response.status == 200){
                this.dialogRef.close(response.media.image);
              }
            },
         error => this.errorMsg = error);
    }
    else{
  	  this.dialogRef.close(this.mediaForm.get('media').value);
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  //trackBy
  trackByMedia(index: number, media: any): number {
    return media.media_id;
  }

}

export interface DialogData {
  media_url: string;
}