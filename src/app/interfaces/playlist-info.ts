export interface PlaylistInfo {
  playlist: {
    announcements:any,
    playlist_id: number,
    name: string,
    description: string,
    assign_calendar: string,
    scroll_announcement: string,
    footer_text: string,
    slug: string,
    slides: Array<slideSort>
  };
  status: number;
}

export interface slideSort {
  status: string;
  slide_id: number;
  order_by: number;
}

export interface PlaylistSection {
  playlist_id: string;
  name: String;
  slug: String;
  description: String;
  scroll_announcement: String;
  footer_text: String;
}

export interface SlideSection {
  slide_id: string;
  title: String;
  description: String;
  duration: String;
  start_datetime: Number;
  end_datetime: Number;
  cover_image: Text;
}