export interface TempWeatherInfo{
  city_name: string;
  temp: number;
  logo: string;
  footerColor: string;
  footerBgcolor: string;
  custom_city_name: string;
}