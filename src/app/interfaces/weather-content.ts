export interface WeatherContent {
  cod: number;
  name: string;
  id: number;
  dt: number;
  main: MainTempData;
}

export interface MainTempData{
  grnd_level: number;
  humidity: number;
  pressure: number;
  sea_level: number;
  temp: number;
  temp_max: number;
  temp_min: number;
}