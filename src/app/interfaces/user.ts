export class User {
    tokeninfo: {
    	user_id:number,
    	token:string,
    	created_at:string,
    	updated_at:string,
    };
    runtime:string;
    message:string;
    status:number;
}