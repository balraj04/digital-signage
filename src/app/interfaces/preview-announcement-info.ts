export interface PreviewAnnouncementInfo {
	text: string,
	scrolldelay: any,
	announcementColor: string,
    announcementBgcolor: string,
    announcements: string

}
