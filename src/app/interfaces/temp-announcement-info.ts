export interface TempAnnouncementInfo {
	text: string,
	scrolldelay: any,
	announcementColor: string,
    announcementBgcolor: string
}