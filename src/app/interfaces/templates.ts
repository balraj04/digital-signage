
export interface TemplatesComponent {
	templatedata: any;
	type: string;
	is_valide: string;
	community_footer: any;
	scroll_announcement: any;
}