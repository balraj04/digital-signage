import { Type } from '@angular/core';

export class TemplatesItem {
	constructor(public component: Type<any>, public content_data: any) {
	}
}