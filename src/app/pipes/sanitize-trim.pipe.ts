import { Pipe, PipeTransform } from '@angular/core';

 
@Pipe({
	name : "trimspace"
})
 
export class RemoveSpaces{
	transform(value){
		return value.replace(/ /g, "");
	}
}