import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/index';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, PageEvent, MatIconModule } from '@angular/material';
import { LogoutModalComponent } from '../../users/logout/logout-modal/logout-modal.component';
import { environment } from './../../../environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  private previousUrl: any = "/";
  public portalDashboard = environment.portalDashboard;
  constructor(
  	public dialog: MatDialog,
  	public authenticationService:AuthenticationService
  	) {}

  ngOnInit() {}
  
  openLogoutModal(){
  	const dialogRef = this.dialog.open(LogoutModalComponent, {disableClose: true, width: '50%',data:{previousUrl:this.previousUrl}});
  }
	
}
