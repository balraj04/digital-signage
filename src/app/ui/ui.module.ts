import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from '../material-module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { FullCalendarModule } from 'ng-fullcalendar';
import { LazyLoadImageModule, intersectionObserverPreset } from 'ng-lazyload-image';
import { AuthGuardService } from '../_guards/auth-guard.service';
import { SanitizeHtmlPipe } from '../pipes/sanitize-html.pipe';
import { Base64ImagPipe } from '../pipes/sanitize-base64.pipe';
import { RemoveSpaces } from '../pipes/sanitize-trim.pipe';

import { UsersComponent } from '../users/users.component';
import { LoginComponent } from '../users/login/login.component';
import { LogoutComponent } from '../users/logout/logout.component';
import { LogoutModalComponent } from '../users/logout/logout-modal/logout-modal.component';

import { LayoutComponent } from './layout/layout.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { PreviewFooterComponent } from '../slides/preview-footer/preview-footer.component';

import { LoaderComponent } from '../loader/loader.component';
import { ModalDialogComponent } from '../modal-dialog/modal-dialog.component';
import { DragModalComponent } from '../modal-dialog/drag-modal/drag-modal.component';
import { GlobalSearchComponent } from '../global-search/global-search.component';
import { SearchListComponent } from '../global-search/search-list/search-list.component';
import { HideSlideModalComponent } from '../hide-slide-modal/hide-slide-modal.component';
import { DeleteSlideModalComponent } from './../delete-slide-modal/delete-slide-modal.component';
import { PreviewDialogComponent } from '../preview/preview-dialog/preview-dialog.component';

import { HomeComponent } from '../home/home.component';
import { SlideComponent } from '../home/slide/slide.component';
import { DashboardComponent } from '../dashboard/dashboard.component';

import { PlaylistComponent } from '../playlist/playlist.component';
import { AddPlaylistComponent } from '../playlist/add-playlist/add-playlist.component';
import { EditPlaylistComponent } from '../playlist/edit-playlist/edit-playlist.component';
import { ScheduleDialogComponent } from '../playlist/edit-playlist/schedule-dialog/schedule-dialog.component';
import { DeletePlaylistModalComponent } from '../delete-playlist-modal/delete-playlist-modal.component';

import { SlidesComponent } from '../slides/slides.component';
import { AddSlideComponent } from '../slides/add-slide/add-slide.component';
import { EditSlideComponent } from '../slides/edit-slide/edit-slide.component';

import { PreviewComponent } from '../preview/preview.component';

import { PrintComponent } from '../print/print.component';
import { PrintDialogComponent } from '../print/print-dialog/print-dialog.component';

import { TemplatesComponent } from '../templates/templates.component';
import { TmplOneComponent } from '../templates/tmpl-one/tmpl-one.component';
import { TmplTwoComponent } from '../templates/tmpl-two/tmpl-two.component';
import { TmplThreeComponent } from '../templates/tmpl-three/tmpl-three.component';
import { TmplFourComponent } from '../templates/tmpl-four/tmpl-four.component';
import { TmplFiveComponent } from '../templates/tmpl-five/tmpl-five.component';
import { TmplSixComponent } from '../templates/tmpl-six/tmpl-six.component';
import { TmplSevenComponent } from '../templates/tmpl-seven/tmpl-seven.component';
import { TmplEventComponent } from '../templates/tmpl-event/tmpl-event.component';
import { TmplPdfComponent } from '../templates/tmpl-pdf/tmpl-pdf.component';
import { TmplVideoComponent } from '../templates/tmpl-video/tmpl-video.component';
import { TmplYoutubeComponent } from '../templates/tmpl-youtube/tmpl-youtube.component';

import { TmplEventWidgetComponent} from '../templates/tmpl-event-widget/tmpl-event-widget.component';
import { TmplAnnouncementWidgetComponent } from '../templates/tmpl-announcement-widget/tmpl-announcement-widget.component';

import { TmplEightComponent } from '../templates/tmpl-eight/tmpl-eight.component';
import { TmplNineComponent } from '../templates/tmpl-nine/tmpl-nine.component';

import { CommunityFooterComponent } from '../templates/community-footer/community-footer.component';
import { ScrollAnnouncementComponent } from '../templates/scroll-announcement/scroll-announcement.component';

import { SettingsComponent } from '../settings/settings.component';

import { MediaLibraryDialogComponent } from '../media-library-dialog/media-library-dialog.component';
import { ModelPdfPptComponent } from '../modal-dialog/model-pdf-ppt/model-pdf-ppt.component';
import { EventWidgetComponent } from '../modal-dialog/event-widget/event-widget.component';
import { AnnouncementWidgetComponent } from '../modal-dialog/announcement-widget/announcement-widget.component';

import { BackgroundImageLoadedDirective } from '../_directives/background-image-loaded.directive';
import { EventTemplateModalComponent } from '../modal-dialog/event-template-modal/event-template-modal.component';

import { SaveTemplatesModalComponent } from '../modal-dialog/save-templates-modal/save-templates-modal.component';
const appRoutes: Routes = [
   {
       path : "login",
       component: LoginComponent
   },
   {
       path : "logout",
       component: LogoutComponent,
       canActivate: [AuthGuardService]
   },
   {
       path : "home/:slug",
       component: HomeComponent
   },
   {
       path : "",
       component: DashboardComponent,
       canActivate: [AuthGuardService] 
   },
   {
       path : "playlist",
       component: PlaylistComponent,
       canActivate: [AuthGuardService] 
   },
   {
       path : "add-playlist",
       component: AddPlaylistComponent,
       canActivate: [AuthGuardService] 
   },
   {
       path : "edit-playlist/:id",
       component: EditPlaylistComponent,
       canActivate: [AuthGuardService] 
   },
   {
       path : "slides",
       component: SlidesComponent,
       canActivate: [AuthGuardService] 
   },
   {
       path : "add-slide",
       component: AddSlideComponent,
       canActivate: [AuthGuardService] 
   },
   {
       path : "edit-slide/:id",
       component: EditSlideComponent,
       canActivate: [AuthGuardService] 
   },
   {
       path : "preview",
       component: PreviewComponent,
       canActivate: [AuthGuardService] 
   },
   {
       path : "print",
       component: PrintComponent,
       canActivate: [AuthGuardService] 
   },
   {
       path : "templates",
       component: TemplatesComponent,
       canActivate: [AuthGuardService] 
   },
   {
       path : "footer",
       component: CommunityFooterComponent,
       canActivate: [AuthGuardService] 
   },
   {
       path : "settings",
       component: SettingsComponent,
       canActivate: [AuthGuardService] 
   },
   {
       path : "search",
       component: SearchListComponent,
       canActivate: [AuthGuardService] 
   },
   // otherwise redirect to home
   { path: '**', redirectTo: '' }
  ];
   
   
@NgModule({
  declarations: [
    UsersComponent,
    LoginComponent,
    LogoutComponent, 
    LogoutModalComponent,
    LayoutComponent, 
    HeaderComponent, 
    FooterComponent, 
    GlobalSearchComponent,
    SearchListComponent,
    LoaderComponent, 
    HomeComponent, 
    SlideComponent,
    DashboardComponent, 
    PlaylistComponent, 
    AddPlaylistComponent, 
    EditPlaylistComponent, 
    DeletePlaylistModalComponent,
    SlidesComponent, 
    AddSlideComponent,
    EditSlideComponent,
    HideSlideModalComponent,
    DeleteSlideModalComponent,
    PreviewComponent, 
    PrintComponent,
    PrintDialogComponent,
    ModalDialogComponent,
    DragModalComponent,
    ScheduleDialogComponent,
    PreviewDialogComponent,
    TemplatesComponent, 
    SettingsComponent,
    TmplOneComponent, 
    TmplTwoComponent, 
    TmplThreeComponent,
    TmplFourComponent,
    TmplFiveComponent,
    TmplSixComponent,
    TmplSevenComponent,
    TmplEventComponent,
    TmplPdfComponent,
    TmplVideoComponent,
    TmplYoutubeComponent,
    TmplEventWidgetComponent,
    TmplAnnouncementWidgetComponent,
    TmplEightComponent,
    TmplNineComponent,
    CommunityFooterComponent,
    ScrollAnnouncementComponent,
    MediaLibraryDialogComponent,
    ModelPdfPptComponent,
    SaveTemplatesModalComponent,
    EventWidgetComponent,
    AnnouncementWidgetComponent,
    EventTemplateModalComponent,
    SanitizeHtmlPipe,
    Base64ImagPipe,
    RemoveSpaces,
    BackgroundImageLoadedDirective,
    PreviewFooterComponent
  ],
  exports: [LayoutComponent],
  imports: [
    CommonModule,	
    BrowserModule,
    BrowserAnimationsModule,
    LazyLoadImageModule.forRoot({
      preset: intersectionObserverPreset
    }),
    FormsModule,
    ReactiveFormsModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    FullCalendarModule,
    AngularMaterialModule,
    RouterModule.forRoot(appRoutes,{ useHash: true })
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],

  entryComponents:[
    TmplOneComponent,
    TmplTwoComponent,
    TmplThreeComponent,
    TmplFourComponent,
    TmplFiveComponent,
    TmplSixComponent,
    TmplSevenComponent,
    TmplEventComponent,
    TmplPdfComponent,
    TmplVideoComponent,
    TmplYoutubeComponent,
    TmplEventWidgetComponent,
    TmplAnnouncementWidgetComponent,
    TmplEightComponent,
    TmplNineComponent,
    CommunityFooterComponent,
    ScrollAnnouncementComponent,
    ModalDialogComponent,
    DragModalComponent,
    PreviewDialogComponent,
    ScheduleDialogComponent,
    HideSlideModalComponent,
    DeleteSlideModalComponent,
    DeletePlaylistModalComponent,
    PrintDialogComponent,
    MediaLibraryDialogComponent,
    ModelPdfPptComponent,
    SaveTemplatesModalComponent,
    EventWidgetComponent,
    AnnouncementWidgetComponent,
    EventTemplateModalComponent,
    LogoutModalComponent //for logut warning show
  ]
})
export class UiModule { }
