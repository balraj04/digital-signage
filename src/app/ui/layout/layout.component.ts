import { Component, OnInit } from '@angular/core';
import { Event, Router, NavigationStart, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
	
    screenHeight: any;
    screenWidth: any;
    matDrawerContentWidth: any;
    toggleClass: string;
    toggleFlag: boolean;
    showLoadingIndicator: boolean = true;
    hideHF: boolean = false;


  	constructor(private _router: Router) {
      this._router.events.subscribe((routerEvent: Event) => {
        if(routerEvent instanceof NavigationStart){
          this.showLoadingIndicator = true;
        }

        if(routerEvent instanceof NavigationEnd){
          this.showLoadingIndicator = false;
          this.hideHF = (routerEvent.url.includes('home') || routerEvent.url.includes('login')) ? false : true;
        }
      });
    }

    onResize(event) {
      this.screenHeight = window.innerHeight;
    }

  	ngOnInit() {
      this.toggleClass = 'toggle-open';
      this.toggleFlag = false;
  		this.screenHeight = window.innerHeight;
      this.screenWidth = window.innerWidth;

      if ( ! this.hideHF ) {
        this.matDrawerContentWidth = 'calc( 100% - 250px )';
      } else {
        this.matDrawerContentWidth = '100%';
      }

  	}

    closeSideNav () {
      if ( this.toggleFlag ) {
        this.toggleClass = "toggle-open";
        this.toggleFlag = false;
      } else { 
        this.toggleClass = "toggle-close";
        this.toggleFlag = true;
      }
    }
}
