import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, RoutesRecognized } from "@angular/router";
import { filter, pairwise } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { User } from '../interfaces/index';

const API_URL = environment.apiUrl;

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    private previousUrl: any = "/";

    constructor(private http: HttpClient, private router: Router) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    Login(loginInfo: any) {
        return this.http.post<any>(API_URL + 'auth/login', loginInfo)
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.tokeninfo.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                }
                return user;
            }));
    }

    Logout() {

        let logoutInfo = {token:this.currentUserSubject.value.tokeninfo.token};
        return this.http.post<any>(API_URL + 'auth/logout', logoutInfo)
        .pipe(map(response => {
            
            if (response.logout===true) {
                // remove user from local storage to log user out
                localStorage.removeItem('currentUser');
                this.currentUserSubject.next(null);
            }
            return response;
        }));
    }
    getPreviousUrl() {
        return new Promise((resolve, reject) => {
            this.router.events.pipe(filter((e: any) => e instanceof RoutesRecognized),pairwise())
            .subscribe((e: any) => {
               resolve(e[0].urlAfterRedirects);
            });        
        });
    }
}