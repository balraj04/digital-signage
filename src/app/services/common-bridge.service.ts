import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { WeatherContent } from '../interfaces/index';
import { Subject } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { PlaylistInfo } from '../interfaces/playlist-info';

const HttpUploadOptions = {
  headers: new HttpHeaders({  "Accept":"multipart/form-data" })
}

const API_URL = environment.apiUrl;
const WEATHER_URL = environment.weatherUrl;

@Injectable({
  providedIn: 'root'
})
export class CommonBridgeService {

  private loaderSubject = new Subject<LoaderState>();
  loaderState = this.loaderSubject.asObservable();

  constructor(private http: HttpClient) { }
  //Get Droplist
  getDroplist(droptype): Observable<any>{
    return this.http.get<PlaylistArr>(API_URL + 'list/droplists', { params: { type: droptype } })
            .catch(this.errorHandler);
  }
  getPlaylist(page = null, limit = null): Observable<PlaylistArr>{
    
    let params = new HttpParams();
    if(page != null){
       params = params.set('page', page);
    }
    if(limit != null){
       params = params.set('limit', limit);
    }
  	return this.http.get<PlaylistArr>(API_URL + 'list/playlists', { params: params })
  					.catch(this.errorHandler);
  }

  getPlaylistInfo(playlist_id, datetime = null, front = false): Observable<PlaylistInfo>{

    let params = new HttpParams();
    params = params.set('front', front.toString());
    if(datetime != null){
      params = params.set('datetime', datetime.toString());
    }

    if(typeof playlist_id == 'number'){
      params = params.set('playlist_id', playlist_id.toString());
    }
    else{
      params = params.set('slug', playlist_id);
    }

    return this.http.get<PlaylistInfo>(API_URL + 'view/playlist', { params: params })
            .catch(this.errorHandler);
  }

  getSlideInfo(slide_id): Observable<any>{
    return this.http.get(API_URL + 'view/slide', { params: { slide_id: slide_id } })
            .catch(this.errorHandler);
  }

  getSavedTemplateInfo(Page, Limit, Searchkey, Sortby): Observable<any>{
    let params = new HttpParams();
    params = params.set('page', Page);
    params = params.set('limit', Limit);
    params = params.set('searchkey', Searchkey.toString());
    params = params.set('sortby', Sortby.toString());

    return this.http.get<any>(API_URL + 'list/templates', { params: params })
            .catch(this.errorHandler);
  }

  getDashboardDetails(): Observable<Dashboard>{
    return this.http.get<Dashboard>(API_URL + 'list/dashboard')
            .catch(this.errorHandler);
  }

  getSlidesList(playlist_id = null, page = null, limit = null, sortby = null): Observable<Slides>{

     let params = new HttpParams();
     if(playlist_id != null){
       params = params.set('playlist_id', playlist_id);
     }
     if(page != null){
       params = params.set('page', page);
     }
     if(limit != null){
       params = params.set('limit', limit);
     }
     if(sortby != null){
       params = params.set('sortby', sortby);
     }
    return this.http.get<Slides>(API_URL + 'list/slides', { params: params })
            .catch(this.errorHandler);
  }

  savePlaylist(playlistData: any){
    let body = new FormData();
    body.append('name', playlistData.name);
    body.append('description', playlistData.description);
    body.append('scroll_announcement', playlistData.scroll_announcement);
    body.append('footer_text', playlistData.footer_text);
    return this.http.post<any>(API_URL + 'create/playlist', body)
            .catch(this.errorHandler);
  }

  updatePlaylist(playlistData: any){
    let body = new FormData();
    body.append('playlist_id', playlistData.playlist_id);
    body.append('name', playlistData.name);
    body.append('description', playlistData.description);
    body.append('assign_calendar', playlistData.assign_calendar);
    body.append('scroll_announcement', playlistData.scroll_announcement);
    body.append('footer_text', playlistData.footer_text);
    return this.http.post<any>(API_URL + 'update/playlist', body)
            .catch(this.errorHandler);
  }

  // SAVE AS NEW TEMPLATE CALL 
  saveAsTemplate(TemplateData:any){
     return this.http.post<any>(API_URL + 'create/template', TemplateData, HttpUploadOptions)
            .catch(this.errorHandler);
  }

  //Copy Slide With Playlist And Content Data 
  copySlide(slide_id): Observable<any>{
    
    return this.http.post<any>(API_URL + 'copy/slide', { slide_id: slide_id }, HttpUploadOptions )
            .catch(this.errorHandler);

    /*return this.http.get(API_URL + 'view/slide', { params: { slide_id: slide_id } })
            .catch(this.errorHandler);*/
  }

  //Save Slide With Playlist And Content Data 
  saveSlide(SlideData: any){
    
    return this.http.post<any>(API_URL + 'create/slide', SlideData, HttpUploadOptions)
            .catch(this.errorHandler);
  }

  // Update Slide with Playlist And Content Data
  updateSlide(SlideData: any){
    
    return this.http.post<any>(API_URL + 'update/slide', SlideData, HttpUploadOptions)
            .catch(this.errorHandler);
  }
  //Global Search Query MAS
  getSearchResults(searchkey, playlist_id, template, start_datetime, end_datetime): Observable<SearchArr>{
    let params = new HttpParams();
     params = params.set('searchkey', searchkey);     
     if(playlist_id[0] != null){
       params = params.set('playlist_id', playlist_id);
     }
     if(template[0] != null){
       params = params.set('template_id', template);
     }
     if(start_datetime != 0){
       params = params.set('start_datetime', start_datetime);
     }
     if(end_datetime != 0){
       params = params.set('end_datetime', end_datetime);
     }
    return this.http.get<SearchArr>(API_URL + 'list/search', { params: params })
            .catch(this.errorHandler);
  }
  updateScheduledSlide(scheduleInfo): Observable<any>{
    return this.http.post<any>(API_URL + 'update/schedule', scheduleInfo)
            .catch(this.errorHandler);
  }

  updateSlideOrder(slideData: []){
    return this.http.post<any>(API_URL + 'update/slideorder', slideData)
            .catch(this.errorHandler);
  }

  deleteSlide(slide_id){
    return this.http.post<any>(API_URL + 'delete/slide', { slide_id: slide_id })
            .catch(this.errorHandler);
  }
  //*MAS Crescente Code
  deleteTemplate(template_info){
    return this.http.post<any>(API_URL + 'delete/template', template_info)
            .catch(this.errorHandler);
  }

  deletePlaylist(playlist_id){
    return this.http.post<any>(API_URL + 'delete/playlist', { playlist_id: playlist_id })
            .catch(this.errorHandler);
  }

  hideShowSlide(slide_id, status){
    return this.http.post<any>(API_URL + 'update/slidestatus', { slide_id: slide_id, status: status })
            .catch(this.errorHandler);
  }

  getWeatherInfo(app_id, city_id, unit): Observable<WeatherContent>{
    let params = new HttpParams();
    params = params.set('id', city_id.toString());
    params = params.set('appid', app_id.toString());
    params = params.set('units', unit.toString());
    return this.http.get<WeatherContent>(WEATHER_URL, { params: params })
            .catch(this.errorHandler);
  }
  
  //Save Settings
  saveSettings(SettingData: any){
    return this.http.post<any>(API_URL + 'update/settings', SettingData, HttpUploadOptions)
            .catch(this.errorHandler);
  }

  //Get Settings 
  getSettings(front = false): Observable<any>{
    let params = new HttpParams();
    params = params.set('front', front.toString());
    return this.http.get<any>(API_URL + 'list/settings', { params: params })
           .catch(this.errorHandler);
  }  

  //Save Media
  saveMedia(mediaData: any){
    return this.http.post<any>(API_URL + 'media/upload', mediaData, HttpUploadOptions)
           .catch(this.errorHandler);
  }  

  getMedia(page = null, limit = null,  type = null): Observable<any>{

     let params = new HttpParams();
     if(page != null){
       params = params.set('page', page);
     }
     if(limit != null){
       params = params.set('limit', limit);
     }
     if(type != null){
       params = params.set('type', type);
     }
     
    return this.http.get<any>(API_URL + 'media/list', { params: params })
            .catch(this.errorHandler);
  }

  //Save PDF or PPT
  savePDFPPT(mediaData: any){
    return this.http.post<any>(API_URL + 'media/upload', mediaData, HttpUploadOptions)
           .catch(this.errorHandler);
  }

  getPDFPPTMedia(page = null, limit = null, type = null): Observable<any>{

     let params = new HttpParams();
     if(page != null){
       params = params.set('page', page);
     }
     if(limit != null){
       params = params.set('limit', limit);
     }
     if(type != null){
       params = params.set('type', type);
     }
     
    return this.http.get<any>(API_URL + 'media/list', { params: params })
            .catch(this.errorHandler);
  }

  errorHandler(error: HttpErrorResponse){
  	return Observable.throw(error.message || "Server Error");
  }

  show() {
    this.loaderSubject.next(<LoaderState>{ show: true });
  }
  hide() {
    this.loaderSubject.next(<LoaderState>{ show: false });
  }
}

export interface PlaylistArr {
  status: number;
  message: string;
  playlists: {
    data: [],
    total: number
  };
}

export interface Dashboard {
  status: number;
  message: string;
  playlists: number;
  slides: number;
  templates: number;
}

export interface Slides {
  announcements: any;
  status: number;
  message: string;
  slides: {
    data: [],
    total: number
  }
}
export interface SearchArr {
  status: number;
  message: string;
  playlists: [];
  slides: [];
}
export interface LoaderState {
  show: boolean;
}