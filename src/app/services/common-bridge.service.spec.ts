import { TestBed } from '@angular/core/testing';

import { CommonBridgeService } from './common-bridge.service';

describe('CommonBridgeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommonBridgeService = TestBed.get(CommonBridgeService);
    expect(service).toBeTruthy();
  });
});
