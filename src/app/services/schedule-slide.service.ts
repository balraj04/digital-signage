import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SharedService } from './shared.service';

import 'rxjs/add/observable/of';

@Injectable({
  providedIn: 'root'
})
export class ScheduleSlideService {

	constructor(
		private sharedService: SharedService){ }

    public getScheduledSlides(slides): Observable<any> {
    	let self = this;
    	let data: any = [];
    	slides.forEach(function (slide) {
		  if(slide.scheduled == 'Y'){

		    slide.daysofweek = (slide.daysofweek) ? slide.daysofweek.toString().split(",") : [];
		    for(var i=0; i<slide.daysofweek.length;i++) slide.daysofweek[i] = +slide.daysofweek[i];

		    if(slide.schedule_type == 'A'){
		    	let start = slide.start_datetime;
			  	let end = slide.end_datetime;
			  	
			  	var startO = new Date(parseInt(start));
			    var endO = new Date(parseInt(end));
			    
			    let startd = startO.toISOString();
			    let endd = endO.toISOString();
			    var allDayVar = false;
			    var shours = startO.getHours();
			    var sminutes = startO.getMinutes();
			    var sseconds = startO.getSeconds();
			    var shoursUtc = startO.getUTCHours();
			    var sminutesUtc = startO.getUTCMinutes();
			    var ehours = endO.getHours();
			    var eminutes = endO.getMinutes();
			    var eseconds = endO.getSeconds();
			    var ehoursUtc = endO.getUTCHours();
			    var eminutesUtc = endO.getUTCMinutes();
			    if ((shours == 0 && sminutes == 0 && sseconds == 0) && ((ehours == 0 && eminutes == 0 && eseconds == 0) || (ehours == 23 && eminutes == 59 && eseconds == 59))) {
			        allDayVar = true
			    }
			    var endEvent = endd;
			    if ((endO.getTime() / 1000) - (startO.getTime() / 1000) > 86400 && allDayVar == true) {
			        endEvent = new Date((endO.getTime() + 86400000)).toISOString()
			    }

		    	data.push({
		    		'slide_id':  slide.slide_id,
		    		'type': 'A',
		    		'editable': false,
		    		'title': slide.title,
		    		'start': shours + ':' + sminutes,
            		'end': ehours + ':' + eminutes,
		    		'allDay': allDayVar,
		    		'dow': slide.daysofweek,
		    		'dowstart': startd,
		    		'dowend': endEvent
		    	});
		    }
		    else if(slide.schedule_type == 'M'){
		    	if(slide.template_id == 'tmpl-event'){
		    		let start = slide.start_datetime;
				  	let end = slide.end_datetime;
				  	var startO = new Date(parseInt(start));
				    var endO = new Date(parseInt(end));
				    let startd = startO.toISOString();
				    let endd = endO.toISOString();
				    var allDayVar = false;
				    var shours = startO.getHours();
				    var sminutes = startO.getMinutes();
				    var sseconds = startO.getSeconds();
				    var shoursUtc = startO.getUTCHours();
				    var sminutesUtc = startO.getUTCMinutes();
				    var ehours = endO.getHours();
				    var eminutes = endO.getMinutes();
				    var eseconds = endO.getSeconds();
				    var ehoursUtc = endO.getUTCHours();
				    var eminutesUtc = endO.getUTCMinutes();
				    if ((shours == 0 && sminutes == 0 && sseconds == 0) && ((ehours == 0 && eminutes == 0 && eseconds == 0) || (ehours == 23 && eminutes == 59 && eseconds == 59))) {
				        allDayVar = true
				    }
				    var endEvent = endd;
				    if ((endO.getTime() / 1000) - (startO.getTime() / 1000) > 86400 && allDayVar == true) {
				        endEvent = new Date((endO.getTime() + 86400000)).toISOString()
				    }
				  	data.push({
			    		'slide_id':  slide.slide_id,
			    		'type': 'M',
			    		'index_id': 1,
			    		'title': slide.title,
			    		'start': startd,
			    		'end': endEvent,
			    		'allDay': allDayVar
			    	});
		    	}
		    	else{

			    	slide.scheduling.forEach(function (obj, index) {
			    		let start = obj.start_datetime;
					  	let end = obj.end_datetime;
					  	var startO = new Date(parseInt(start));
					    var endO = new Date(parseInt(end));
					    let startd = startO.toISOString();
					    let endd = endO.toISOString();
					    var allDayVar = false;
					    var shours = startO.getHours();
					    var sminutes = startO.getMinutes();
					    var sseconds = startO.getSeconds();
					    var shoursUtc = startO.getUTCHours();
					    var sminutesUtc = startO.getUTCMinutes();
					    var ehours = endO.getHours();
					    var eminutes = endO.getMinutes();
					    var eseconds = endO.getSeconds();
					    var ehoursUtc = endO.getUTCHours();
					    var eminutesUtc = endO.getUTCMinutes();
					    if ((shours == 0 && sminutes == 0 && sseconds == 0) && ((ehours == 0 && eminutes == 0 && eseconds == 0) || (ehours == 23 && eminutes == 59 && eseconds == 59))) {
					        allDayVar = true
					    }
					    var endEvent = endd;
					    if ((endO.getTime() / 1000) - (startO.getTime() / 1000) > 86400 && allDayVar == true) {
					        endEvent = new Date((endO.getTime() + 86400000)).toISOString()
					    }
					  	data.push({
				    		'slide_id':  slide.slide_id,
				    		'type': 'O',
				    		'index_id': index,
				    		'title': slide.title,
				    		'start': startd,
				    		'end': endEvent,
				    		'allDay': allDayVar
				    	});
					});
		    	}
		    }
		    else{
		    	let start = slide.start_datetime;
			  	let end = slide.end_datetime;
			  	var startO = new Date(parseInt(start));
			    var endO = new Date(parseInt(end));
			    let startd = startO.toISOString();
			    let endd = endO.toISOString();
			    var allDayVar = false;
			    var shours = startO.getHours();
			    var sminutes = startO.getMinutes();
			    var sseconds = startO.getSeconds();
			    var shoursUtc = startO.getUTCHours();
			    var sminutesUtc = startO.getUTCMinutes();
			    var ehours = endO.getHours();
			    var eminutes = endO.getMinutes();
			    var eseconds = endO.getSeconds();
			    var ehoursUtc = endO.getUTCHours();
			    var eminutesUtc = endO.getUTCMinutes();
			    if ((shours == 0 && sminutes == 0 && sseconds == 0) && ((ehours == 0 && eminutes == 0 && eseconds == 0) || (ehours == 23 && eminutes == 59 && eseconds == 59))) {
			        allDayVar = true
			    }
			    var endEvent = endd;
			    if ((endO.getTime() / 1000) - (startO.getTime() / 1000) > 86400 && allDayVar == true) {
			        endEvent = new Date((endO.getTime() + 86400000)).toISOString()
			    }
				data.push({
		    		'slide_id':  slide.slide_id,
		    		'type': 'O',
		    		'title': slide.title,
		    		'start': startd,
		    		'end': endEvent,
		    		'allDay': allDayVar
		    	});
		    }
		  }
		});
        return Observable.of(data);
    }

    public updateScheduledSlides(slides): Observable<any> {
    	let self = this;
    	let update: any = [];

    	slides.forEach(function (slide, index) {
		    if(slide.schedule_type == 'A'){
		    	let start = slide.start_datetime;
			  	let end = slide.end_datetime;
			  	
			  	var startO = new Date(parseInt(start));
			    var endO = new Date(parseInt(end));
			    let startd = startO.toISOString();
			    let endd = endO.toISOString();
			    var allDayVar = false;
			    var shours = startO.getHours();
			    var sminutes = startO.getMinutes();
			    var sseconds = startO.getSeconds();
			    var shoursUtc = startO.getUTCHours();
			    var sminutesUtc = startO.getUTCMinutes();
			    var ehours = endO.getHours();
			    var eminutes = endO.getMinutes();
			    var eseconds = endO.getSeconds();
			    var ehoursUtc = endO.getUTCHours();
			    var eminutesUtc = endO.getUTCMinutes();
			    if ((shours == 0 && sminutes == 0 && sseconds == 0) && ((ehours == 0 && eminutes == 0 && eseconds == 0) || (ehours == 23 && eminutes == 59 && eseconds == 59))) {
			        allDayVar = true
			    }
			    var endEvent = endd;
			    if ((endO.getTime() / 1000) - (startO.getTime() / 1000) > 86400 && allDayVar == true) {
			        endEvent = new Date((endO.getTime() + 86400000)).toISOString()
			    }

		    	update.push({
		    		'slide_id':  slide.slide_id,
		    		'type': 'A',
	    			'editable': false,
		    		'title': slide.title,
		    		'start': shours + ':' + sminutes,
            		'end': ehours + ':' + eminutes,
		    		'allDay': allDayVar,
		    		'dow': slide.daysofweek,
		    		'dowstart': startd,
		    		'dowend': endEvent
		    	});
			}
	    	else if(slide.schedule_type == 'M'){
	    		if(slide.template_id == 'tmpl-event'){
	    			let start = slide.start_datetime;
				  	let end = slide.end_datetime;
				  	var startO = new Date(parseInt(start));
				    var endO = new Date(parseInt(end));
				    let startd = startO.toISOString();
				    let endd = endO.toISOString();
				    var allDayVar = false;
				    var shours = startO.getHours();
				    var sminutes = startO.getMinutes();
				    var sseconds = startO.getSeconds();
				    var shoursUtc = startO.getUTCHours();
				    var sminutesUtc = startO.getUTCMinutes();
				    var ehours = endO.getHours();
				    var eminutes = endO.getMinutes();
				    var eseconds = endO.getSeconds();
				    var ehoursUtc = endO.getUTCHours();
				    var eminutesUtc = endO.getUTCMinutes();
				    if ((shours == 0 && sminutes == 0 && sseconds == 0) && ((ehours == 0 && eminutes == 0 && eseconds == 0) || (ehours == 23 && eminutes == 59 && eseconds == 59))) {
				        allDayVar = true
				    }
				    var endEvent = endd;
				    if ((endO.getTime() / 1000) - (startO.getTime() / 1000) > 86400 && allDayVar == true) {
				        endEvent = new Date((endO.getTime() + 86400000)).toISOString()
				    }

				    update.push({
			    		'slide_id':  slide.slide_id,
			    		'type': 'M',
			    		'index_id': 1,
			    		'title': slide.title,
			    		'start': startd,
			    		'end': endEvent,
			    		'allDay': allDayVar
			    	});
	    		}
	    		else{
		    		slide.scheduling.forEach(function (obj, index) {
			    		let start = obj.start_datetime;
					  	let end = obj.end_datetime;
					  	var startO = new Date(parseInt(start));
					    var endO = new Date(parseInt(end));
					    let startd = startO.toISOString();
					    let endd = endO.toISOString();
					    var allDayVar = false;
					    var shours = startO.getHours();
					    var sminutes = startO.getMinutes();
					    var sseconds = startO.getSeconds();
					    var shoursUtc = startO.getUTCHours();
					    var sminutesUtc = startO.getUTCMinutes();
					    var ehours = endO.getHours();
					    var eminutes = endO.getMinutes();
					    var eseconds = endO.getSeconds();
					    var ehoursUtc = endO.getUTCHours();
					    var eminutesUtc = endO.getUTCMinutes();
					    if ((shours == 0 && sminutes == 0 && sseconds == 0) && ((ehours == 0 && eminutes == 0 && eseconds == 0) || (ehours == 23 && eminutes == 59 && eseconds == 59))) {
					        allDayVar = true
					    }
					    var endEvent = endd;
					    if ((endO.getTime() / 1000) - (startO.getTime() / 1000) > 86400 && allDayVar == true) {
					        endEvent = new Date((endO.getTime() + 86400000)).toISOString()
					    }

					    update.push({
				    		'slide_id':  slide.slide_id,
				    		'type': 'M',
				    		'index_id': index,
				    		'title': slide.title,
				    		'start': startd,
				    		'end': endEvent,
				    		'allDay': allDayVar
				    	});
					});
	    		}
	    	}
		    else{
		    	let start = slide.start;
			  	let end = slide.end;
			  	var startO = new Date(parseInt(start));
			    var endO = new Date(parseInt(end));
			    let startd = startO.toISOString();
			    let endd = endO.toISOString();
			    var allDayVar = false;
			    var shours = startO.getHours();
			    var sminutes = startO.getMinutes();
			    var sseconds = startO.getSeconds();
			    var shoursUtc = startO.getUTCHours();
			    var sminutesUtc = startO.getUTCMinutes();
			    var ehours = endO.getHours();
			    var eminutes = endO.getMinutes();
			    var eseconds = endO.getSeconds();
			    var ehoursUtc = endO.getUTCHours();
			    var eminutesUtc = endO.getUTCMinutes();
			    if ((shours == 0 && sminutes == 0 && sseconds == 0) && ((ehours == 0 && eminutes == 0 && eseconds == 0) || (ehours == 23 && eminutes == 59 && eseconds == 59))) {
			        allDayVar = true
			    }
			    var endEvent = endd;
			    if ((endO.getTime() / 1000) - (startO.getTime() / 1000) > 86400 && allDayVar == true) {
			        endEvent = new Date((endO.getTime() + 86400000)).toISOString()
			    }
				update.push({
		    		'slide_id':  slide.slide_id,
		    		'type': 'O',
		    		'title': slide.title,
		    		'start': startd,
		    		'end': endEvent,
		    		'allDay': allDayVar
		    	});
		    }
		});
        return Observable.of(update);
    }
};