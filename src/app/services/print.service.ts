import { Injectable } from '@angular/core';
import { TemplatesItem } from '../templates/templates-item';
import { TmplOneComponent } from '../templates/tmpl-one/tmpl-one.component';
import { TmplTwoComponent } from '../templates/tmpl-two/tmpl-two.component';
import { TmplThreeComponent } from '../templates/tmpl-three/tmpl-three.component';
import { TmplFourComponent } from '../templates/tmpl-four/tmpl-four.component';
import { TmplFiveComponent } from '../templates/tmpl-five/tmpl-five.component';
import { TmplSixComponent } from '../templates/tmpl-six/tmpl-six.component';
import { TmplSevenComponent } from '../templates/tmpl-seven/tmpl-seven.component';
import { TmplEventComponent } from '../templates/tmpl-event/tmpl-event.component';
import { TmplPdfComponent } from '../templates/tmpl-pdf/tmpl-pdf.component';
import { TmplVideoComponent } from '../templates/tmpl-video/tmpl-video.component';
import { TmplYoutubeComponent } from '../templates/tmpl-youtube/tmpl-youtube.component';
import { ScrollAnnouncementComponent } from '../templates/scroll-announcement/scroll-announcement.component';
import { CommunityFooterComponent } from '../templates/community-footer/community-footer.component';
import { TmplEventWidgetComponent } from '../templates/tmpl-event-widget/tmpl-event-widget.component';
import { TmplAnnouncementWidgetComponent } from '../templates/tmpl-announcement-widget/tmpl-announcement-widget.component';
import { TmplEightComponent } from '../templates/tmpl-eight/tmpl-eight.component';
import { TmplNineComponent } from '../templates/tmpl-nine/tmpl-nine.component';

@Injectable({
  providedIn: 'root'
})
export class PrintService {

  constructor() { }

  getComponents(filtered) : TemplatesItem[] {
    let result: TemplatesItem[] = [];
    
    for (let item of filtered) {
      if(item.status == 'A'){
        let comp = this.resolveComponentsName(item.template_id);
        let newItem = new TemplatesItem(comp, item, 'print');
        result.push(newItem);
      }
    }

    return result;
  }

  private resolveComponentsName(template_id){
    if(template_id == "tmpl-one"){
      return TmplOneComponent;
    } else if(template_id == "tmpl-two"){
      return TmplTwoComponent;
    } else if(template_id == "tmpl-three"){
      return TmplThreeComponent;
    } else if(template_id == "tmpl-four"){
      return TmplFourComponent;
    } else if(template_id == "tmpl-five"){
      return TmplFiveComponent;
    } else if(template_id == "tmpl-six"){
      return TmplSixComponent;
    } else if(template_id == "tmpl-seven"){
      return TmplSevenComponent;
    } else if(template_id == "tmpl-event"){
      return TmplEventComponent;
    } else if(template_id == "tmpl-announcement"){
      return ScrollAnnouncementComponent;
    } else if(template_id == "tmpl-footer"){
      return CommunityFooterComponent;
    } else if(template_id == "tmpl-pdf"){
      return TmplPdfComponent;
    } else if(template_id == "tmpl-video"){
      return TmplVideoComponent;
    } else if(template_id == "tmpl-youtube"){
      return TmplYoutubeComponent;
    } else if(template_id == "tmpl-event-widget"){
      return TmplEventWidgetComponent;
    } else if(template_id == "tmpl-announcement-widget"){
      return TmplAnnouncementWidgetComponent;
    } else if(template_id == "tmpl-eight"){
      return TmplEightComponent;
    } else if(template_id == "tmpl-nine"){
      return TmplNineComponent;
    }

  }

  printPages(printContents){
    let popupWin;

    const linksHtml = this.getTagsHtml('link');
    const stylesHtml = this.getTagsHtml('style');

    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title></title>
          ${linksHtml}
          ${stylesHtml}
          <style>
            app-tmpl-pdf, app-tmpl-video, app-tmpl-youtube{ display: none; }
            // .print_modal .print-div { height: 100vh; width: 100%; margin: 0px auto; }
            .print_modal2 { transform: translate(-50%, 50%) scale(2) !important; width: 50%; height: 50%; }
            .print_modal .print-div .full-height .row .col-sm-4, 
            .print_modal .print-div .full-height .row .col-sm-8 { height: auto !important; }
            .print-div .font-slide { font-size: 2.3vh !important; line-height: 3.3vh !important; }
            .print-div .font-lg { font-size: 2.0vh !important; line-height: 2.5vh !important; }
            .print-div .font-md { font-size: 1.8vh !important; line-height: 2.3vh !important; }
            .print-div .font-sm { font-size: 1.6vh !important; line-height: 2.1vh !important; }
            .print-div .font-xs { font-size: 1.2vh !important; line-height: 1.7vh !important; }
           </style>
        </head>
        <body onload="window.focus();window.print();setTimeout(function(){window.close();},1000);">
          <div class="print_modal">
            <mat-drawer-container>
              ${printContents}
            </mat-drawer-container>
          </div>
        </body>
      </html>`
    );
    popupWin.document.close();
    // setTimeout(function(){popupWin.close();},1000);
  }

  private getTagsHtml(tagName: keyof HTMLElementTagNameMap): string
  {
    const htmlStr: string[] = [];
    const elements = document.getElementsByTagName(tagName);
    for (let idx = 0; idx < elements.length; idx++)
    {
        htmlStr.push(elements[idx].outerHTML);
    }
    return htmlStr.join('\r\n');
  }
}