import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  
  SharedData = new EventEmitter<any>();
  SlideData = new EventEmitter<any>();
  RouterUrl = new EventEmitter<any>();
  FooterDisplay = new EventEmitter<any>();
  tempEditData : any;

  private EditSlideData = new BehaviorSubject<any>(this.tempEditData);
  currEditData = this.EditSlideData.asObservable();
  
  convertedData: any = {};

  constructor(private http: HttpClient) {}

  changeEditData(editData : any) {
    if (editData) {
      this.EditSlideData.next(editData);
    }
    else {
      this.EditSlideData.next(null);
    }
  }

  sendSharedJson(sharedata=null){
    if(sharedata.get('SettingsJson'))
    {
      this.convertedData.settings = JSON.parse(sharedata.get('SettingsJson'));      
    }
    return Observable.create(observer => {
        this.convertedData
        observer.next(this.convertedData);
        observer.complete();
    });
  }

  formatDate(date) {
      var d = new Date(date),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return [year, month, day].join('-');
  }

  dateToTimestamp(dt){
    let millitime = new Date(dt).setSeconds(0);
    return millitime;
  }

  calendarStartTimestamp(start){
    return (parseInt(start.format('X')) + (60 * (new Date()).getTimezoneOffset())) * 1000;
  }

  calendarEndTimestamp(end, allDay){
    var endds = new Date(end + (60 * 1000 * (new Date()).getTimezoneOffset()));
    if (allDay == true && endds.getHours() == 0 && endds.getMinutes() == 0 && endds.getSeconds() == 0) {
        endds = new Date(endds.getTime() - 1000)
    }
    return endds.getTime();
  }
  
  //Get Base64 of Image File
  returnBase64Image(file:File){
    if(file)
    {
      var file:File = file;
      var imageReader:FileReader = new FileReader();
      imageReader.readAsDataURL(file);
      
      return Observable.create(observer => {
        imageReader.onloadend = () => {
          var base64image = imageReader.result;
          observer.next(base64image);
          observer.complete();
        };
      });
    }
    else
    {
      return Observable.create(observer => {
          var base64image = '';
          observer.next(base64image);
          observer.complete();
      });
    }
  }
}