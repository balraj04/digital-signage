import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { CommonBridgeService } from './common-bridge.service';
import { environment } from '../../environments/environment';
import { AuthenticationService } from './authentication.service';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';

const WEATHER_URL = environment.weatherUrl;

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {
   authToken: any;
   constructor(private router: Router,private commonBridgeService: CommonBridgeService,private authenticationService: AuthenticationService) {
	
   }

   	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    	this.showLoader();
    	let changedRequest = request;
	    const headerSettings: {[name: string]: string | string[]; } = {};
	    for (const key of request.headers.keys()) {
	      headerSettings[key] = request.headers.getAll(key);
	    }

	    this.authToken = this.authenticationService.currentUserValue ? this.authenticationService.currentUserValue['tokeninfo'].token : "";
	    if (this.authToken) {
	      headerSettings['Authorization'] = 'Bearer ' + this.authToken;
	    }
	    if(changedRequest.url != WEATHER_URL){
		    const newHeader = new HttpHeaders(headerSettings);
		    changedRequest = request.clone({headers: newHeader});
	    }

	    return next.handle(changedRequest).pipe(tap((event: HttpEvent<any>) => { 
	      if (event instanceof HttpResponse) {
	      	if(event.body.errorcode==391)
	      	{
	      	this.router.navigate(['/logout']);
	      	}        
	        this.onEnd();
	      }
	    },
	      (err: any) => {
	        this.onEnd();
	    }));
	}
	
	private onEnd(): void {
		this.hideLoader();
	}
	
	private showLoader(): void {
		this.commonBridgeService.show();
	}

	private hideLoader(): void {
		this.commonBridgeService.hide();
	}
}