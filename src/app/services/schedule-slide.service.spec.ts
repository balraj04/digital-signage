import { TestBed } from '@angular/core/testing';

import { ScheduleSlideService } from './schedule-slide.service';

describe('ScheduleSlideService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ScheduleSlideService = TestBed.get(ScheduleSlideService);
    expect(service).toBeTruthy();
  });
});
