import { Component, OnInit } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, PageEvent } from '@angular/material';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';
import { CommonBridgeService, SharedService, PrintService } from './../services/index';
import { PlaylistInfo, TempWeatherInfo, TempAnnouncementInfo } from './../interfaces/index';
import { environment } from '../../environments/environment';
import { PreviewDialogComponent } from './../preview/preview-dialog/preview-dialog.component';
import { DeletePlaylistModalComponent } from '../delete-playlist-modal/delete-playlist-modal.component';

const LIMIT = environment.limit;

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.css']
})
export class PlaylistComponent implements OnInit {

  title: string;

  playlists = [];
  errorMsg: any;

  // MatPaginator Inputs
  length: number = 0;
  pageSize = LIMIT;
  page: number = 1;
  currUrl: string;
  playlistURL: string = environment.playlistURL;

  // MatPaginator Output
  pageEvent: PageEvent;

  playlistInfo: PlaylistInfo;
  tempWeather: TempWeatherInfo = {
    city_name: '',
    temp: 0,
    logo: '',
    footerColor: '',
    footerBgcolor: '',
    custom_city_name: ''
  };

  tempAnnouncement: TempAnnouncementInfo = {
    text: '',
    scrolldelay: 0,
    announcementColor: '',
    announcementBgcolor: ''
  };

  // Snackbar Posittion
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private commonBridgeService: CommonBridgeService,
    protected sharedService: SharedService,
    private printService: PrintService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private platformLocation: PlatformLocation
    ) {
  	this.title = 'Playlists';    
    this.currUrl = (this.platformLocation as any).location.origin;
  }

  ngOnInit() {
  	this.getPlaylists(this.page, LIMIT);
  }

  getNext(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.getPlaylists(this.page, LIMIT);
  }

  getPlaylists(page, limit){
    this.commonBridgeService.getPlaylist(page.toString(), limit.toString())
      .subscribe(response =>
        {
          if(response.message == 'success'){
            this.playlists = response.playlists.data;
            this.length = response.playlists.total;
          }
        },
        error => this.errorMsg = error);
  }

  previewPlaylistsInfo(playlist_id){
    let datetime = new Date().getTime();
    this.commonBridgeService.getPlaylistInfo(+playlist_id, datetime)
      .subscribe(response =>
                            {
                              if(response.status == 200){
                                this.playlistInfo = response;
                                if(this.playlistInfo.playlist.slides.length){
                                  this.configComponent();
                                }
                                else{
                                  this.snackBar.open('No slides available', 'Error', {
                                    duration: 4000,
                                    horizontalPosition: this.horizontalPosition,
                                    verticalPosition: this.verticalPosition,
                                    panelClass: ['cust-snackbar', 'not-submitted'],
                                  });
                                }
                              }
                            },
                error => this.errorMsg = error);
  }

  openDelteDialog(event): void {
    const dialogRef = this.dialog.open(DeletePlaylistModalComponent, {
      width: '50%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result == 'Y'){
        this.deletePlaylist(event);
      }
    });
  }

  deletePlaylist(playlist_id){
    this.commonBridgeService.deletePlaylist(+playlist_id)
      .subscribe(response =>
                            {
                              if(response.status == 200 && response.delete){
                                this.snackBar.open('Playlist deleted successfully !', 'Success', {
                                  duration: 4000,
                                  horizontalPosition: this.horizontalPosition,
                                  verticalPosition: this.verticalPosition,
                                  panelClass: ['cust-snackbar', 'submitted'],
                                });
                                let updateItem = this.playlists.find(this.findIndexToUpdate, playlist_id);
                                let index = this.playlists.indexOf(updateItem);
                                if (index !== -1) {
                                  this.playlists.splice(index, 1);
                                }
                              }
                              else{
                                this.snackBar.open('Unable to delete playlist', 'Error', {
                                  duration: 4000,
                                  horizontalPosition: this.horizontalPosition,
                                  verticalPosition: this.verticalPosition,
                                  panelClass: ['cust-snackbar', 'not-submitted'],
                                });
                              }
                            },
                error => this.errorMsg = error);
  }

  findIndexToUpdate(newItem) { 
    return newItem.playlist_id === this;
  }

  configComponent(){
    if(this.playlistInfo.playlist.footer_text == 'Y' || this.playlistInfo.playlist.scroll_announcement == 'Y'){
      this.getFooterSettings();
    }
    else{
      this.loadComponent();
    }
  }

  loadComponent(){
    let infos = this.printService.getComponents(this.playlistInfo.playlist.slides);
    if(infos.length){
      for (var i = infos.length - 1; i >= 0; i--) {
        this.tempAnnouncement.text = (this.playlistInfo.playlist.scroll_announcement == 'Y') ? this.playlistInfo.playlist.announcements : '';
        infos[i].content_data['community_footer'] = {'status': this.playlistInfo.playlist.footer_text, 'content': this.tempWeather};
        infos[i].content_data['scroll_announcement'] = {'status': this.playlistInfo.playlist.scroll_announcement, 'content': this.tempAnnouncement};
      }
      const dialogRef = this.dialog.open(PreviewDialogComponent, {
        width: '50%',
        data: { infos: infos, prev_type: 2 },
        panelClass: 'print_modal'
      });
    }
    else{
      this.snackBar.open('No slides available or temporarily hidden', 'Error', {
        duration: 4000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        panelClass: ['cust-snackbar', 'not-submitted'],
      });
    }
  }

  getFooterSettings(){
    this.commonBridgeService.getSettings()
      .subscribe(response =>
                            {
                              if(response.status == 200){
                                if(response.settings[0]){
                                  if(response.settings[0].footer_logo){
                                    this.tempWeather.logo = response.settings[0].footer_logo;
                                  }
                                  if(response.settings[0].footerColor){
                                    this.tempWeather.footerColor = response.settings[0].footerColor;
                                  }
                                  if(response.settings[0].footerBgcolor){
                                    this.tempWeather.footerBgcolor = response.settings[0].footerBgcolor;
                                  }
                                  if(response.settings[0].announcementColor){
                                    this.tempAnnouncement.announcementColor = response.settings[0].announcementColor;
                                  }
                                  if(response.settings[0].announcementBgcolor){
                                    this.tempAnnouncement.announcementBgcolor = response.settings[0].announcementBgcolor;
                                  }
                                  if(response.settings[0].scrolldelay){
                                    this.tempAnnouncement.scrolldelay = response.settings[0].scrolldelay;
                                  }
                                  if(response.settings[0].city_name){
                                    this.tempWeather.custom_city_name = response.settings[0].city_name;
                                  }
                                  if(response.settings[0].app_id && response.settings[0].city_id){
                                    this.getWeatherInfo(response.settings[0].app_id, response.settings[0].city_id, response.settings[0].weatherUnit);
                                  }
                                  else{
                                    this.loadComponent();
                                  }
                                }
                              }
                            },
                error => this.errorMsg = error);
  }

  getWeatherInfo(app_id, city_id, unit){
    this.commonBridgeService.getWeatherInfo(app_id, city_id, unit)
      .subscribe(response =>
                            {
                              if(response.cod == 200){
                                this.tempWeather.temp = response.main.temp;
                                this.tempWeather.city_name = response.name;
                                this.loadComponent();
                              }
                            },
                error => {
                  this.loadComponent();
                  this.errorMsg = error;
                });
  }
  //trackBy
  trackByPlaylist(index: number, playlist: any): number {
  return playlist.playlist_id;
  }
}