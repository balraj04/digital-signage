import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { CommonBridgeService } from '../../services/common-bridge.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';

@Component({
  selector: 'app-add-playlist',
  templateUrl: './add-playlist.component.html',
  styleUrls: ['./add-playlist.component.css']
})
export class AddPlaylistComponent implements OnInit {

	private addPlaylistForm: FormGroup;
  errorMsg: any;
  title: string;

  // Snackbar Posittion
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  
  constructor(
    private router: Router,
    private commonBridgeService: CommonBridgeService,
    private snackBar: MatSnackBar) { 
    this.title = 'Add New Playlist';
  }

  ngOnInit() {
  }

  onSubmit = function (playlist_data) {
   this.commonBridgeService.savePlaylist(playlist_data)
     .subscribe(response =>
       {
         if(response.status == 200){
           if(response.playlist.id > 0){
             this.snackBar.open('Playlist saved successfully !', 'Success', {
                duration: 4000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
                panelClass: ['cust-snackbar', 'submitted'],
              });
             this.router.navigate(['/edit-playlist', response.playlist.id]);
           }
           else {
             this.snackBar.open('Playlist fail to save !', 'Error', {
                duration: 4000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
                panelClass: ['cust-snackbar', 'not-submitted'],
             });
           }
         }
         else {
           this.snackBar.open('Playlist fail to save !', 'Error', {
              duration: 4000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
              panelClass: ['cust-snackbar', 'not-submitted'],
           });
         }
       },
       error => this.errorMsg = error);
 }

}
