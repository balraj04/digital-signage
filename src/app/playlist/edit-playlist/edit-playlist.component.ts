import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CdkDrag, CdkDragStart, CdkDropList, CdkDropListContainer, CdkDropListGroup, moveItemInArray } from "@angular/cdk/drag-drop";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { CalendarComponent } from 'ng-fullcalendar';
import { Options } from 'fullcalendar';
import { CommonBridgeService, ScheduleSlideService, SharedService, PrintService } from './../../services/index';
import { PlaylistInfo, TempWeatherInfo, TempAnnouncementInfo } from './../../interfaces/index';
import { ScheduleDialogComponent } from './schedule-dialog/schedule-dialog.component';
import { HideSlideModalComponent } from './../../hide-slide-modal/hide-slide-modal.component';
import { DeleteSlideModalComponent } from './../../delete-slide-modal/delete-slide-modal.component';
import { PrintDialogComponent } from './../../print/print-dialog/print-dialog.component';
import { environment } from './../../../environments/environment';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';

const LIMIT = environment.limit;

@Component({
  selector: 'app-edit-playlist',
  templateUrl: './edit-playlist.component.html',
  styleUrls: ['./edit-playlist.component.css']
})
export class EditPlaylistComponent implements OnInit {

  @ViewChild(CdkDropListGroup) listGroup: CdkDropListGroup<CdkDropList>;
  @ViewChild(CdkDropList) placeholder: CdkDropList;

  public target: CdkDropList;
  public targetIndex: number;
  public source: CdkDropListContainer;
  public sourceIndex: number;
	
	title: string;
  playlist_id: number;
  errorMsg: any;
  private sub: any;
  playlistInfo: PlaylistInfo = {
    playlist: {
      playlist_id: 0,
      name: '',
      assign_calendar: '',
      scroll_announcement: '',
      footer_text: '',
      slug: '',
      slides: []
    }
  } as any;

  printInfo: PlaylistInfo;

  tempWeather: TempWeatherInfo = {
    city_name: '',
    temp: 0,
    logo: '',
    footerColor: '',
    footerBgcolor: '',
    custom_city_name: ''
  };

  tempAnnouncement: TempAnnouncementInfo = {
    text: '',
    scrolldelay: 0,
    announcementColor: '',
    announcementBgcolor: ''
  };

  events = [];
  tmp = [];
  mediaUrl = environment.mediaUrl;
  dateTemp : any = Math.random();

  calendarOptions: any;
  displayEvent: any;
  @ViewChild(CalendarComponent) ucCalendar: CalendarComponent;

  private editPlaylistForm: FormGroup;
  slide_thumb: string = 'assets/slides/slides-thumb.png';
  isScheduled: boolean = true;
  
  // Snackbar Posittion
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';  

	constructor(
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private commonBridgeService: CommonBridgeService,
    protected scheduleSlideService: ScheduleSlideService,
    protected sharedService: SharedService,
    private printService: PrintService,
    private snackBar: MatSnackBar,
    private _location: Location,
    private router: Router,)
  { 
		this.title = "Entry Hall";

    this.target = null;
    this.source = null;
	}

  ngAfterViewInit() {
    let phElement = this.placeholder.element.nativeElement;

    phElement.style.display = 'none';
    phElement.parentNode.removeChild(phElement);
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
       this.playlist_id = +params['id'];
       this.getPlaylistInfo(this.playlist_id);
    });

    /*var draggableEl = document.getElementById('draggable-el');
    console.log(draggableEl);*/
    /*new Draggable(containerEl, {
        itemSelector: '.fc-event',
        eventData: function(eventEl) {
          return {
            title: eventEl.innerText
          };
        }
      });*/
    // new Draggable(draggableEl);
    
    this.calendarOptions = {
        editable: true,
        droppable: true,
        resizable: true,
        timezone: 'local',
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay,listMonth'
        },
        events: [],
      };
  }
  //redirect to edit slide page with previous url
  editSlidePage(slideId:any=0) {

    this.sharedService.RouterUrl.emit(this.router.url);
    this.router.navigate(['/edit-slide', +slideId]);
    //this._location.back();
  }

  getEvent(event) {
    return JSON.stringify(event);
  }

  loadEvents(slides) {
    this.tmp = [];
    this.scheduleSlideService.getScheduledSlides(slides).subscribe(data => {
      this.events = data;
      this.tmp = [...slides];
    });
  }

  updateEvents(slides) {
    this.tmp = [];
    this.scheduleSlideService.updateScheduledSlides(slides).subscribe(data => {
      this.events = data;
      this.tmp = [...slides];
    });
  }

  eventrender(event)
  {
    if(event.event.start){
      event.event.start = new Date(event.event.start);
    }
    if(event.event.dowstart){
      event.event.dowstart = new Date(event.event.dowstart);
    }
    if(event.event.dowend){
      event.event.dowend = new Date(event.event.dowend);
    }
    var theDate = event.event.start;
    var startDate = event.event.dowstart;
    var endDate = event.event.dowend;
    if (theDate >= endDate) {
      event.element[0].setAttribute("class", 'hide');
      return false;
    }

    if (theDate <= startDate) {
      event.element[0].setAttribute("class", 'hide');
      return false;
    }
  }

  eventClick(model: any) {
    let slides = [...this.playlistInfo.playlist.slides];
    let currEvent:any;
    currEvent = slides.find(x => x.slide_id == model.event.slide_id);
    /*let eventStart = model.event.start;
    let eventEnd = (model.event.end) ? model.event.end : model.event.start;
    let start = eventStart._d;
    let end = eventEnd._d;*/
    let start = currEvent.start_datetime;
    start = new Date(start);
    let end = currEvent.end_datetime;
    end = new Date(end);
    let daysofweek = currEvent.daysofweek;
    let is_all_day = (currEvent.is_all_day == 'Y') ? true : false;
    let tmpl_type = (currEvent.template_id == 'tmpl-event') ? 'E' : '';
    this.openScheduleDialog({slide_id: model.event.slide_id, title: model.event.title, start: start, end: end, daysofweek: daysofweek, is_all_day: is_all_day, schedule_type: currEvent.schedule_type, scheduling: currEvent.scheduling, tmpl_type: tmpl_type});
  }

  updateEvent(model: any) {

    let slides = [...this.playlistInfo.playlist.slides];
    let currEvent:any;
    currEvent = slides.find(x => x.slide_id == model.event.slide_id);

    let eventStart = model.event.start;
    let eventEnd = (model.event.end) ? model.event.end : model.event.start;
    let start_scheduling = eventStart._d;
    let end_scheduling = eventEnd._d;
    start_scheduling = this.sharedService.dateToTimestamp(start_scheduling);
    end_scheduling = this.sharedService.dateToTimestamp(end_scheduling);

    let start = currEvent.start_datetime;
    start = new Date(start);
    let end = currEvent.end_datetime;
    end = new Date(end);
    let daysofweek = currEvent.daysofweek;
    let is_all_day = (currEvent.is_all_day == 'Y') ? true : false;

    if(currEvent.template_id == "tmpl-event"){
      currEvent.start_datetime = start_scheduling;
      currEvent.end_datetime = end_scheduling;
    }
    else{
      currEvent.scheduling[model.event.index_id].start_datetime = start_scheduling;
      currEvent.scheduling[model.event.index_id].end_datetime = end_scheduling;
    }

    let updateScheduledData = {
      'playlist_id': this.playlist_id.toString(), 
      'slide_id': model.event.slide_id,
      'offset': new Date().getTimezoneOffset().toString(),
      'scheduled': 'Y', 
      'schedule_type': 'M',
      'daysofweek': '',
      "is_all_day": 'N',
      "schedulingdates": {dates:currEvent.scheduling},
      'start_datetime': start_scheduling,
      'end_datetime': end_scheduling
    };
    this.updateScheduledSlide(updateScheduledData);
    // this.updateScheduledSlide({'playlist_id': this.playlist_id.toString(), 'slide_id': model.event.slide_id, 'start_datetime': start.toString(), 'end_datetime': end.toString(), 'offset': new Date().getTimezoneOffset().toString(), 'scheduled': 'Y'});
  }

  updateScheduledSlide(scheduleInfo){
    this.commonBridgeService.updateScheduledSlide(scheduleInfo)
      .subscribe(response =>
                  {
                    if(response.status === 200) {
                      // console.log(response);
                    }
                  },
      error => this.errorMsg = error);
  }

  openScheduleDialog(event): void {
    
    const dialogRef = this.dialog.open(ScheduleDialogComponent, {
      width: '50%',
      data: event
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result != 'no' && result){
        let tmpResult = {...result};
        let updateItem = this.tmp.find(this.findIndexToUpdate, event.slide_id);
        let index = this.tmp.indexOf(updateItem);

        let startTS = 0;
        let endTS = 0;

        this.tmp[index].schedule_type = tmpResult.schedule_type;
        if(tmpResult.schedule_type == 'M'){
          if(tmpResult.tmpl_type == 'E'){
            startTS = tmpResult.start_datetime;
            endTS = tmpResult.end_datetime;

            this.tmp[index].start_datetime = startTS;
            this.tmp[index].end_datetime = endTS;
          }
          this.tmp[index].scheduling = tmpResult.recursiverows.dates;
        }
        else{
          this.tmp[index].start_datetime = tmpResult.recursiverows.dates[0].start_datetime;
          this.tmp[index].end_datetime = tmpResult.recursiverows.dates[0].end_datetime;
          this.tmp[index].daysofweek = tmpResult.daysOfWeek;

          startTS = tmpResult.start_datetime;
          endTS = tmpResult.end_datetime;
        }

        let updateScheduledData = {
          'playlist_id': this.playlist_id.toString(), 
          'slide_id': event.slide_id, 
          'offset': new Date().getTimezoneOffset().toString(),
          'scheduled': 'Y', 
          'schedule_type':result.schedule_type,
          'daysofweek': (result.daysOfWeek ? result.daysOfWeek.toString() : ''),
          "is_all_day": result.is_all_day,
          "schedulingdates": result.recursiverows,
          'start_datetime': startTS,
          'end_datetime': endTS
        };

        this.updateScheduledSlide(updateScheduledData);
        this.updateEvents(this.tmp);
      }
    });
  }

  copySlide(slide_id){
    this.commonBridgeService.copySlide(slide_id)
       .subscribe(response =>{
         if(response.status == 200){
           
           this.snackBar.open('Slide copied successfully !', 'Success', {
              duration: 4000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
              panelClass: ['cust-snackbar', 'submitted'],
           });
           this.router.navigate(['/edit-slide', response.slide_id]);                   
         }
         else {
          this.snackBar.open('Slide fail to copy !', 'Error', {
            duration: 4000, 
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            panelClass: ['cust-snackbar', 'not-submitted'],
          });
        }
       },
       error => this.errorMsg = error);
  }
  
  openDelteDialog(event): void {
    const dialogRef = this.dialog.open(DeleteSlideModalComponent, {
      width: '50%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result == 'Y'){
        this.deleteSlide(event);
      }
    });
  }

  deleteSlide(slide_id){
    this.commonBridgeService.deleteSlide(slide_id)
     .subscribe(response =>
       {
         if(response.status == 200){
           if(response.delete){
             this.snackBar.open('Slide deleted successfully !', 'Success', {
                duration: 4000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
                panelClass: ['cust-snackbar', 'submitted'],
              });
              let updateItem = this.playlistInfo.playlist.slides.find(this.findIndexToUpdate, slide_id);
              let index = this.playlistInfo.playlist.slides.indexOf(updateItem);
              if (index !== -1) {
                this.playlistInfo.playlist.slides.splice(index, 1);
              }
           }
           else {
             this.snackBar.open('Fail to delete slide !', 'Error', {
                duration: 4000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
                panelClass: ['cust-snackbar', 'not-submitted'],
             });
           }
         }
         else {
           this.snackBar.open('Fail to delete slide !', 'Error', {
              duration: 4000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
              panelClass: ['cust-snackbar', 'not-submitted'],
           });
         }
       },
       error => this.errorMsg = error);
  }

  openHideShowDialog(slide_id, status): void {
    const dialogRef = this.dialog.open(HideSlideModalComponent, {
      width: '50%',
      data: { status: status }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result == 'Y'){
        this.hideShowSlide(slide_id, status);
      }
    });
  }

  hideShowSlide(slide_id, status){
    let newStatus = (status == 'I') ? 'A' : 'I';
    this.commonBridgeService.hideShowSlide(slide_id, newStatus)
     .subscribe(response =>
       {
         if(response.status == 200){
           if(response.update){
             this.updateSlideStatus(slide_id, newStatus);
             this.snackBar.open('Slide status updated successfully !', 'Success', {
                duration: 4000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
                panelClass: ['cust-snackbar', 'submitted'],
              });
           }
           else {
             this.snackBar.open('Fail to update slide status !', 'Error', {
                duration: 4000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
                panelClass: ['cust-snackbar', 'not-submitted'],
             });
           }
         }
         else {
           this.snackBar.open('Fail to update slide status !', 'Error', {
              duration: 4000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
              panelClass: ['cust-snackbar', 'not-submitted'],
           });
         }
       },
       error => this.errorMsg = error);
  }

  updateSlideStatus(slide_id, status){
    let updateItem = this.playlistInfo.playlist.slides.find(this.findIndexToUpdate, slide_id);
    let index = this.playlistInfo.playlist.slides.indexOf(updateItem);
    if (index !== -1) {
      this.playlistInfo.playlist.slides[index].status = status;
    }
  }

  showUpdatedItem(newItem){
    /*console.log('showUpdatedItem newItem ==>', newItem);
    this.tmp.push(newItem);
    for (var i = this.tmp.length - 1; i >= 0; i--) {
      var tmpstart = this.dateToTimestamp(this.tmp[i].start);
      var tmpend = this.dateToTimestamp(this.tmp[i].end);
       // new Date().toDateString();
       // new Date().toDateString();

      var newstart = this.dateToTimestamp(newItem.start);
      var newend = this.dateToTimestamp(newItem.end);

      // console.log('temp Sa==>', tmpstart, tmpend);
      // console.log('new ==>', newstart, newend);
      if (this.tmp[i].start != newItem.start && this.tmp[i].end != newItem.end) {
        this.tmp.push(newItem);
      }
      else {
        
      }
      if (this.tmp[i].start != newItem.start && this.tmp[i].end != newItem.end) {
        this.tmp.push(newItem); 
      }
    }*/

    let updateItem = this.tmp.find(this.findIndexToUpdate, newItem.slide_id);
    let index = this.tmp.indexOf(updateItem);
    this.tmp[index] = newItem;
  }

  findIndexToUpdate(newItem) { 
        return newItem.slide_id === this;
  }

  getPlaylistInfo(playlist_id){
    this.commonBridgeService.getPlaylistInfo(playlist_id)
      .subscribe(response =>
                            {
                              if(response.status === 200){
                                if(response.playlist){
                                  this.playlistInfo = response;
                                  this.loadEvents(this.playlistInfo.playlist.slides);
                                  this.isScheduled = (this.playlistInfo.playlist.assign_calendar == 'Y') ? true :false;
                                }
                                else{
                                  this.router.navigate(['/playlist']);
                                  this.snackBar.open('Playlist does not exist', 'Error', {
                                    duration: 4000,
                                    horizontalPosition: this.horizontalPosition,
                                    verticalPosition: this.verticalPosition,
                                    panelClass: ['cust-snackbar', 'not-submitted'],
                                  });
                                }
                              }
                            },
                error => this.errorMsg = error);
  }

  printPlaylistsInfo(){
    let datetime = new Date().getTime();
    this.commonBridgeService.getPlaylistInfo(this.playlist_id, datetime)
      .subscribe(response =>
                            {
                              if(response.status == 200){
                                this.printInfo = response;
                                if(this.printInfo.playlist.slides.length){
                                  this.configComponent();
                                }
                                else{
                                  this.snackBar.open('No slides available', 'Error', {
                                    duration: 4000,
                                    horizontalPosition: this.horizontalPosition,
                                    verticalPosition: this.verticalPosition,
                                    panelClass: ['cust-snackbar', 'not-submitted'],
                                  });
                                }
                              }
                            },
                error => this.errorMsg = error);
  }

  configComponent(){
    if(this.playlistInfo.playlist.footer_text == 'Y' || this.playlistInfo.playlist.scroll_announcement == 'Y'){
      this.getFooterSettings();
    }
    else{
      this.loadComponent();
    }
  }

  loadComponent(){
    let infos = this.printService.getComponents(this.printInfo.playlist.slides);
    if(infos.length){
      for (var i = infos.length - 1; i >= 0; i--) {
        this.tempAnnouncement.text = (this.playlistInfo.playlist.scroll_announcement == 'Y') ? this.playlistInfo.playlist.announcements : '';
        infos[i].content_data['community_footer'] = {'status': this.playlistInfo.playlist.footer_text, 'content': this.tempWeather};
        infos[i].content_data['scroll_announcement'] = {'status': this.playlistInfo.playlist.scroll_announcement, 'content': this.tempAnnouncement};
      }
      const dialogRef = this.dialog.open(PrintDialogComponent, {
        width: '50%',
        data: { infos: infos, prev_type: 2 },
        panelClass: 'print_modal'
      });
    }
    else{
      this.snackBar.open('No slides available or temporarily hidden', 'Error', {
        duration: 4000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        panelClass: ['cust-snackbar', 'not-submitted'],

      });
    }
  }

  getFooterSettings(){
    this.commonBridgeService.getSettings()
      .subscribe(response =>
                            {
                              if(response.status == 200){
                                if(response.settings[0]){
                                  if(response.settings[0].footer_logo){
                                    this.tempWeather.logo = response.settings[0].footer_logo;
                                  }
                                  if(response.settings[0].footerColor){
                                    this.tempWeather.footerColor = response.settings[0].footerColor;
                                  }
                                  if(response.settings[0].footerBgcolor){
                                    this.tempWeather.footerBgcolor = response.settings[0].footerBgcolor;
                                  }
                                  if(response.settings[0].announcementColor){
                                    this.tempAnnouncement.announcementColor = response.settings[0].announcementColor;
                                  }
                                  if(response.settings[0].announcementBgcolor){
                                    this.tempAnnouncement.announcementBgcolor = response.settings[0].announcementBgcolor;
                                  }
                                  if(response.settings[0].scrolldelay){
                                    this.tempAnnouncement.scrolldelay = response.settings[0].scrolldelay;
                                  }
                                  if(response.settings[0].app_id && response.settings[0].city_id){
                                    this.getWeatherInfo(response.settings[0].app_id, response.settings[0].city_id, response.settings[0].weatherUnit);
                                  }
                                  else{
                                    this.loadComponent();
                                  }
                                }
                              }
                            },
                error => this.errorMsg = error);
  }

  getWeatherInfo(app_id, city_id, unit){
    this.commonBridgeService.getWeatherInfo(app_id, city_id, unit)
      .subscribe(response =>
                            {
                              if(response.cod == 200){
                                this.tempWeather.temp = response.main.temp;
                                this.tempWeather.city_name = response.name;
                                this.loadComponent();
                              }
                            },
                error => {
                  this.loadComponent();
                  this.errorMsg = error;
                });
  }

  isScheduledCheck(flag){
    this.isScheduled = (flag == 'Y') ? true : false;
    if(flag == 'Y'){
      this.loadEvents(this.playlistInfo.playlist.slides);
    }
  }

  updateSlideOrder(slideData){
    this.commonBridgeService.updateSlideOrder(slideData)
      .subscribe(response =>
                            {
                              if(response.status === 200){
                                // console.log(response);
                              }
                            },
                error => this.errorMsg = error);
  }

  backClicked() {
    this._location.back();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onSubmit = function (playlist_data) {
   this.commonBridgeService.updatePlaylist(playlist_data)
     .subscribe(response =>
       {
         if(response.status == 200){
           if(response.playlist.playlist_id > 0){
             this.snackBar.open('Playlist updated successfully !', 'Success', {
                duration: 4000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
                panelClass: ['cust-snackbar', 'submitted'],
              });
           }
           else {
             this.snackBar.open('Playlist fail to update !', 'Error', {
                duration: 4000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
                panelClass: ['cust-snackbar', 'not-submitted'],
             });
           }
         }
         else {
           this.snackBar.open('Playlist fail to update !', 'Error', {
              duration: 4000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
              panelClass: ['cust-snackbar', 'not-submitted'],
           });
         }
       },
       error => this.errorMsg = error);
 }

  updateArrayOrder(arr){
    let tmpArr = [];
    for (let i = 0; i < arr.length; i++) {
      let order = i;
      tmpArr.push({
        'slide_id':  arr[i].slide_id,
        'order_by': order + 1
      });
    }

    this.updateSlideOrder(tmpArr);
  }

  drop() {
    if (!this.target)
      return;

    let phElement = this.placeholder.element.nativeElement;
    let parent = phElement.parentNode;

    phElement.style.display = 'none';

    parent.removeChild(phElement);
    parent.appendChild(phElement);
    parent.insertBefore(this.source.element.nativeElement, parent.children[this.sourceIndex]);

    this.target = null;
    this.source = null;

    if (this.sourceIndex != this.targetIndex){
      moveItemInArray(this.playlistInfo.playlist.slides, this.sourceIndex, this.targetIndex);
      this.updateArrayOrder(this.playlistInfo.playlist.slides);
    }
  }

  enter = (drag: CdkDrag, drop: CdkDropList) => {
    if (drop == this.placeholder)
      return true;

    let phElement = this.placeholder.element.nativeElement;
    let dropElement = drop.element.nativeElement;

    let dragIndex = __indexOf(dropElement.parentNode.children, drag.dropContainer.element.nativeElement);
    let dropIndex = __indexOf(dropElement.parentNode.children, dropElement);

    if (!this.source) {
      this.sourceIndex = dragIndex;
      this.source = drag.dropContainer;

      let sourceElement = this.source.element.nativeElement;
      phElement.style.width = sourceElement.clientWidth + 'px';
      phElement.style.height = sourceElement.clientHeight + 'px';
      
      sourceElement.parentNode.removeChild(sourceElement);
    }

    this.targetIndex = dropIndex;
    this.target = drop;

    phElement.style.display = '';
    dropElement.parentNode.insertBefore(phElement, (dragIndex < dropIndex)
      ? dropElement.nextSibling : dropElement);

    this.source.start();
    this.placeholder.enter(drag, drag.element.nativeElement.offsetLeft, drag.element.nativeElement.offsetTop);

    return false;
  }

  //trackBy
  trackBySlide(index: number, slide: any): number {
  return slide.slide_id;
  }

}

function __indexOf(collection, node) {
  return Array.prototype.indexOf.call(collection, node);
};