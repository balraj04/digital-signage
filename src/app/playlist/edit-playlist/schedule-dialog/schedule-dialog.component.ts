import { Component, ViewChild, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray, NgForm } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatOption } from '@angular/material';

@Component({
  selector: 'app-schedule-dialog',
  templateUrl: './schedule-dialog.component.html',
  styleUrls: ['./schedule-dialog.component.css']
})
export class ScheduleDialogComponent implements OnInit {

	public scheduleForm: FormGroup;
  recursiverows: FormArray;
	start: any;
	end: any;

  all_days = [];
  selected_days = [];
  is_all_day: boolean = false;

  @ViewChild('allSelected') private allSelected: MatOption;

  constructor(
    public dialogRef: MatDialogRef<ScheduleDialogComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: ScheduleDialogData) {
    this.all_days = [
      {
        id: 0,
        value: 'SU'
      },
      {
        id: 1,
        value: 'MO'
      },
      {
        id: 2,
        value: 'TU'
      },
      {
        id: 3,
        value: 'WE'
      },
      {
        id: 4,
        value: 'TH'
      },
      {
        id: 5,
        value: 'FR'
      },
      {
        id: 6,
        value: 'SA'
      }
    ];
    this.recursiverows = this.fb.array([]);
    this.selected_days = data.selected_days;
  }

  ngOnInit() {
    // Declare Form Group
    this.scheduleForm = this.fb.group({
      schedule_type: this.fb.control('M'),
      daysOfWeek: this.fb.control([]),
    });

    this.scheduleForm.addControl('recursiverows', this.recursiverows);
    
    if (this.data) {
      let tmpData:any = {...this.data}
      this.scheduleForm.controls['schedule_type'].setValue(tmpData.schedule_type);
      this.scheduleForm.controls['daysOfWeek'].setValue(tmpData.daysofweek);
      if(tmpData.schedule_type == 'M'){
        if(tmpData.tmpl_type == 'E'){
          var start_date = new Date(tmpData.start);
          var end_date = new Date(tmpData.end);
          this.createExistingItemFormGroup(start_date, end_date)
        }
        else{
          for (var i = tmpData.scheduling.length - 1; i >= 0; i--) {
            var start_date = new Date(tmpData.scheduling[i].start_datetime);
            var end_date = new Date(tmpData.scheduling[i].end_datetime);
            this.createExistingItemFormGroup(start_date, end_date)
          }
        }
      }
      else{
        var start_date = new Date(tmpData.start);
        var end_date = new Date(tmpData.end);
        this.createExistingItemFormGroup(start_date, end_date)
      }
    }
    else {
      this.onAddRow()
    }
  }

  /*onAddExistingRow() {
    this.recursiverows.push(this.createExistingItemFormGroup());
  }*/

  createExistingItemFormGroup(start_date, end_date) {
    this.recursiverows.push(this.fb.group({
      start_datetime: this.fb.control(start_date, Validators.required), 
      end_datetime: this.fb.control(end_date, Validators.required),
      min_datetime: this.fb.control(start_date), 
      max_datetime: this.fb.control(end_date)
    }));
    ;
  }

  dateToTimestamp(dt){    
    let millitime = new Date(dt).setSeconds(0);
    return millitime;
  }

  submitShedule() {
      let SheduleData = this.scheduleForm.value;
      //console.log(' SheduleData =>', SheduleData);
      let scheudling: any;
      let dates = [];
      for (var i = 0; i <= SheduleData.recursiverows.length - 1; i++) {
        // console.log('SheduleData.recursiverows[i].start_datetime ==>>', SheduleData.recursiverows[i].start_datetime);
        let startTS = this.dateToTimestamp(SheduleData.recursiverows[i].start_datetime);
        let endTS = this.dateToTimestamp(SheduleData.recursiverows[i].end_datetime);

        if (isNaN(startTS)) {
          startTS = 0;
        }
        if (isNaN(endTS)) {
          endTS = 0;
        }

        let tempDates = { 
          start_datetime: startTS,
          end_datetime: endTS
        };

        dates.push(tempDates);      
      }

      if(this.data.tmpl_type == 'E'){
        let startTS = this.dateToTimestamp(SheduleData.recursiverows[0].start_datetime);
        let endTS = this.dateToTimestamp(SheduleData.recursiverows[0].end_datetime);

        if (isNaN(startTS)) {
          startTS = 0;
        }
        if (isNaN(endTS)) {
          endTS = 0;
        }

        SheduleData['start_datetime'] = startTS;
        SheduleData['end_datetime'] = endTS;
      }

      SheduleData['tmpl_type'] = (this.data.tmpl_type == 'E') ? 'E' : '';

      scheudling = {dates:dates};
      SheduleData['recursiverows'] = scheudling;

      let is_all_day = 'N';

      if(SheduleData.daysOfWeek && SheduleData.daysOfWeek.length >= this.all_days.length){
        SheduleData['is_all_day'] = 'Y';
      }
      else{
        SheduleData['is_all_day'] = 'N';
      }
      
      this.dialogRef.close(SheduleData);
      // this.dialogRef.close('no');  
  }

  onAddRow() {
    this.recursiverows.push(this.createItemFormGroup());
  }

  createItemFormGroup(): FormGroup {
    return this.fb.group({
      start_datetime: this.fb.control(null, Validators.required), 
      end_datetime: this.fb.control(null, Validators.required),
      min_datetime: this.fb.control(null), 
      max_datetime: this.fb.control(null)
    });
  }

  onRemoveRow(rowIndex:number){
    this.recursiverows.removeAt(rowIndex);
  }

  makeScheudlingBlank() {    
    let PostData      = this.scheduleForm.value;    
    for (var i = PostData.recursiverows.length - 1; i >= 0; i--) {     
      this.recursiverows.removeAt(i);
    }    
  }

  makeScheudlingAdd(){
    // this.scheduleForm.get('daysOfWeek').setValidators([Validators.required]);
    this.scheduleForm.controls['daysOfWeek'].setValue([]);
    this.scheduleForm.get('daysOfWeek').clearValidators();
    this.scheduleForm.get('daysOfWeek').updateValueAndValidity();

     this.makeScheudlingBlank();
     let PostData  = this.scheduleForm.value;
     var i = PostData.recursiverows ? PostData.recursiverows.length : 0;
     if (i === 0) {
       this.onAddRow();
     }
  }

  makeScheudlingAuto(){
    this.scheduleForm.get('daysOfWeek').setValidators([Validators.required]);
    // this.scheduleForm.get('daysOfWeek').clearValidators();
    this.scheduleForm.get('daysOfWeek').updateValueAndValidity();

     this.makeScheudlingBlank();
     let PostData  = this.scheduleForm.value;
     var i = PostData.recursiverows ? PostData.recursiverows.length : 0;
     if (i === 0) {
       this.onAddRow();
     }
  }  

  onNoClick(): void {
    this.dialogRef.close('no');
  }  

  updateItemFormGroup(min_dt, max_dt): FormGroup {    
    return this.fb.group({      
      start_datetime: this.fb.control(min_dt, Validators.required), 
      end_datetime: this.fb.control(max_dt, Validators.required),
      min_datetime: this.fb.control(min_dt), 
      max_datetime: this.fb.control(max_dt)
    });
  }

  changeMinDTime(row, index){
    let min_dt = row.controls['start_datetime'].value ? row.controls['start_datetime'].value : null;
    let max_dt = row.controls['end_datetime'].value ? row.controls['end_datetime'].value : null;
    this.onRemoveRow(index);
    this.recursiverows.insert(index, this.updateItemFormGroup(min_dt, max_dt));
   
    // console.log('min_dt', min_dt);
    // console.log('this.recursiverows', this.recursiverows);
    /*if(this.SlideForm.controls['start_datetime'].value){
      this.min_dt = this.SlideForm.controls['start_datetime'].value;
    }*/
  }

  changeMaxDTime(row, index){
    let min_dt = row.controls['start_datetime'].value ? row.controls['start_datetime'].value : null;
    let max_dt = row.controls['end_datetime'].value ? row.controls['end_datetime'].value : null;
    this.onRemoveRow(index);
    this.recursiverows.insert(index, this.updateItemFormGroup(min_dt, max_dt));
    // console.log('max_dt', max_dt);
    // console.log('this.recursiverows', this.recursiverows);
    /*if(this.SlideForm.controls['end_datetime'].value){
      this.max_dt = this.SlideForm.controls['end_datetime'].value;      
    }*/
  }

  isChecked() {
    if(this.selected_days.length === this.all_days.length){
      this.is_all_day = true;
    }
    else{
      this.is_all_day = false;
    }
    return this.selected_days.length === this.all_days.length;
  }

  isIndeterminate() {
    return (this.selected_days.length !== 0 && this.selected_days.length !== this.all_days.length);
  }

  selectBadge(e, sel_id) {
   if (e.checked) {
     this.selected_days.push(sel_id);
   }  else {
     this.selected_days.splice(this.selected_days.indexOf(sel_id), 1);
   }
  }

  exists(curr_slide){
    return this.selected_days.indexOf(curr_slide) > -1;
  }

  UncheckAllDays(all){ 
      if (this.allSelected.selected){  
          this.allSelected.deselect();
          return false;
      }
      if(this.scheduleForm.controls.daysOfWeek.value.length==this.all_days.length){
          this.allSelected.select();        
      }  
  }
  
  checkAllDays() {
    if (this.allSelected.selected) {  
      this.scheduleForm.controls.daysOfWeek.setValue(this.all_days.map(item => item.id));
      this.allSelected.select();
    } else {
      this.scheduleForm.controls.daysOfWeek.setValue([]);
    }
  }

  selectAllBadge(e) {
   this.selected_days.splice(0,7);
   if (e.checked) {
     this.all_days.forEach(y => this.selected_days.push(y.id));
   }
  }

  trackByDay(index: number, all_days: any): number {
    return all_days.id;
  }

}

export interface ScheduleDialogData {
  title: string;
  slide_id: number;
  start: any;
  end: any;
  selected_days: any;
  tmpl_type: string;
}