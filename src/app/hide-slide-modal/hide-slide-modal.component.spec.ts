import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HideSlideModalComponent } from './hide-slide-modal.component';

describe('HideSlideModalComponent', () => {
  let component: HideSlideModalComponent;
  let fixture: ComponentFixture<HideSlideModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HideSlideModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HideSlideModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
