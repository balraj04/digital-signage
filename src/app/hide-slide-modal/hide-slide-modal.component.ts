import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-hide-slide-modal',
  templateUrl: './hide-slide-modal.component.html',
  styleUrls: ['./hide-slide-modal.component.css']
})
export class HideSlideModalComponent implements OnInit {

  constructor(
  	public dialogRef: MatDialogRef<HideSlideModalComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogdata: any) {
  }

  ngOnInit() {
  }

  onBtnClick(flag): void {
    this.dialogRef.close(flag);
  }

}
