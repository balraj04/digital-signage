import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonBridgeService } from './../services/index';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

	showLoader: boolean = false;
	private subscription: Subscription;

  constructor(
    private loaderService: CommonBridgeService) {}

  ngOnInit() {
  	this.subscription = this.loaderService.loaderState
    .subscribe((state: LoaderState) => {
      let ref = this;
      setTimeout(function() {
        ref.showLoader = state.show;
      });
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}

export interface LoaderState {
  show: boolean;
}