import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, PageEvent } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-announcement-widget',
  templateUrl: './announcement-widget.component.html',
  styleUrls: ['./announcement-widget.component.css']
})
export class AnnouncementWidgetComponent implements OnInit {

  public AcementSettingForm: FormGroup;
  public HomeAnnouncements: any = [];
  constructor(
  	private fb: FormBuilder,
  	public dialogRef: MatDialogRef<AnnouncementWidgetComponent>,
  	@Inject(MAT_DIALOG_DATA) public dailogData: DialogData) { }

  ngOnInit() {
    this.HomeAnnouncements = this.dailogData.announcements
  	this.AcementSettingForm = this.fb.group({
	  widgetLimit: this.fb.control(this.dailogData.limit, [Validators.required]),
	  widgetType: this.fb.control(this.dailogData.widget_type, [Validators.required]),
    HomeAnnounceId: this.fb.control(this.dailogData.HomeAnnounceId)
	});
  }

  submitAcementSetting() {
  	this.dialogRef.close({widgetLimit: this.AcementSettingForm.get('widgetLimit').value, widgetType: this.AcementSettingForm.get('widgetType').value,HomeAnnounceId: this.AcementSettingForm.get('HomeAnnounceId').value});
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

export interface DialogData {
  widget_type: string;
  limit: string;
  HomeAnnounceId: number;
  announcements: number;
}
