import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, PageEvent } from '@angular/material';
@Component({
  selector: 'app-drag-modal',
  templateUrl: './drag-modal.component.html',
  styleUrls: ['./drag-modal.component.css']
})
export class DragModalComponent implements OnInit {

  public frameArea:any =[];

  constructor(
  	public dialogRef: MatDialogRef<DragModalComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogdata: any
  	) { }

  ngOnInit() {
  }
  //fnction to set Y/N flag for darg template
  DragTemplate(dragtype:any="N") {  	
  	this.dialogRef.close({dragType:dragtype});
    this.showIframeArea();
  }

  showIframeArea(){
    this.frameArea = document.getElementsByClassName('iframearea');
    if(this.frameArea.length>0)
    {
      this.frameArea[0].style = 'display:block;';
    }
  }

}
