import { Component, OnInit, Output, Injectable, EventEmitter, ViewChild, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SharedService } from './../services/shared.service';
import { CommonBridgeService } from './../services/common-bridge.service';
import { environment } from '../../environments/environment';
import { MediaLibraryDialogComponent } from '../media-library-dialog/media-library-dialog.component';

const mediaUrl = environment.mediaUrl;

@Component({
  selector: 'app-modal-dialog',
  templateUrl: './modal-dialog.component.html',
  styleUrls: ['./modal-dialog.component.css']
})
export class ModalDialogComponent implements OnInit {

  blurDefault = environment.blurDefault;
  noMedia = environment.noimageDefault;
  public ShareForm: FormGroup;
  imgData : Boolean = false;
  imgUrl : string;
  baseUrl: string;
  private errorMsg: any;

  constructor(
    private commonBridgeService: CommonBridgeService,
    private sharedService: SharedService, 
    public dialogRef: MatDialogRef<ModalDialogComponent>, 
    private fb: FormBuilder,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public dialogdata: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.baseUrl = mediaUrl;
    this.ShareForm = this.fb.group({
      txtcolor: this.fb.control(this.dialogdata.txtcolor),
      bgcolor: this.fb.control(this.dialogdata.bgcolor),
      bgimage: this.fb.control(this.dialogdata.bgimage),
      bgrepeat: this.fb.control(this.dialogdata.bgrepeat),
      bgsize: this.fb.control(this.dialogdata.bgsize),
      bgposition: this.fb.control(this.dialogdata.bgposition),
      bgtype: this.fb.control(this.dialogdata.bgtype),
      column: this.fb.control(this.dialogdata.column),
      isText: this.fb.control(this.dialogdata.isText),
      styled: this.fb.control(true),
    });
    this.CheckRequiredFields();
  }

  SaveSettings(f) {
    var formInfo = new FormData();

    formInfo.append('SettingsJson', JSON.stringify(this.ShareForm.value));
    this.sharedService.SharedData.emit(formInfo);
    this.dialogRef.close();
  }

  CheckRequiredFields() {
      this.ShareForm.get('bgtype').valueChanges.subscribe((backtype: string) => {
          if (backtype === 'bgcolor') {
              this.ShareForm.get('txtcolor').setValidators([Validators.required]);
              this.ShareForm.get('bgcolor').setValidators([Validators.required]);
              this.ShareForm.get('bgrepeat').clearValidators();
              this.ShareForm.get('bgsize').clearValidators();
              this.ShareForm.get('bgposition').clearValidators();
              this.ShareForm.get('bgimage').clearValidators();
          }
          else if (backtype === 'bgimage') {
              this.ShareForm.get('txtcolor').setValidators([Validators.required]);
              this.ShareForm.get('bgimage').setValidators([Validators.required]);
              this.ShareForm.get('bgrepeat').setValidators([Validators.required]);
              this.ShareForm.get('bgsize').setValidators([Validators.required]);
              this.ShareForm.get('bgposition').setValidators([Validators.required]);
              this.ShareForm.get('bgcolor').clearValidators();
          }
          this.ShareForm.get('txtcolor').updateValueAndValidity();
          this.ShareForm.get('bgcolor').updateValueAndValidity();
          this.ShareForm.get('bgrepeat').updateValueAndValidity();
          this.ShareForm.get('bgsize').updateValueAndValidity();
          this.ShareForm.get('bgposition').updateValueAndValidity();
          this.ShareForm.get('bgimage').updateValueAndValidity();      
      });
  }

  selectImageLibrary(event): void {
    const dialogRef = this.dialog.open(MediaLibraryDialogComponent, {
      width: '50%',
      data: {media_url: ''}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.ShareForm.get('bgimage').setValue(result);
        this.ShareForm.get('bgimage').clearValidators();
        this.ShareForm.get('bgimage').updateValueAndValidity();
      }
    });
  }
}