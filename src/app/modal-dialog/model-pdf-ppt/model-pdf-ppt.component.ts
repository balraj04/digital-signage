import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, PageEvent } from '@angular/material';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonBridgeService } from './../../services/common-bridge.service';
import { SharedService } from './../../services/shared.service';
import { environment } from '../../../environments/environment';

const LIMIT = environment.limit;

@Component({
  selector: 'app-model-pdf-ppt',
  templateUrl: './model-pdf-ppt.component.html',
  styleUrls: ['./model-pdf-ppt.component.css']
})
export class ModelPdfPptComponent implements OnInit {

  fileName : string;
  imgoffset = environment.imgoffset;
  blurDefault = environment.blurDefault;
  public mediaForm: FormGroup;
  errorMsg: any;
  errorSaveMsg: any;
  medias: [];
  dateTemp : any = Math.random();
  mediaUrl = environment.mediaUrl;
  media_thumb: string;
  imgUrl : String;

  // MatPaginator Inputs
  length: number = 0;
  pageSize = LIMIT;
  page: number = 1;

  // MatPaginator Output
  pageEvent: PageEvent;
  inputAccpets : string = ".pdf, .ppt, .pptx, .ppsx, .pps, .ppsm";

  reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w|embed\/|watch\?v=|\&v= .-]*/?'; // /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/; 

  @ViewChild('fileInput') el:ElementRef;

  constructor(
  	private commonBridgeService: CommonBridgeService,
    private sharedService: SharedService,
  	private _sanitizer: DomSanitizer,
  	private fb: FormBuilder,
  	public dialogRef: MatDialogRef<ModelPdfPptComponent>,
  	@Inject(MAT_DIALOG_DATA) public dailogData: DialogData) {
    
  	this.media_thumb = environment.noimageDefault;
    if (this.dailogData.modal_type === 'pdf_options') {
      this.inputAccpets = ".pdf, .ppt, .pptx, .ppsx, .pps, .ppsm";
    }
    if (this.dailogData.modal_type === 'video_options') {
      this.inputAccpets = "video/*";
    }
  }

  ngOnInit() {  
    let defaultRedioSelect = ''; 
    let localVideourl = '';
    let youtubeVideourl = '';  
    let youtubeUrl = this.dailogData.uploadedfile.includes('youtube');
    if (youtubeUrl || this.dailogData.modal_type === 'youtube_options') {
      defaultRedioSelect = 'upload_url';
      youtubeVideourl = this.dailogData.uploadedfile;
    }
    else {
      defaultRedioSelect = this.dailogData.uploadedfile !== '' ? 'ext_img' : 'new_upload';      
      localVideourl = this.dailogData.uploadedfile !== '' ? this.dailogData.uploadedfile : '';
    }
    

    this.mediaForm = this.fb.group({
      bgmediatype: this.fb.control(defaultRedioSelect),
      media: this.fb.control(localVideourl),
      external_url: this.fb.control(youtubeVideourl, [Validators.required, Validators.pattern(this.reg)]),
      newmedia: this.fb.control('', [Validators.required])
    });
    this.CheckRequiredFields();
    if (defaultRedioSelect === 'ext_img' || defaultRedioSelect === 'upload_url') {
      defaultRedioSelect === 'ext_img' ? this.getMedia(this.page, LIMIT) : null;
      this.mediaForm.get('newmedia').clearValidators();
      this.mediaForm.get('newmedia').updateValueAndValidity();
      this.mediaForm.get('external_url').clearValidators();
      this.mediaForm.get('external_url').updateValueAndValidity();    
      this.mediaForm.get('media').clearValidators();
      this.mediaForm.get('media').updateValueAndValidity();
    }
  }

  getSlicedFileName(fileName){
    if (fileName.includes('public/uploads/pdf/') ) {
      return fileName.replace('public/uploads/pdf/', '');
    }
    else if ( fileName.includes('public/uploads/ppt/') ) {
      return fileName.replace('public/uploads/ppt/', '');
    } 
    else {      
      return fileName.replace('public/uploads/video/', '');
    }
  }

  getIconFileName(fileName){
    if ( fileName.includes('public/uploads/pdf/') ) {
      return 'picture_as_pdf';
    }
    else if ( fileName.includes('public/uploads/ppt/') ) {
      return 'folder_shared';
    } 
    else {
      return 'video_library';
    }
  }


  CheckRequiredFields() {    
    this.mediaForm.get('bgmediatype').valueChanges.subscribe((backtype: string) => {
      if (backtype === 'new_upload') {        
        this.mediaForm.get('newmedia').setValidators([Validators.required]);
        this.mediaForm.get('external_url').clearValidators();
        this.mediaForm.get('media').clearValidators();
      }
      else if (backtype === 'upload_url') {    
        // const reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';   
        this.mediaForm.get('external_url').setValidators([Validators.required,  Validators.pattern(this.reg)]); 
        this.mediaForm.get('newmedia').clearValidators();
        this.mediaForm.get('media').clearValidators();       
      }
      else if (backtype === 'ext_img') {
        this.getMedia(this.page, LIMIT);
        this.mediaForm.get('media').setValidators([Validators.required]);
        this.mediaForm.get('newmedia').clearValidators();
        this.mediaForm.get('external_url').clearValidators();
      }
      this.mediaForm.get('newmedia').updateValueAndValidity();
      this.mediaForm.get('external_url').updateValueAndValidity();
      this.mediaForm.get('media').updateValueAndValidity();
    });
  }

  //set Preview Image when user select input file
  setPreviewImage(fileInput:any) {    
    if (fileInput.target.files && fileInput.target.files[0]) {    	
      this.fileName = fileInput.target.files[0].name;
      this.mediaForm.get('newmedia').clearValidators();
      this.mediaForm.get('newmedia').updateValueAndValidity();
      this.mediaForm.get('external_url').clearValidators();
      this.mediaForm.get('external_url').updateValueAndValidity();    
      this.mediaForm.get('media').clearValidators();
      this.mediaForm.get('media').updateValueAndValidity();
    }

  }

  getNext(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.getMedia(this.page, LIMIT);
  }

  getMedia(page, limit){
    this.medias = [];

    let type; 
    if (this.dailogData.modal_type === 'pdf_options') {
      type = 'ppt,pdf';
    }
    if (this.dailogData.modal_type === 'video_options') {      
      type = 'video';
    }

    this.commonBridgeService.getPDFPPTMedia(page.toString(), limit.toString(), type)
      .subscribe(response =>
        {
          if(response.message == 'success'){
            this.medias = response.media.data;
            this.length = response.media.total;
          }
        },
        error => this.errorMsg = error);
  }

  getYoutubeId(url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return 'error';
    }
  }

  submitMedia() {
    if(this.mediaForm.get('bgmediatype').value=='new_upload' && (this.el.nativeElement.files.length>0)) {
      let slideFormInfo = new FormData();
        if (this.dailogData.modal_type === 'pdf_options') {
          if (this.el.nativeElement.files[0].name.includes(".pdf")) {
            slideFormInfo.append('type','pdf');
          }
          else{
            slideFormInfo.append('type','ppt');
          } 
        }
        if (this.dailogData.modal_type === 'video_options') {
          slideFormInfo.append('type','video');
        }
               
        slideFormInfo.append('image',this.el.nativeElement.files[0]);
        
        this.commonBridgeService.savePDFPPT(slideFormInfo)
	      .subscribe(response =>
	        {
	          if(response.message == 'success'){
	          	this.errorSaveMsg = null;
	          	this.dialogRef.close({fileName: response.media.image, videoIframe : 'video'});
	          }
	          else{
	          	this.errorSaveMsg = 'File is not Saved';
	          }
	        },
	        error => this.errorSaveMsg = error);
    }
    else if (this.mediaForm.get('bgmediatype').value === 'upload_url') {
      
      let entredURL = this.mediaForm.get('external_url').value;
      if (entredURL.includes('youtube')) {
        let youtubeID = this.getYoutubeId(this.mediaForm.get('external_url').value);
        // this.dialogRef.close({fileName: '//www.youtube.com/embed/'+youtubeID, videoIframe : 'iframe'});
        this.dialogRef.close({fileName: 'https://www.youtube.com/embed/'+youtubeID+'?autoplay=1&loop=1', videoIframe : 'iframe', thumbnail : 'https://img.youtube.com/vi/'+youtubeID+'/0.jpg', });
      }
      else {
        this.dialogRef.close({fileName: this.mediaForm.get('external_url').value, videoIframe : 'iframe'});     
      }
    }
    else{
  	  this.dialogRef.close({fileName: this.mediaForm.get('media').value, videoIframe : 'video'});
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  //trackBy
  trackByMedia(index: number, media: any): number {
  return media.media_id;
  }

}

export interface DialogData {
  media_url: string;
  modal_type: string;
  uploadedfile: string;
}