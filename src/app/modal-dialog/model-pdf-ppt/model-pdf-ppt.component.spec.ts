import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelPdfPptComponent } from './model-pdf-ppt.component';

describe('ModelPdfPptComponent', () => {
  let component: ModelPdfPptComponent;
  let fixture: ComponentFixture<ModelPdfPptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelPdfPptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelPdfPptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
