import { Component, OnInit, Output, AfterViewInit, Injectable, EventEmitter, ViewChild, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { environment } from '../../../environments/environment';
@Component({
  selector: 'app-tiny-mce',
  templateUrl: './tiny-mce.component.html',
  styleUrls: ['./tiny-mce.component.css']
})
export class TinyMceComponent implements OnInit,AfterViewInit {

  showEditor: any=false;
  tinyKey: any = "";
  editorData: any="";	
  public tinyMceSettings = {
    plugins: 'link insertdatetime paste code',
    menubar:false,
    statusbar: false,
    paste_as_text:true,
    fontsize_formats: '11px 12px 14px 16px 18px 24px 36px 48px',
    toolbar:'fontsizeselect | fontselect | formatselect | bold italic underline strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify | pastetext undo redo code',
    font_formats: 'Roboto=Roboto,sans-serif;Andale Mono=andale mono,monospace;Arial=arial,helvetica,sans-serif;Arial Black=arial black,sans-serif;Book Antiqua=book antiqua,palatino,serif;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier,monospace;Georgia=georgia,palatino,serif;Helvetica=helvetica,arial,sans-serif;Impact=impact,sans-serif;sans-serif=sans-serif,sans-serif',
    content_css: [
      environment.tinyMceCss
    ],
    setup : function(ed) {
        ed.on('init', function (ed) {
            //ed.target.editorCommands.execCommand("fontName", false, "");
        });
    }
  };
  constructor(
  	public dialogRef: MatDialogRef<TinyMceComponent>,
  	public dialog: MatDialog,
  	@Inject(MAT_DIALOG_DATA) public dialogdata: any
  	) {
  	this.tinyKey = environment.tinyMceKey
  }

  ngOnInit() {
  	this.editorData = this.dialogdata;
  }
  ngAfterViewInit() {
    /*setTimeout(() => {
        this.showEditor = true;
    }, 1000);*/
  	
  }

  loadedMCE(){
    setTimeout(() => {
        this.showEditor = true;
    }, 1000);
  }
  saveEditorInfo() {
  	this.dialogRef.close({'type': 'submit', 'content': this.editorData});
  }

}
