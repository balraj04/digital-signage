import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TinyMceComponent } from './tiny-mce.component';
import { EditorModule } from '@tinymce/tinymce-angular';
import { MatProgressSpinnerModule } from '@angular/material';
@NgModule({
  declarations: [TinyMceComponent],
  imports: [
    CommonModule,
    FormsModule,
    EditorModule,
    MatProgressSpinnerModule
  ],
  entryComponents:[TinyMceComponent],
  exports: [ TinyMceComponent ]
})
export class TinyMceModule { }
