import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, PageEvent } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-save-templates-modal',
  templateUrl: './save-templates-modal.component.html',
  styleUrls: ['./save-templates-modal.component.css']
})
export class SaveTemplatesModalComponent implements OnInit {

	template_name : string ='';
	delete_template : number = 0;
  	public mediaForm: FormGroup; 	

  	constructor(private fb: FormBuilder,
  	public dialogRef: MatDialogRef<SaveTemplatesModalComponent>,
  	@Inject(MAT_DIALOG_DATA) public dailogData: DialogData) { 
  		if (this.dailogData.template_name) {
	      	this.template_name = this.dailogData.template_name;
	      	this.delete_template = this.dailogData.delete_template;
	    }
	    if (this.dailogData.delete_template) {
	      	this.delete_template = this.dailogData.delete_template;
	    }
  	}

  	ngOnInit() {
	    this.mediaForm = this.fb.group({	      
	      	template_name: this.fb.control(this.template_name, [Validators.required])
	    });    
  	}

  	submitMedia() {
	    let template_name = this.mediaForm.get('template_name').value;	    
	    this.dialogRef.close({templateName: this.mediaForm.get('template_name').value });     
	}

	deleteTemplate(submitValue='N') {
	    this.dialogRef.close({deletetemplate: submitValue});     
	}

	onNoClick(): void {
	    this.dialogRef.close({deletetemplate: 'N'});
	}
}

export interface DialogData {
  template_name: string;
  delete_template: number;
}
