import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveTemplatesModalComponent } from './save-templates-modal.component';

describe('SaveTemplatesModalComponent', () => {
  let component: SaveTemplatesModalComponent;
  let fixture: ComponentFixture<SaveTemplatesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveTemplatesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveTemplatesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
