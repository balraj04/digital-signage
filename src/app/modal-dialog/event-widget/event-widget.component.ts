import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, PageEvent } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-event-widget',
  templateUrl: './event-widget.component.html',
  styleUrls: ['./event-widget.component.css']
})
export class EventWidgetComponent implements OnInit {

	public eventSettingForm: FormGroup;

  constructor(
  	private fb: FormBuilder,
  	public dialogRef: MatDialogRef<EventWidgetComponent>,
  	@Inject(MAT_DIALOG_DATA) public dailogData: DialogData) { }

  ngOnInit() {
  	this.eventSettingForm = this.fb.group({
	  widgetLimit: this.fb.control(this.dailogData.limit, [Validators.required]),
	  widgetType: this.fb.control(this.dailogData.widget_type, [Validators.required])
	});
  }

  submitEventSetting() {
  	this.dialogRef.close({widgetLimit: this.eventSettingForm.get('widgetLimit').value, widgetType: this.eventSettingForm.get('widgetType').value});
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

export interface DialogData {
  widget_type: string;
  limit: string;
}