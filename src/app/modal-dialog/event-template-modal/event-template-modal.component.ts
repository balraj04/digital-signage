// import { Component, OnInit } from '@angular/core';

import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, PageEvent } from '@angular/material';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonBridgeService } from './../../services/common-bridge.service';
import { SharedService } from './../../services/shared.service';
import { environment } from '../../../environments/environment';


const LIMIT = environment.limit;

@Component({
  selector: 'app-event-template-modal',
  templateUrl: './event-template-modal.component.html',
  styleUrls: ['./event-template-modal.component.css']
})
export class EventTemplateModalComponent implements OnInit {

  fileName : string; 
  mediaUrl = environment.mediaUrl;
  baseUrl = environment.playlistURL;
  public templateForm: FormGroup;  

  constructor(
  	private commonBridgeService: CommonBridgeService,
    private sharedService: SharedService,
  	private _sanitizer: DomSanitizer,
  	private fb: FormBuilder,
  	public dialogRef: MatDialogRef<EventTemplateModalComponent>,
  	@Inject(MAT_DIALOG_DATA) public dailogData: DialogData) {    
  	
  }

  ngOnInit() {

    this.templateForm = this.fb.group({
      layout_id: this.fb.control(this.dailogData.layout_id, [Validators.required])
    });
  }  
 
  submitTemplate() {
  	let tmplData = this.templateForm.value || "";
    // console.log('fieldData ==>>', tmplData);
    this.dialogRef.close(tmplData);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }  
}

export interface DialogData {
  layout_id: string;  
}