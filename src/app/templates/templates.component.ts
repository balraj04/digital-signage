import { Component } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.css']
})
export class TemplatesComponent {
  
  title: string;
  slide_thumb: string;
  frameArea: any = [];  
  constructor(){
    this.title = 'Template Gallery';
    this.slide_thumb = 'assets/slides/slides-thumb.png';
  }

  hideIframeArea(event:any){
    this.frameArea = document.getElementsByClassName('iframearea');
    if(this.frameArea.length>0)
    {
      this.frameArea[0].style = 'display:none;';      
    }
  }

  showIframeArea(event:any){
    this.frameArea = document.getElementsByClassName('iframearea');
    if(this.frameArea.length>0)
    {
      this.frameArea[0].style = 'display:block;';
    }
  }
  

}
