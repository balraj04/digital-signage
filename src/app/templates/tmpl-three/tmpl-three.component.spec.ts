import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmplThreeComponent } from './tmpl-three.component';

describe('TmplThreeComponent', () => {
  let component: TmplThreeComponent;
  let fixture: ComponentFixture<TmplThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmplThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmplThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
