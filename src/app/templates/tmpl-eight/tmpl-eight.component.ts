import { Component, OnInit, AfterViewInit, OnChanges, OnDestroy, Inject, Input, ElementRef,Renderer } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ISubscription } from "rxjs/Subscription";
import { ModalDialogComponent } from './../../modal-dialog/modal-dialog.component';
import { TinyMceComponent } from '../../modal-dialog/tiny-mce/tiny-mce.component'; //rich text editior
import { SharedService } from './../../services/shared.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-tmpl-eight',
  templateUrl: './tmpl-eight.component.html',
  styleUrls: ['./tmpl-eight.component.css']
})
export class TmplEightComponent implements OnInit {

  public blurDefault = environment.blurDefault;
	public lefttransvisibility = 'hidden';
	public righttransvisibility = 'hidden';
	public offset = environment.imgoffset;
	private subscription: ISubscription;
	private editMode: Boolean = false;
	private tmplformInfo = new FormData();
	private background: any = {leftbgimage:"",rightbgimage:""};
	private templatedata : any = {
		pretitle:"pretitle",
		title:"title",
		posttitle:"posttitle",
		description:"Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio",
		styled:false,
		leftcolsettings: {
			txtcolor: "#000",
		    bgtype: "bgcolor",
		    bgcolor: "#f2f2f2",
		    bgimage: "",
		    bgrepeat: "no-repeat",
		    bgsize: "auto",
		    bgposition: "top left"
		},
		rightcolsettings: {
			txtcolor: "#000",
		    bgtype: "bgcolor",
		    bgcolor: "#f2f2f2",
		    bgimage: "",
		    bgrepeat: "no-repeat",
		    bgsize: "auto",
		    bgposition: "top left"
		},
		leftcolimage: "",
		rightcolimage: ""
	};

	mediaUrl = environment.mediaUrl;
	dateTemp : any = Math.random();

	@Input() type: string = 'add';
	@Input() is_valide: string = 'yes';
	@Input() community_footer: any;
	@Input() scroll_announcement: any;
	
	screenHeight: any;
    screenWidth: any;
    moreHeight: boolean;
    scaleCount:any;
    translateCount:any;
    editorBlockStyle: any = {};
    editorBlock: any;
    editorHeight: any;
    editorWidth: any;
    scaleBlock: any;

  	constructor(
  		private sharedService: SharedService, 
  		public dialog: MatDialog, 
  		private _sanitizer: DomSanitizer,
  		private router: Router,
  		private el:ElementRef) 
  	{	
  		// if (this.router.url.includes("/edit-slide/")) {
  			this.editMode = true;
  			this.sharedService.currEditData.subscribe( editData => {	  			
	  			this.templatedata = editData ? editData : this.templatedata;			
	  		});
	  		this.background.leftbgimage	= this.mediaUrl + this.templatedata.leftcolsettings.bgimage;
	  		this.background.rightbgimage = this.mediaUrl + this.templatedata.rightcolsettings.bgimage;
	  	// }  		
  	}
  	//OPEN TinyMceComponent DIALOG FOR TEXT EDITING
	openEditor(editorInfo:any="",tmplType: string="") {
		const dialogRef = this.dialog.open(TinyMceComponent,{data:editorInfo});
	    dialogRef.afterClosed().subscribe(result => {
	    	if(result.type == 'submit') {
	    		this.templatedata[tmplType] = result.content;
	    		this.ngOnChanges();
	    	}
	    });
	}
  	openDialog(coltype:String="",isText=true): void {
  		let ModelData;
  		if (this.editMode) {
  			ModelData = {
		      	isText:isText,
		      	column:coltype,
		      	modal_type: 'slide_options',
		      	txtcolor: coltype == 'left' ? this.templatedata.leftcolsettings.txtcolor : this.templatedata.rightcolsettings.txtcolor,
		      	bgcolor: coltype == 'left' ? this.templatedata.leftcolsettings.bgcolor : this.templatedata.rightcolsettings.bgcolor, 
		      	bgtype: coltype == 'left' ? this.templatedata.leftcolsettings.bgtype : this.templatedata.rightcolsettings.bgtype, 
		      	bgimage: coltype == 'left' ? this.templatedata.leftcolsettings.bgimage : this.templatedata.rightcolsettings.bgimage,
		      	bgrepeat: coltype == 'left' ? this.templatedata.leftcolsettings.bgrepeat : this.templatedata.rightcolsettings.bgrepeat,
		      	bgsize: coltype == 'left' ? this.templatedata.leftcolsettings.bgsize : this.templatedata.rightcolsettings.bgsize,
		      	bgposition: coltype == 'left' ? this.templatedata.leftcolsettings.bgposition : this.templatedata.rightcolsettings.bgposition,
		      	type:'edit'
		    };
  		}
  		else {
  			ModelData = {
		      	isText:isText,
		      	column:coltype,
		      	modal_type: 'slide_options',
		      	txtcolor: coltype == 'left' ? this.templatedata.leftcolsettings.txtcolor : this.templatedata.rightcolsettings.txtcolor,
		      	bgcolor: coltype == 'left' ? this.templatedata.leftcolsettings.bgcolor : this.templatedata.rightcolsettings.bgcolor, 
		      	bgtype: coltype == 'left' ? this.templatedata.leftcolsettings.bgtype : this.templatedata.rightcolsettings.bgtype, 
		      	bgimage: coltype == 'left' ? this.templatedata.leftcolsettings.bgimage : this.templatedata.rightcolsettings.bgimage,
		      	bgrepeat: coltype == 'left' ? this.templatedata.leftcolsettings.bgrepeat : this.templatedata.rightcolsettings.bgrepeat,
		      	bgsize: coltype == 'left' ? this.templatedata.leftcolsettings.bgsize : this.templatedata.rightcolsettings.bgsize,
		      	bgposition: coltype == 'left' ? this.templatedata.leftcolsettings.bgposition : this.templatedata.rightcolsettings.bgposition,
		      	type:'add'
	      	};
  		}

	    const dialogRef = this.dialog.open(ModalDialogComponent, {
	      disableClose: true,
	      width: '50%',
	      data: ModelData,
	      panelClass: 'background_modal'
	    });
	}
	
	ngAfterViewChecked () {
		/*this.screenHeight = window.innerHeight;
        this.editorBlock = document.getElementsByClassName('content-col');
        this.scaleBlock = document.getElementsByClassName('scale-block');
        if(this.editorBlock[0] && this.scaleBlock[0]){
	        this.editorWidth = this.editorBlock[0].clientWidth;
			let editorInnerWidth = (this.editorWidth - 30);
		  	this.scaleCount = ( this.editorBlock[0].clientHeight / this.screenHeight);
		  	this.translateCount = ( 100 - ( this.scaleCount * 100 ) ) / 2;
		  	if ( this.scaleCount < 1 ) {
		  		let finalWidth = editorInnerWidth / this.scaleCount;
			  	this.scaleBlock[0].style.transform = 'translate(-'+this.translateCount+'%, -'+this.translateCount+'%) scale('+this.scaleCount+')';
				this.scaleBlock[0].style.width = ' '+finalWidth+'px';
		  	}
		}*/
	}

  	ngAfterViewInit() {
  		this.tmplformInfo.append('SettingsJson', JSON.stringify(this.templatedata));
	    this.sharedService.SharedData.emit(this.tmplformInfo);
	    
	    /*this.editorBlock = document.getElementsByClassName('printable_div');
	    this.scaleBlock  = document.getElementsByClassName('printable_div_trans');
	    
	    if(this.editorBlock[0]){
			this.editorWidth = this.editorBlock[0].clientWidth;
			this.editorHeight = this.editorBlock[0].clientHeight;
			this.scaleCount = Math.min(this.editorWidth / 640, this.editorHeight / 360);
	    }

	    if(this.scaleCount)
	    {
	    	for (var i = this.editorBlock.length - 1; i >= 0; i--) {
	    		this.scaleBlock[i].style.transform = 'translate(-50%, -50%) scale('+this.scaleCount+')';
	    	} 	

	    }*/
  	}
  	ngOnChanges() {  		
	    // changes.prop contains the old and the new value...
	    this.tmplformInfo.append('SettingsJson', JSON.stringify(this.templatedata));
	    this.sharedService.SharedData.emit(this.tmplformInfo);
	}
  	saveEditable(e) {
        this.tmplformInfo.append('SettingsJson', JSON.stringify(this.templatedata));
	    this.sharedService.SharedData.emit(this.tmplformInfo);
    }
    //Set Background Image SafeUrl
    getBackground(image) {
	    return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
	}

	ngOnInit () {
		this.fetchSubscribedData();
		this.screenHeight = window.innerHeight;
		this.screenWidth = window.innerWidth;
		if ( this.screenHeight > this.screenWidth ) {
			this.moreHeight = true;
		} else {
			this.moreHeight = false;
		}
	}

	fetchSubscribedData(){
		this.subscription = this.sharedService.SharedData.subscribe(
          (data: any) => {	            
            this.sharedService.sendSharedJson(data).subscribe(
          		(result: any) => {
          			this.templatedata.styled = result.settings.styled;
          			switch (result.settings.column) {
          				case "left":
          					this.background.leftbgimage					= result.base64image;
            				this.templatedata.leftcolsettings.bgtype	= result.settings.bgtype;
            				this.templatedata.leftcolsettings.bgcolor	= result.settings.bgcolor;
            				this.templatedata.leftcolsettings.bgrepeat	= result.settings.bgrepeat;
            				this.templatedata.leftcolsettings.bgsize	= result.settings.bgsize;
            				this.templatedata.leftcolsettings.bgposition= result.settings.bgposition;
            				this.templatedata.leftcolsettings.txtcolor	= result.settings.txtcolor;
            				this.templatedata.leftcolsettings.bgimage	= result.settings.bgimage;
          					this.templatedata.leftcolimage				= result.bgimage;
          					break;
          				case "right":
          					this.background.rightbgimage				= result.base64image;
            				this.templatedata.rightcolsettings.bgtype	= result.settings.bgtype;
            				this.templatedata.rightcolsettings.bgcolor	= result.settings.bgcolor;
            				this.templatedata.rightcolsettings.bgrepeat	= result.settings.bgrepeat;
            				this.templatedata.rightcolsettings.bgsize	= result.settings.bgsize;
            				this.templatedata.rightcolsettings.bgposition= result.settings.bgposition;
            				this.templatedata.rightcolsettings.txtcolor	= result.settings.txtcolor;
            				this.templatedata.rightcolsettings.bgimage	= result.settings.bgimage;
          					this.templatedata.rightcolimage				= result.bgimage;
          					break;
          				default:
          					break;
          			}
          			this.sharedService.SlideData.emit(this.templatedata);
          	});
        });
	}

	ngOnDestroy() {
	  this.subscription.unsubscribe();
	}
}
