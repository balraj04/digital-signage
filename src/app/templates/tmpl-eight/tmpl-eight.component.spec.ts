import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmplEightComponent } from './tmpl-eight.component';

describe('TmplEightComponent', () => {
  let component: TmplEightComponent;
  let fixture: ComponentFixture<TmplEightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmplEightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmplEightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
