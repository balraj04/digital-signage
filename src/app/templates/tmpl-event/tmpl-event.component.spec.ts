import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmplEventComponent } from './tmpl-event.component';

describe('TmplEventComponent', () => {
  let component: TmplEventComponent;
  let fixture: ComponentFixture<TmplEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmplEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmplEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
