import { Component, OnInit, AfterViewInit, OnChanges, OnDestroy, Inject, Input, ChangeDetectorRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ISubscription } from "rxjs/Subscription";
import { ModalDialogComponent } from './../../modal-dialog/modal-dialog.component';
import { SharedService } from './../../services/shared.service';
import { environment } from '../../../environments/environment';
import { EventTemplateModalComponent } from './../../modal-dialog/event-template-modal/event-template-modal.component';

@Component({
  selector: 'app-tmpl-event',
  templateUrl: './tmpl-event.component.html',
  styleUrls: ['./tmpl-event.component.css']
})
export class TmplEventComponent implements AfterViewInit, OnChanges, OnDestroy {
	public blurDefault = environment.blurDefault;
	public offset = environment.imgoffset;
	public lefttransvisibility = 'hidden';
	public righttransvisibility = 'hidden';
	private subscription: ISubscription;
	private editMode: Boolean = false;
	private tmplformInfo = new FormData();
	private background: any = {leftbgimage:"",rightbgimage:""};
	private layout_Id: string='template_one';
	public templatedata : any = {
		event_start_date: '',
		event_start_time: '',
		event_end_date: '',
		event_end_time: '',
		event_location: '',
		event_title:"Event title",		
		event_desc:"Event sollicitudin. Cras purus odio",
		event_id: '',
		styled:false,
		leftcolsettings: {
			txtcolor: "#000",
		    bgtype: "bgcolor",
		    bgcolor: "#f2f2f2",
		    bgimage: "",
		    bgrepeat: "no-repeat",
		    bgsize: "auto",
		    bgposition: "top left"
		},
		rightcolsettings: {
			txtcolor: "#000",
		    bgtype: "bgcolor",
		    bgcolor: "#f2f2f2",
		    bgimage: "",
		    bgrepeat: "no-repeat",
		    bgsize: "auto",
		    bgposition: "top left"
		},
		leftcolimage: "",
		rightcolimage: "",
		layout_id : 'template_one',
	};

	mediaUrl = environment.mediaUrl;
	dateTemp : any = Math.random();

	@Input() type: string = 'add';
	@Input() is_valide: string = 'yes';
	@Input() community_footer: any;
	@Input() scroll_announcement: any;
	
	screenHeight: any;
    screenWidth: any;
    moreHeight: boolean;
    scaleCount:any; 
    translateCount:any;
    editorBlockStyle: any = {};
    editorBlock: any;
    editorHeight: any;
    editorWidth: any;
    scaleBlock: any;
    diffEvent: boolean = false;    

  	constructor(
  		private cdr: ChangeDetectorRef,
  		private sharedService: SharedService, 
  		public dialog: MatDialog, 
  		private _sanitizer: DomSanitizer,
  		private router: Router ) 
  	{	
  		if (this.router.url.includes("/edit-slide/")) {
  			this.editMode = true;
  			this.sharedService.currEditData.subscribe( editData => {	  			
	  			this.templatedata = editData ? editData : this.templatedata;
	  		});
	  		this.background.leftbgimage	= this.templatedata.leftcolsettings!==undefined ? this.mediaUrl + this.templatedata.leftcolsettings.bgimage :"";
	  		this.background.rightbgimage = this.templatedata.rightcolsettings!==undefined ? this.mediaUrl + this.templatedata.rightcolsettings.bgimage :"";
	  	}

  		this.subscription = this.sharedService.SharedData.subscribe(
	          (data: any) => {	            
	            this.sharedService.sendSharedJson(data).subscribe(
	          		(result: any) => {	          			
	          			this.templatedata.styled = result.settings.styled;
	          			
	          			this.templatedata.layout_id = result.layout_id ? result.layout_id : this.templatedata.layout_id ? this.templatedata.layout_id : this.layout_Id ;
	          			switch (result.settings.column) {
	          				case "left":
	          					this.background.leftbgimage					= result.base64image;
	            				this.templatedata.leftcolsettings.bgtype	= result.settings.bgtype;
	            				this.templatedata.leftcolsettings.bgcolor	= result.settings.bgcolor;
	            				this.templatedata.leftcolsettings.bgrepeat	= result.settings.bgrepeat;
	            				this.templatedata.leftcolsettings.bgsize	= result.settings.bgsize;
	            				this.templatedata.leftcolsettings.bgposition= result.settings.bgposition;
	            				this.templatedata.leftcolsettings.txtcolor	= result.settings.txtcolor;
	            				this.templatedata.leftcolsettings.bgimage	= result.settings.bgimage;
	          					this.templatedata.leftcolimage				= result.bgimage;
	          					break;
	          				case "right":
	          					this.background.rightbgimage				= result.base64image;
	            				this.templatedata.rightcolsettings.bgtype	= result.settings.bgtype;
	            				this.templatedata.rightcolsettings.bgcolor	= result.settings.bgcolor;
	            				this.templatedata.rightcolsettings.bgrepeat	= result.settings.bgrepeat;
	            				this.templatedata.rightcolsettings.bgsize	= result.settings.bgsize;
	            				this.templatedata.rightcolsettings.bgposition= result.settings.bgposition;
	            				this.templatedata.rightcolsettings.txtcolor	= result.settings.txtcolor;
	            				this.templatedata.rightcolsettings.bgimage	= result.settings.bgimage;
	          					this.templatedata.rightcolimage				= result.bgimage;
	          					break;
	          				default:
	          					break;
	          			}
	          			
	          			this.sharedService.SlideData.emit(this.templatedata);
	          	});
	        });
  	}

  	openDialog(coltype:String="",isText=true): void {
  		let ModelData;
  		if (this.editMode) {
  			ModelData = {
  				isText:isText,
		      	column:coltype,
		      	modal_type: 'slide_options',
		      	txtcolor: coltype == 'left' ? this.templatedata.leftcolsettings.txtcolor : this.templatedata.rightcolsettings.txtcolor,
		      	bgcolor: coltype == 'left' ? this.templatedata.leftcolsettings.bgcolor : this.templatedata.rightcolsettings.bgcolor, 
		      	bgtype: coltype == 'left' ? this.templatedata.leftcolsettings.bgtype : this.templatedata.rightcolsettings.bgtype, 
		      	bgimage: coltype == 'left' ? this.templatedata.leftcolsettings.bgimage : this.templatedata.rightcolsettings.bgimage,
		      	bgrepeat: coltype == 'left' ? this.templatedata.leftcolsettings.bgrepeat : this.templatedata.rightcolsettings.bgrepeat,
		      	bgsize: coltype == 'left' ? this.templatedata.leftcolsettings.bgsize : this.templatedata.rightcolsettings.bgsize,
		      	bgposition: coltype == 'left' ? this.templatedata.leftcolsettings.bgposition : this.templatedata.rightcolsettings.bgposition,
		      	type:'edit'
		    };
  		}
  		else {
  			ModelData = {
  				isText:isText,
		      	column:coltype,
		      	modal_type: 'slide_options',
		      	txtcolor: coltype == 'left' ? this.templatedata.leftcolsettings.txtcolor : this.templatedata.rightcolsettings.txtcolor,
		      	bgcolor: coltype == 'left' ? this.templatedata.leftcolsettings.bgcolor : this.templatedata.rightcolsettings.bgcolor, 
		      	bgtype: coltype == 'left' ? this.templatedata.leftcolsettings.bgtype : this.templatedata.rightcolsettings.bgtype, 
		      	bgimage: coltype == 'left' ? this.templatedata.leftcolsettings.bgimage : this.templatedata.rightcolsettings.bgimage,
		      	bgrepeat: coltype == 'left' ? this.templatedata.leftcolsettings.bgrepeat : this.templatedata.rightcolsettings.bgrepeat,
		      	bgsize: coltype == 'left' ? this.templatedata.leftcolsettings.bgsize : this.templatedata.rightcolsettings.bgsize,
		      	bgposition: coltype == 'left' ? this.templatedata.leftcolsettings.bgposition : this.templatedata.rightcolsettings.bgposition,
		      	type:'add'
	      	};
  		}

	    const dialogRef = this.dialog.open(ModalDialogComponent, {
	      disableClose: true,
	      width: '50%',
	      data: ModelData
	    });

	    //dialogRef.afterClosed().subscribe(result => {});

	}
	
	changeTemplate() {		
		let ModelData;
		ModelData = {
			layout_id: this.templatedata.layout_id,
	    };

	    const dialogRef = this.dialog.open(EventTemplateModalComponent, {
	      disableClose: true,
	      width: '50%',
	      data: ModelData
	    });

	    dialogRef.afterClosed().subscribe(result => {
	     
	      if(result){
	      	this.templatedata.layout_id = result.layout_id;
	      	
	      	this.sharedService.SharedData.emit(this.tmplformInfo);
	      }

	    });
	}

  	ngAfterViewInit() {
  		this.tmplformInfo.append('SettingsJson', JSON.stringify(this.templatedata));
	    this.sharedService.SharedData.emit(this.tmplformInfo);
	    this.cdr.detectChanges();
	    /*this.editorBlock = document.getElementsByClassName('printable_div');
	    this.scaleBlock  = document.getElementsByClassName('printable_div_trans');
	    
	    if(this.editorBlock[0]){
			this.editorWidth = this.editorBlock[0].clientWidth;
			this.editorHeight = this.editorBlock[0].clientHeight;
			this.scaleCount = Math.min(this.editorWidth / 640, this.editorHeight / 360);
	    }

	    if(this.scaleCount)
	    {
	    	for (var i = this.editorBlock.length - 1; i >= 0; i--) {
	    		this.scaleBlock[i].style.transform = 'translate(-50%, -50%) scale('+this.scaleCount+')';
	    	} 	

	    }*/
  	}
  	ngOnChanges() {  		
	    // changes.prop contains the old and the new value...
	    this.tmplformInfo.append('SettingsJson', JSON.stringify(this.templatedata));
	    this.sharedService.SharedData.emit(this.tmplformInfo);
	}
  	saveEditable(e) {        
        this.tmplformInfo.append('SettingsJson', JSON.stringify(this.templatedata));
	    this.sharedService.SharedData.emit(this.tmplformInfo);
    }
    //Set Background Image SafeUrl
    getBackground(image) {
	    return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
	}

	ngOnInit () {
		this.templatedata.event_desc = this.templatedata.event_desc.replace(/ *\[[^\]]*]/g, '');
		this.diffEvent = ((this.templatedata.event_end_date) && (this.templatedata.event_start_date != this.templatedata.event_end_date)) ? true : false;
		this.templatedata.event_end_time =  (this.templatedata.event_end_time) ? this.templatedata.event_end_time : (this.templatedata.event_start_time) ? this.templatedata.event_start_time : '11:59 PM';
		this.templatedata.event_start_time = (this.templatedata.event_start_time) ? this.templatedata.event_start_time : '08:00 am';

		this.screenHeight = window.innerHeight;
		this.screenWidth = window.innerWidth;
		if ( this.screenHeight > this.screenWidth ) {
			this.moreHeight = true;
		} else {
			this.moreHeight = false;
		}
	}

	ngOnDestroy() {
	  this.subscription.unsubscribe();
	}
	
}
