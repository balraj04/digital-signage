import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmplOneComponent } from './tmpl-one.component';

describe('TmplOneComponent', () => {
  let component: TmplOneComponent;
  let fixture: ComponentFixture<TmplOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmplOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmplOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
