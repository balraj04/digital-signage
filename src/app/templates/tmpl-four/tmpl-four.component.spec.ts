import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmplFourComponent } from './tmpl-four.component';

describe('TmplFourComponent', () => {
  let component: TmplFourComponent;
  let fixture: ComponentFixture<TmplFourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmplFourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmplFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
