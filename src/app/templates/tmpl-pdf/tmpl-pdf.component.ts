import { Component, OnInit, AfterViewInit, OnChanges, OnDestroy, Inject, Input, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { ISubscription } from "rxjs/Subscription";

import { ModelPdfPptComponent } from './../../modal-dialog/model-pdf-ppt/model-pdf-ppt.component';

import { SharedService } from './../../services/shared.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-tmpl-pdf',
  templateUrl: './tmpl-pdf.component.html',
  styleUrls: ['./tmpl-pdf.component.css']
})
export class TmplPdfComponent implements AfterViewInit, OnChanges, OnDestroy {

	@ViewChild("iframe") iframe: ElementRef;

	public blurDefault = environment.blurDefault;
	public offset = environment.imgoffset;
	public lefttransvisibility = 'hidden';
	public righttransvisibility = 'hidden';
	private subscription: ISubscription;
	private editMode: Boolean = false;
	private tmplformInfo = new FormData();

	private templatedata : any = {
		url:""
	};

	mediaUrl = environment.mediaUrl;
	dateTemp : any = Math.random();

	tmpUrl : any = '';
	fileNameUrl : any = '';

	@Input() type: string = 'add';
	@Input() is_valide: string = 'yes';
	@Input() community_footer: any;
	@Input() scroll_announcement: any;
	
	screenHeight: any;
    screenWidth: any;
    moreHeight: boolean;
    scaleCount:any; 
    translateCount:any;
    editorBlockStyle: any = {};
    editorBlock: any;
    editorHeight: any;
    editorWidth: any;
    scaleBlock: any;
    
  	constructor(
  		private sharedService: SharedService, 
  		public dialog: MatDialog, 
  		private _sanitizer: DomSanitizer,
  		private router: Router,
  		private route: ActivatedRoute ) {
  		if (this.router.url.includes("/edit-slide/")) {
  			this.editMode = true;
  			this.sharedService.currEditData.subscribe( editData => {	  			
	  			this.templatedata = editData ? editData : this.templatedata;			
	  		});
	  	}
  	}

  	openDialog(coltype:String=""): void {
  		let	ModelData = {
	      	column:coltype,
	      	modal_type: 'pdf_options',
	      	uploadedfile: (this.templatedata.url) ? this.templatedata.url : '',		      	
      	};

	    const dialogRef = this.dialog.open(ModelPdfPptComponent, {
	      disableClose: true,
	      width: '50%',
	      data: ModelData,
	      panelClass: 'background_modal'
	    });

	    dialogRef.afterClosed().subscribe(result => {
	      if(result){
	      	let fileName = result.fileName;
		    this.templatedata.url = fileName;
	      	if (fileName !== '') {
	      		let tmpUrl;
	      		if (fileName.includes('.pdf')) {
		      		//tmpUrl = 'https://docs.google.com/gview?url='+this.mediaUrl + fileName+'&embedded=true#:0.page.20';
		      		tmpUrl = this.mediaUrl + fileName +'#toolbar=0&view=FitH';
		  		}
		  		else {
		  			tmpUrl = 'https://docs.google.com/gview?url='+this.mediaUrl + fileName+'&embedded=true';
		  		}
		  		// this.fileNameUrl = this._sanitizer.bypassSecurityTrustResourceUrl(tmpUrl);
		  		this.route.params.subscribe( params => {
			        this.iframe.nativeElement.contentWindow.location.replace(tmpUrl);
			    });
	      	}
	      	else{
	  			this.fileNameUrl = this._sanitizer.bypassSecurityTrustResourceUrl('');
	  		}
	      }
	    });
	}
	
  	ngAfterViewInit() {
  		this.tmplformInfo.append('SettingsJson', JSON.stringify(this.templatedata));
	    this.sharedService.SharedData.emit(this.tmplformInfo);
  	}
  	ngOnChanges() {
  		// changes.prop contains the old and the new value...
	    this.tmplformInfo.append('SettingsJson', JSON.stringify(this.templatedata));
	    this.sharedService.SharedData.emit(this.tmplformInfo);
	}

	ngOnInit () {
		this.screenHeight = window.innerHeight;
		this.screenWidth = window.innerWidth;
		if ( this.screenHeight > this.screenWidth ) {
			this.moreHeight = true;
		} else {
			this.moreHeight = false;
		}

		if(this.type != 'print'){
		  	this.subscription = this.sharedService.SharedData.subscribe(
	          (data: any) => {        
	            this.sharedService.sendSharedJson(data).subscribe(
	          		(result: any) => {
	          			this.templatedata.url = result.settings.url;
	      				this.sharedService.SlideData.emit(this.templatedata);
	          	});
	        });
	  	}

	  	this.trustURL();
	}

	trustURL(){
		if(this.templatedata.url){
			let tmpUrl;
			if (this.templatedata.url.includes('.pdf')) {
	  			//tmpUrl = 'https://docs.google.com/gview?url='+this.mediaUrl + this.templatedata.url+'&embedded=true#:0.page.20';
	  			tmpUrl = this.mediaUrl + this.templatedata.url+'#toolbar=0&view=FitH';
	  		}
	  		else{
	  			tmpUrl = 'https://docs.google.com/gview?url='+this.mediaUrl + this.templatedata.url+'&embedded=true';
	  		}
			this.fileNameUrl = this._sanitizer.bypassSecurityTrustResourceUrl(tmpUrl);
		}
		else{
			this.fileNameUrl = this._sanitizer.bypassSecurityTrustResourceUrl('');
		}
	}

	ngOnDestroy() {
	  if(this.type != 'print'){
  		this.subscription.unsubscribe();
  	  }
	}
}
