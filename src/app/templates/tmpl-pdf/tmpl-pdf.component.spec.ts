import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmplPdfComponent } from './tmpl-pdf.component';

describe('TmplPdfComponent', () => {
  let component: TmplPdfComponent;
  let fixture: ComponentFixture<TmplPdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmplPdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmplPdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
