import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmplSevenComponent } from './tmpl-seven.component';

describe('TmplSevenComponent', () => {
  let component: TmplSevenComponent;
  let fixture: ComponentFixture<TmplSevenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmplSevenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmplSevenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
