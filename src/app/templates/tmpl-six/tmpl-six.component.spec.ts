import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmplSixComponent } from './tmpl-six.component';

describe('TmplSixComponent', () => {
  let component: TmplSixComponent;
  let fixture: ComponentFixture<TmplSixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmplSixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmplSixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
