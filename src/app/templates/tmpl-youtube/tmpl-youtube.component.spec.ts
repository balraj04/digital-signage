import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmplYoutubeComponent } from './tmpl-youtube.component';

describe('TmplYoutubeComponent', () => {
  let component: TmplYoutubeComponent;
  let fixture: ComponentFixture<TmplYoutubeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmplYoutubeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmplYoutubeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
