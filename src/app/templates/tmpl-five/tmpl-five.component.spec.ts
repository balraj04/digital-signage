import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmplFiveComponent } from './tmpl-five.component';

describe('TmplFiveComponent', () => {
  let component: TmplFiveComponent;
  let fixture: ComponentFixture<TmplFiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmplFiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmplFiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
