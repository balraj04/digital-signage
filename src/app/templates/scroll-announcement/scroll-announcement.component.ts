import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-scroll-announcement',
  templateUrl: './scroll-announcement.component.html',
  styleUrls: ['./scroll-announcement.component.css']
})
export class ScrollAnnouncementComponent implements OnInit {

	@Input() scroll_text : any;
  	constructor() {	}

  	ngOnInit() {
  	}

}
