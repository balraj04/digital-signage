import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { environment } from './../../../environments/environment';

@Component({
  selector: 'app-community-footer',
  templateUrl: './community-footer.component.html',
  styleUrls: ['./community-footer.component.css']
})
export class CommunityFooterComponent implements OnInit {
  public blurDefault = environment.blurDefault;
  public offset = environment.imgoffset;
	errorMsg: any;
  now: number;
  default_logo: string = environment.spDefaultLogo;
  mediaUrl = environment.mediaUrl;
  dateTemp : any = Math.random();
  interval: any;

  @Input() community_footer : any;

  constructor(
    public _sanitizer: DomSanitizer) {
    this.now = Date.now();
    this.interval = setInterval(() => {
      this.now = Date.now();
    }, 10000);
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }
}
