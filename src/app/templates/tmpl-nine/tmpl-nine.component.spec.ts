import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmplNineComponent } from './tmpl-nine.component';

describe('TmplNineComponent', () => {
  let component: TmplNineComponent;
  let fixture: ComponentFixture<TmplNineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmplNineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmplNineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
