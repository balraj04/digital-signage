import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmplAnnouncementWidgetComponent } from './tmpl-announcement-widget.component';

describe('TmplAnnouncementWidgetComponent', () => {
  let component: TmplAnnouncementWidgetComponent;
  let fixture: ComponentFixture<TmplAnnouncementWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmplAnnouncementWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmplAnnouncementWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
