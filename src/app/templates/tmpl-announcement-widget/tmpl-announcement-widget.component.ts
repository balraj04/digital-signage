import { Component, OnInit, AfterViewInit, OnChanges, OnDestroy, Inject, Input, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { ISubscription } from "rxjs/Subscription";

import { AnnouncementWidgetComponent } from './../../modal-dialog/announcement-widget/announcement-widget.component';
import { ModalDialogComponent } from './../../modal-dialog/modal-dialog.component';

import { SharedService } from './../../services/shared.service';
import { CommonBridgeService } from './../../services/common-bridge.service';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-tmpl-announcement-widget',
  templateUrl: './tmpl-announcement-widget.component.html',
  styleUrls: ['./tmpl-announcement-widget.component.css']
})
export class TmplAnnouncementWidgetComponent implements OnInit {
	public blurDefault = environment.blurDefault;
	public offset = environment.imgoffset;
	public lefttransvisibility = 'hidden';
	public righttransvisibility = 'hidden';
	private subscription: ISubscription;
	private editMode: Boolean = false;
	private tmplformInfo = new FormData();
	private background: any = {leftbgimage:"",rightbgimage:""};

	private templatedata : any = {
		announcements:{
			dining:[],
			homepage:[]
		},
		widget_type:"homepage",
		limit: "4",
		HomeAnnounceId: 0,
		styled:false,
		leftcolsettings: {
			txtcolor: "#000",
		    bgtype: "bgcolor",
		    bgcolor: "#f2f2f2",
		    bgimage: "",
		    bgrepeat: "no-repeat",
		    bgsize: "auto",
		    bgposition: "top left"
		},
		rightcolsettings: {
			txtcolor: "#000",
		    bgtype: "bgcolor",
		    bgcolor: "#f2f2f2",
		    bgimage: "",
		    bgrepeat: "no-repeat",
		    bgsize: "auto",
		    bgposition: "top left"
		},
		leftcolimage: "",
		rightcolimage: ""
	};

	mediaUrl = environment.mediaUrl;
	dateTemp : any = Math.random();

	tmpUrl : any = '';
	fileNameUrl : any = '';

	@Input() type: string = 'add';
	@Input() is_valide: string = 'yes';
	@Input() community_footer: any;
	@Input() scroll_announcement: any;

	screenHeight: any;
    screenWidth: any;
    moreHeight: boolean;
    scaleCount:any; 
    translateCount:any;
    editorBlockStyle: any = {};
    editorBlock: any;
    editorHeight: any;
    editorWidth: any;
    scaleBlock: any;

	constructor(
  		private commonBridgeService:CommonBridgeService,
  		private sharedService: SharedService, 
  		public dialog: MatDialog, 
  		private _sanitizer: DomSanitizer,
  		private router: Router,
  		private route: ActivatedRoute ) {

  		//Get Playlist Dropdown Api
	    this.commonBridgeService.getDroplist('commonlistinfo').subscribe(response => {
	      if(response.message == 'success'){	        
	        this.templatedata.announcements = response.announcements;	        
	      }
	    });

  		if (this.router.url.includes("/edit-slide/")) {
  			this.editMode = true;
  			this.sharedService.currEditData.subscribe( editData => {	  			
	  			this.templatedata = editData ? editData : this.templatedata;			
	  		});
	  		this.background.leftbgimage	= this.mediaUrl + this.templatedata.leftcolsettings.bgimage;
	  		this.background.rightbgimage = this.mediaUrl + this.templatedata.rightcolsettings.bgimage;
	  	}

  		
  	}

  	openDialog(coltype:String="",isText=true): void {
  		let ModelData;
  		if (this.editMode) {
  			ModelData = {
		      	isText:isText,
		      	column:coltype,
		      	modal_type: 'slide_options',
		      	txtcolor: coltype == 'left' ? this.templatedata.leftcolsettings.txtcolor : this.templatedata.rightcolsettings.txtcolor,
		      	bgcolor: coltype == 'left' ? this.templatedata.leftcolsettings.bgcolor : this.templatedata.rightcolsettings.bgcolor, 
		      	bgtype: coltype == 'left' ? this.templatedata.leftcolsettings.bgtype : this.templatedata.rightcolsettings.bgtype, 
		      	bgimage: coltype == 'left' ? this.templatedata.leftcolsettings.bgimage : this.templatedata.rightcolsettings.bgimage,
		      	bgrepeat: coltype == 'left' ? this.templatedata.leftcolsettings.bgrepeat : this.templatedata.rightcolsettings.bgrepeat,
		      	bgsize: coltype == 'left' ? this.templatedata.leftcolsettings.bgsize : this.templatedata.rightcolsettings.bgsize,
		      	bgposition: coltype == 'left' ? this.templatedata.leftcolsettings.bgposition : this.templatedata.rightcolsettings.bgposition,
		      	type:'edit'
		    };
  		}
  		else {
  			ModelData = {
		      	isText:isText,
		      	column:coltype,
		      	modal_type: 'slide_options',
		      	txtcolor: coltype == 'left' ? this.templatedata.leftcolsettings.txtcolor : this.templatedata.rightcolsettings.txtcolor,
		      	bgcolor: coltype == 'left' ? this.templatedata.leftcolsettings.bgcolor : this.templatedata.rightcolsettings.bgcolor, 
		      	bgtype: coltype == 'left' ? this.templatedata.leftcolsettings.bgtype : this.templatedata.rightcolsettings.bgtype, 
		      	bgimage: coltype == 'left' ? this.templatedata.leftcolsettings.bgimage : this.templatedata.rightcolsettings.bgimage,
		      	bgrepeat: coltype == 'left' ? this.templatedata.leftcolsettings.bgrepeat : this.templatedata.rightcolsettings.bgrepeat,
		      	bgsize: coltype == 'left' ? this.templatedata.leftcolsettings.bgsize : this.templatedata.rightcolsettings.bgsize,
		      	bgposition: coltype == 'left' ? this.templatedata.leftcolsettings.bgposition : this.templatedata.rightcolsettings.bgposition,
		      	type:'add'
	      	};
  		}

	    const dialogRef = this.dialog.open(ModalDialogComponent, {
	      disableClose: true,
	      width: '50%',
	      data: ModelData,
	      panelClass: 'background_modal'
	    });
	}

	openAnnouncementWidgetDialog(): void {
  		let	ModelData = {
	      	widget_type: this.templatedata.widget_type,
	      	limit: this.templatedata.limit,
	      	announcements:this.templatedata.announcements.homepage,
	      	HomeAnnounceId:this.templatedata.HomeAnnounceId
      	};

	    const dialogRef = this.dialog.open(AnnouncementWidgetComponent, {
	      disableClose: true,
	      width: '50%',
	      data: ModelData,
	      panelClass: 'background_modal'
	    });

	    dialogRef.afterClosed().subscribe(result => {
	      if(result){
	      	this.templatedata.widget_type = result.widgetType;
	      	this.templatedata.limit = result.widgetLimit;
	      	this.templatedata.HomeAnnounceId = result.HomeAnnounceId;
	      	this.ngOnChanges();
	      }
	    });
	}

	//Set Background Image SafeUrl
    getBackground(image) {
	    return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
	}

  	ngAfterViewInit() {
  		this.tmplformInfo.append('SettingsJson', JSON.stringify(this.templatedata));
	    this.sharedService.SharedData.emit(this.tmplformInfo);

	    /*this.editorBlock = document.getElementsByClassName('printable_div');
	    this.scaleBlock  = document.getElementsByClassName('printable_div_trans');
	    
	    if(this.editorBlock[0]){
			this.editorWidth = this.editorBlock[0].clientWidth;
			this.editorHeight = this.editorBlock[0].clientHeight;
			this.scaleCount = Math.min(this.editorWidth / 640, this.editorHeight / 360);
	    }

	    if(this.scaleCount)
	    {
	    	for (var i = this.editorBlock.length - 1; i >= 0; i--) {
	    		this.scaleBlock[i].style.transform = 'translate(-50%, -50%) scale('+this.scaleCount+')';
	    	} 	

	    }*/
  	}
  	ngOnChanges() {
  		// changes.prop contains the old and the new value...
  		//delete(this.templatedata.announcements);
	    this.tmplformInfo.append('SettingsJson', JSON.stringify(this.templatedata));
	    this.sharedService.SharedData.emit(this.tmplformInfo);
	}

	ngOnInit () {
		this.screenHeight = window.innerHeight;
		this.screenWidth = window.innerWidth;
		if ( this.screenHeight > this.screenWidth ) {
			this.moreHeight = true;
		} else {
			this.moreHeight = false;
		}

		if(this.type != 'print'){
		  	this.subscription = this.sharedService.SharedData.subscribe(
	          (data: any) => {	            
	            this.sharedService.sendSharedJson(data).subscribe(
	          		(result: any) => {
	          			this.templatedata.styled = result.settings.styled;
	          			switch (result.settings.column) {
	          				case "left":
	          					this.background.leftbgimage					= result.base64image;
	            				this.templatedata.leftcolsettings.bgtype	= result.settings.bgtype;
	            				this.templatedata.leftcolsettings.bgcolor	= result.settings.bgcolor;
	            				this.templatedata.leftcolsettings.bgrepeat	= result.settings.bgrepeat;
	            				this.templatedata.leftcolsettings.bgsize	= result.settings.bgsize;
	            				this.templatedata.leftcolsettings.bgposition= result.settings.bgposition;
	            				this.templatedata.leftcolsettings.txtcolor	= result.settings.txtcolor;
	            				this.templatedata.leftcolsettings.bgimage	= result.settings.bgimage;
	          					this.templatedata.leftcolimage				= result.bgimage;
	          					break;
	          				case "right":
	          					this.background.rightbgimage				= result.base64image;
	            				this.templatedata.rightcolsettings.bgtype	= result.settings.bgtype;
	            				this.templatedata.rightcolsettings.bgcolor	= result.settings.bgcolor;
	            				this.templatedata.rightcolsettings.bgrepeat	= result.settings.bgrepeat;
	            				this.templatedata.rightcolsettings.bgsize	= result.settings.bgsize;
	            				this.templatedata.rightcolsettings.bgposition= result.settings.bgposition;
	            				this.templatedata.rightcolsettings.txtcolor	= result.settings.txtcolor;
	            				this.templatedata.rightcolsettings.bgimage	= result.settings.bgimage;
	          					this.templatedata.rightcolimage				= result.bgimage;
	          					break;
	          				default:
	          					break;
	          			}
	          	});
	        });
	  		this.sharedService.SlideData.emit(this.templatedata);
	  	}
	}

	ngOnDestroy() {
	  if(this.type != 'print'){
  		this.subscription.unsubscribe();
  	  }
	}

}
