import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmplEventWidgetComponent } from './tmpl-event-widget.component';

describe('TmplEventWidgetComponent', () => {
  let component: TmplEventWidgetComponent;
  let fixture: ComponentFixture<TmplEventWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmplEventWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmplEventWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
