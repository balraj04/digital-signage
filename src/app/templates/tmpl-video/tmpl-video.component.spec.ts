import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmplVideoComponent } from './tmpl-video.component';

describe('TmplVideoComponent', () => {
  let component: TmplVideoComponent;
  let fixture: ComponentFixture<TmplVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmplVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmplVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
