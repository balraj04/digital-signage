import { Component, OnInit, AfterViewInit, OnDestroy, Inject, Input, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { ISubscription } from "rxjs/Subscription";

import { ModelPdfPptComponent } from './../../modal-dialog/model-pdf-ppt/model-pdf-ppt.component';

import { SharedService } from './../../services/shared.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-tmpl-video',
  templateUrl: './tmpl-video.component.html',
  styleUrls: ['./tmpl-video.component.css']
})
export class TmplVideoComponent implements OnInit, AfterViewInit, OnDestroy {

	@ViewChild('videoPlayer') videoplayer: ElementRef;

	public blurDefault = environment.blurDefault;
	public offset = environment.imgoffset;
	public lefttransvisibility = 'hidden';
	public righttransvisibility = 'hidden';
	private subscription: ISubscription;
	private editMode: Boolean = false;
	private tmplformInfo = new FormData();

	private templatedata : any = {
		url:"",
		video_type:"video"
	};

	mediaUrl = environment.mediaUrl;
	dateTemp : any = Math.random();

	playVideoUrl : any = '';

	@Input() type: string = 'add';
	@Input() is_valide: string = 'yes';
	@Input() community_footer: any;
	@Input() scroll_announcement: any;
	
	screenHeight: any;
    screenWidth: any;
    moreHeight: boolean;
    scaleCount:any; 
    translateCount:any;
    editorBlockStyle: any = {};
    editorBlock: any;
    editorHeight: any;
    editorWidth: any;
    scaleBlock: any;

  	constructor(
  		private sharedService: SharedService, 
  		public dialog: MatDialog, 
  		private _sanitizer: DomSanitizer,
  		private router: Router,
  		private route: ActivatedRoute ){
  		if (this.router.url.includes("/edit-slide/")) {
  			this.editMode = true;
  			this.sharedService.currEditData.subscribe( editData => {	  			
	  			this.templatedata = editData ? editData : this.templatedata;			
	  		});
	  	}
  	}

  	openDialog(coltype:String="",isText=true): void {
  		let	ModelData = {
	      	column: coltype,
	      	modal_type: 'video_options',
	      	uploadedfile: (this.templatedata.url) ? this.templatedata.url : '',
	      	videoIframe: (this.templatedata.video_type) ? this.templatedata.video_type : ''
	    };

	    const dialogRef = this.dialog.open(ModelPdfPptComponent, {
	      disableClose: true,
	      width: '50%',
	      data: ModelData,
	      panelClass: 'background_modal'
	    });

	    dialogRef.afterClosed().subscribe(result => {
	      if(result){
	      	this.templatedata.video_type = result.videoIframe;
	      	let fileName = result.fileName;
	      	if (fileName !== '') {
	      		if (result.videoIframe === 'video') {
		      		this.templatedata.url = fileName;
		      		this.playVideoUrl = this._sanitizer.bypassSecurityTrustResourceUrl(this.mediaUrl + fileName);
		  		}
	      	}
	      	else{
	  			this.templatedata.url = '';
	  			this.playVideoUrl = this._sanitizer.bypassSecurityTrustResourceUrl('');
	  		}
	      }
	    });
	}

  	ngAfterViewInit() {
  		this.tmplformInfo.append('SettingsJson', JSON.stringify(this.templatedata));
	    this.sharedService.SharedData.emit(this.tmplformInfo);

	    /*this.editorBlock = document.getElementsByClassName('printable_div');
	    this.scaleBlock  = document.getElementsByClassName('printable_div_trans');
	    
	    if(this.editorBlock[0]){
			this.editorWidth = this.editorBlock[0].clientWidth;
			this.editorHeight = this.editorBlock[0].clientHeight;
			this.scaleCount = Math.min(this.editorWidth / 640, this.editorHeight / 360);
	    }

	    if(this.scaleCount)
	    {
	    	for (var i = this.editorBlock.length - 1; i >= 0; i--) {
	    		this.scaleBlock[i].style.transform = 'translate(-50%, -50%) scale('+this.scaleCount+')';
	    	} 	

	    }*/
  	}

  	ngOnChanges() {
  		// changes.prop contains the old and the new value...
	    this.tmplformInfo.append('SettingsJson', JSON.stringify(this.templatedata));
	    this.sharedService.SharedData.emit(this.tmplformInfo);
	}

	ngOnInit () {
		this.screenHeight = window.innerHeight;
		this.screenWidth = window.innerWidth;
		if ( this.screenHeight > this.screenWidth ) {
			this.moreHeight = true;
		} else {
			this.moreHeight = false;
		}

		if(this.type != 'print'){
		  	this.subscription = this.sharedService.SharedData.subscribe(
	          (data: any) => {        
	            this.sharedService.sendSharedJson(data).subscribe(
	          		(result: any) => {
	          			this.templatedata.url 		 = result.settings.url;
	          			this.templatedata.video_type = result.settings.video_type;
	      			
	      				this.sharedService.SlideData.emit(this.templatedata);
	          	});
	        });
	  	}

		this.trustURL();
	}

	trustURL(){
		if(this.templatedata.url){
			if(this.templatedata.video_type == 'video'){
	  			this.playVideoUrl = this._sanitizer.bypassSecurityTrustResourceUrl(this.mediaUrl + this.templatedata.url);
	  		}
		}
		else{
			this.playVideoUrl = this._sanitizer.bypassSecurityTrustResourceUrl('');
		}
	}

	onMetadata(e, video) {
		this.templatedata.duration = video.duration;
		this.templatedata.duration += 2;
	}

	ngOnDestroy() {
		if(this.type != 'print'){
	  		this.subscription.unsubscribe();
	  	}
	}
}