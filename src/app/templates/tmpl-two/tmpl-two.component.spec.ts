import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmplTwoComponent } from './tmpl-two.component';

describe('TmplTwoComponent', () => {
  let component: TmplTwoComponent;
  let fixture: ComponentFixture<TmplTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmplTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmplTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
