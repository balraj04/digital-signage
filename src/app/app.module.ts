import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatDialogModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

import { AppComponent } from './app.component';
import { UiModule } from './ui/ui.module';
import { TinyMceModule } from './modal-dialog/tiny-mce/tiny-mce.module';
import { LoaderInterceptor, CommonBridgeService, SharedService } from './services/index';


@NgModule({
  declarations: [
    AppComponent,    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    UiModule,
    TinyMceModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },CommonBridgeService,SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
