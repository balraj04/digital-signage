'use strict';
import { TmplOneComponent } from './templates/tmpl-one/tmpl-one.component';
import { TmplTwoComponent } from './templates/tmpl-two/tmpl-two.component';
import { TmplThreeComponent } from './templates/tmpl-three/tmpl-three.component';
import { TmplFourComponent } from './templates/tmpl-four/tmpl-four.component';
import { TmplFiveComponent } from './templates/tmpl-five/tmpl-five.component';
import { TmplSixComponent } from './templates/tmpl-six/tmpl-six.component';
import { TmplSevenComponent } from './templates/tmpl-seven/tmpl-seven.component';
import { TmplEventComponent } from './templates/tmpl-event/tmpl-event.component';
import { TmplPdfComponent } from './templates/tmpl-pdf/tmpl-pdf.component';
import { TmplVideoComponent } from './templates/tmpl-video/tmpl-video.component';
import { TmplYoutubeComponent } from './templates/tmpl-youtube/tmpl-youtube.component';
import { TmplEventWidgetComponent } from './templates/tmpl-event-widget/tmpl-event-widget.component';
import { TmplAnnouncementWidgetComponent } from './templates/tmpl-announcement-widget/tmpl-announcement-widget.component';

import { TmplEightComponent } from './templates/tmpl-eight/tmpl-eight.component';
import { TmplNineComponent } from './templates/tmpl-nine/tmpl-nine.component';

export const TEMPLATES_CONST = {
    'tmpl-one': TmplOneComponent,
    'tmpl-two':TmplTwoComponent,
    'tmpl-three':TmplThreeComponent,
    'tmpl-four':TmplFourComponent,
    'tmpl-five':TmplFiveComponent,
    'tmpl-six':TmplSixComponent,
    'tmpl-seven':TmplSevenComponent,
    'tmpl-event': TmplEventComponent,
    'tmpl-pdf': TmplPdfComponent,
    'tmpl-video': TmplVideoComponent,
    'tmpl-youtube': TmplYoutubeComponent,
    'tmpl-event-widget': TmplEventWidgetComponent,
    'tmpl-announcement-widget': TmplAnnouncementWidgetComponent,
    'tmpl-eight':TmplEightComponent,
    'tmpl-nine':TmplNineComponent,
};


export const TEMPLATES_NAMES = [
    {'value': 'tmpl-one', 'name': 'Template One'},
    {'value': 'tmpl-two', 'name': 'Template Two'},
    {'value': 'tmpl-three', 'name': 'Template Three'},
    {'value': 'tmpl-four', 'name': 'Template Four'},
    {'value': 'tmpl-five', 'name': 'Template Five'},
    {'value': 'tmpl-six', 'name': 'Template Six'},
    {'value': 'tmpl-seven', 'name': 'Template Seven'},
    {'value': 'tmpl-event', 'name': 'Template Event'},
    {'value': 'tmpl-pdf', 'name': 'Template Pdf'},
    {'value': 'tmpl-video', 'name': 'Template Video'},
    {'value': 'tmpl-youtube', 'name': 'Template Youtube'},
    {'value': 'tmpl-event-widget', 'name': 'Template Event Widget'},
    {'value': 'tmpl-announcement-widget', 'name': 'Template Announcement Widget'},
    {'value': 'tmpl-eight', 'name': 'Template Eight'},
    {'value': 'tmpl-nine', 'name': 'Template Nine'}
];