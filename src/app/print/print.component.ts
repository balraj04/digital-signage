import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, PageEvent } from '@angular/material';
import { PrintService, CommonBridgeService } from './../services/index';
import { environment } from '../../environments/environment';
import { PrintDialogComponent } from './print-dialog/print-dialog.component';
import { TempWeatherInfo, TempAnnouncementInfo } from './../interfaces/index';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';

// const LIMIT = environment.limit;
const LIMIT = 1000;

@Component({
  selector: 'app-print',
  templateUrl: './print.component.html',
  styleUrls: ['./print.component.css']
})
export class PrintComponent implements OnInit {
  public blurDefault = environment.blurDefault;
  public offset = environment.imgoffset;
	title: string;
	slide_thumb: string;
  selected_playlist_ids = [];

  playlists: [];
  errorMsg: any;

  // MatPaginator Inputs
  length: number = 0;
  pageSize = LIMIT;
  playlistPage: number = 1;
  slidePage: number = 1;

  // MatPaginator Output
  pageEvent: PageEvent;

  slides: Slide[];
  footer_text: any;
  scroll_announcement_status: string = 'N';
  scroll_announcement_text: any;
  footer_text_status: any;

  mediaUrl = environment.mediaUrl;
  noMedia = environment.noimageDefault;
  dateTemp : any = Math.random();

  tempWeather: TempWeatherInfo = {
    city_name: '',
    temp: 0,
    logo: '',
    footerColor: '',
    footerBgcolor: '',
    custom_city_name: ''
  };

  tempAnnouncement: TempAnnouncementInfo = {
    text: '',
    scrolldelay: 0,
    announcementColor: '',
    announcementBgcolor: ''
  };

  // Snackbar Posittion
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private printService: PrintService,
    private commonBridgeService: CommonBridgeService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar) {
  	this.title = 'Print';
  	this.slide_thumb = environment.noimageDefault;
  }

  openPrintDialog(): void {

    let tmp_print = [];
    let printContents;
    let filtered = multiFilter(this.slides, this.selected_playlist_ids);
    
    filtered.forEach(function (temp) {
      tmp_print.push(temp['template_id']);
    });
    let infos = this.printService.getComponents(filtered);
    if(infos.length){
      for (var i = infos.length - 1; i >= 0; i--) {
        this.tempAnnouncement.text = (this.scroll_announcement_status == 'Y') ? this.scroll_announcement_text.text : '';
        infos[i].content_data['community_footer'] = {'status': this.footer_text_status, 'content': this.tempWeather};
        infos[i].content_data['scroll_announcement'] = {'status': this.scroll_announcement_status, 'content': this.tempAnnouncement};
      }
      
      const dialogRef = this.dialog.open(PrintDialogComponent, {
        width: '50%',
        data: { infos: infos },
        panelClass: 'print_modal'
      });
    }
    else{
      this.snackBar.open('Selected slide is temporarily hidden', 'Error', {
        duration: 4000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        panelClass: ['cust-snackbar', 'not-submitted'],

      });
    }
  }

  ngOnInit() {
    this.getPlaylists(this.playlistPage, LIMIT);
  }

  getNext(event: PageEvent) {
    this.selected_playlist_ids = [];
    this.slides = [];
    this.playlists = [];
    this.playlistPage = event.pageIndex + 1;
    this.getPlaylists(this.playlistPage, LIMIT);
  }

  getPlaylists(page, limit){
    this.commonBridgeService.getPlaylist(page.toString(), limit.toString())
      .subscribe(response =>
                            {
                              if(response.message == 'success'){
                                this.playlists = response.playlists.data;
                                this.length = response.playlists.total;
                              }
                            },
                error => this.errorMsg = error);
  }

  isChecked() {
    return this.selected_playlist_ids.length === this.slides.length;
  }

  isIndeterminate() {
    return (this.selected_playlist_ids.length !== 0 && this.selected_playlist_ids.length !== this.slides.length);
  }

  selectBadge(e, sel_id) {
   if (e.checked) {
     this.selected_playlist_ids.push(sel_id);
   }  else {
     this.selected_playlist_ids.splice(this.selected_playlist_ids.indexOf(sel_id), 1);
   }
  }

  exists(curr_slide){
    return this.selected_playlist_ids.indexOf(curr_slide) > -1;
  }

  selectAllBadge(e) {
   this.selected_playlist_ids = [];
   if (e.checked) {
     this.slides.forEach(y => this.selected_playlist_ids.push(y.slide_id));
   }
  }

  getSlides(playlist_id, footer_text_status, scroll_announcement_status){
    this.footer_text_status = footer_text_status;
    this.scroll_announcement_status = scroll_announcement_status;
    this.selected_playlist_ids = [];
    this.commonBridgeService.getSlidesList(playlist_id.toString(), this.slidePage.toString(), LIMIT.toString())
      .subscribe(response =>
                            {
                              if(response.status === 200){
                                this.scroll_announcement_text = (response.announcements) ? response.announcements : '';
                                this.slides = response.slides.data;
                                this.configComponent(footer_text_status, scroll_announcement_status);
                              }
                            },
                error => this.errorMsg = error);
  }

  configComponent(footer_text_status, scroll_announcement_status){
    if(footer_text_status == 'Y' || scroll_announcement_status == 'Y'){
      this.getFooterSettings();
    }
  }

  getFooterSettings(){
    this.commonBridgeService.getSettings()
      .subscribe(response =>
                            {
                              if(response.status == 200){
                                if(response.settings[0]){
                                  if(response.settings[0].footer_logo){
                                    this.tempWeather.logo = response.settings[0].footer_logo;
                                  }
                                  if(response.settings[0].footerColor){
                                    this.tempWeather.footerColor = response.settings[0].footerColor;
                                  }
                                  if(response.settings[0].footerBgcolor){
                                    this.tempWeather.footerBgcolor = response.settings[0].footerBgcolor;
                                  }
                                  if(response.settings[0].announcementColor){
                                    this.tempAnnouncement.announcementColor = response.settings[0].announcementColor;
                                  }
                                  if(response.settings[0].announcementBgcolor){
                                    this.tempAnnouncement.announcementBgcolor = response.settings[0].announcementBgcolor;
                                  }
                                  if(response.settings[0].scrolldelay){
                                    this.tempAnnouncement.scrolldelay = response.settings[0].scrolldelay;
                                  }
                                  if(response.settings[0].city_name){
                                    this.tempWeather.custom_city_name = response.settings[0].city_name;
                                  }
                                  if(response.settings[0].app_id && response.settings[0].city_id){
                                    this.getWeatherInfo(response.settings[0].app_id, response.settings[0].city_id, response.settings[0].weatherUnit);
                                  }
                                }
                              }
                            },
                error => this.errorMsg = error);
  }

  getWeatherInfo(app_id, city_id, unit){
    this.commonBridgeService.getWeatherInfo(app_id, city_id, unit)
      .subscribe(response =>
                            {
                              if(response.cod == 200){
                                this.tempWeather.temp = response.main.temp;
                                this.tempWeather.city_name = response.name;
                              }
                            },
                error => this.errorMsg = error);
  }

  clearSlides(){
    this.slides = [];
  }

  trackByPlaylist(index: number, playlist: any): number {
    return playlist.playlist_id;
  }

  trackBySlide(index: number, playlist: any): number {
    return playlist.slide_id;
  }
}

export interface Slide {
  slide_id: number;
  title: string;
  thumb_img: string;
  content: string;
}

export const multiFilter = (arr: Object[], filters) => {
  return arr.filter(eachObj => {
      if (!filters.length) {
        return true;
      }
      return filters.includes(eachObj['slide_id']);
  });
};