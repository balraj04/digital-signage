import { Component, OnInit, Inject, ViewChild, ViewContainerRef, ComponentFactoryResolver, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TemplatesComponent } from '../../templates/templates';
import { PrintService } from '../../services/print.service';
import * as html2canvas from 'html2canvas';

@Component({
  selector: 'app-print-dialog',
  templateUrl: './print-dialog.component.html',
  styleUrls: ['./print-dialog.component.css']
})
export class PrintDialogComponent implements OnInit {

	@ViewChild('processContainer', { read: ViewContainerRef }) container;
  @ViewChild('screen') screen: ElementRef;
  screenHeight: any;
	printData: DialogData;
  // Footer and Announcement Flags
  hasFooter: boolean = false;
  hasAnnouncement: boolean = false;

  editorBlock: any;
  editorHeight: any;
  editorWidth: any;
  scaleBlock: any;
  scaleCount:any;

  constructor(public dialogRef: MatDialogRef<PrintDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: DialogData, 
    private resolver: ComponentFactoryResolver, 
    private printService: PrintService) {
      this.printData = data;
  }

  ngOnInit() {
    this.screenHeight = window.innerHeight;
  	this.container.clear();
  	for (let info of this.printData.infos) {
      let tempContent = (info.content_data.content) ? {...(JSON.parse(info.content_data.content))} : {};
      let tmpIsValide = (info.content_data.content) ? 'yes' : 'no'
      if (info.content_data.template_id == 'tmpl-event') {
        if (tempContent.event_id || tempContent.title) {
          tmpIsValide = 'yes';
          tempContent['event_start_date'] = tempContent.event_start_date;
          tempContent['event_start_time'] = tempContent.event_start_time;
          tempContent['event_end_date'] = tempContent.event_end_date;
          tempContent['event_end_time'] = tempContent.event_end_time;
          tempContent['event_location'] = tempContent.event_location;
          tempContent['event_title'] = tempContent.event_title;
          tempContent['event_desc'] = tempContent.event_desc;
          tempContent['event_id'] = tempContent.event_id;
          tempContent['styled']= false;
        }
        else {
          tmpIsValide = 'yes';
          tempContent['event_start_date'] = info.content_data.event_start_date;
          tempContent['event_start_time'] = info.content_data.event_start_time;
          tempContent['event_end_date'] = info.content_data.event_end_date;
          tempContent['event_end_time'] = info.content_data.event_end_time;
          tempContent['event_location'] = info.content_data.event_location;
          tempContent['event_title'] = info.content_data.event_title;
          tempContent['event_desc'] = info.content_data.event_desc;
          tempContent['event_id'] = info.content_data.event_id;
          tempContent['styled']= false;
          tempContent['leftcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#fafafa", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
          tempContent['rightcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#cacaca", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
          tempContent['leftcolimage'] = "";
          tempContent['rightcolimage'] = "";
        }        
      }
      else if (info.content_data.template_id == 'tmpl-event-widget') {
        if (tempContent) {
          if(info.content_data.eventslist){
            tempContent['eventslist'] = info.content_data.eventslist;
          }
          if(tempContent.widget_type){
            tempContent['widget_type'] = tempContent.widget_type;
          }
          tempContent['leftcolimage'] = (info.content_data.content.leftcolimage != null && info.content_data.content.leftcolimage != undefined) ? info.content_data.content.leftcolimage : '';
          tempContent['rightcolimage'] = (info.content_data.content.rightcolimage != null && info.content_data.content.rightcolimage != undefined) ? info.content_data.content.rightcolimage : '';
        }
        else {
          tempContent = {};
          tempContent['eventslist'] = [];
          tempContent['styled']= false;
          tempContent['leftcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#fafafa", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
          tempContent['rightcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#cacaca", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
          tempContent['leftcolimage'] = "";
          tempContent['rightcolimage'] = "";
        } 
      }
      else if (info.content_data.template_id == 'tmpl-announcement-widget') {
        if (tempContent) {
         let announInfo = info.content_data.content ? JSON.parse(info.content_data.content) : [];
          tempContent['announcements'] = announInfo.announcements ? announInfo.announcements : [];
          tempContent['leftcolimage'] = (info.content_data.content.leftcolimage != null && info.content_data.content.leftcolimage != undefined) ? info.content_data.content.leftcolimage : '';
          tempContent['rightcolimage'] = (info.content_data.content.rightcolimage != null && info.content_data.content.rightcolimage != undefined) ? info.content_data.content.rightcolimage : '';
        }
        else {
          tempContent = {};
          tempContent['announcements'] = [];
          tempContent['styled']= false;
          tempContent['leftcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#fafafa", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
          tempContent['rightcolsettings'] = { txtcolor: "#000", bgtype: "bgcolor", bgcolor: "#cacaca", bgimage: "", bgrepeat: "no-repeat", bgsize: "auto", bgposition: "top left" };
          tempContent['leftcolimage'] = "";
          tempContent['rightcolimage'] = "";
        } 
      }

      const factory = this.resolver.resolveComponentFactory(info.component);
      let componentRef = this.container.createComponent(factory);      
      let content = tempContent;
      let is_valide = tmpIsValide;
      (<TemplatesComponent>componentRef.instance).templatedata = content;
      (<TemplatesComponent>componentRef.instance).is_valide = is_valide;
      (<TemplatesComponent>componentRef.instance).type = 'print';
      (<TemplatesComponent>componentRef.instance).community_footer = info.content_data.community_footer;
      (<TemplatesComponent>componentRef.instance).scroll_announcement = info.content_data.scroll_announcement;
    }
  }
  ngAfterViewInit() {
    this.scaleBlock  = document.getElementsByClassName('print_modal2');

    this.editorWidth = window.innerWidth;
    this.editorHeight = window.innerHeight;
    this.scaleCount = Math.min(this.editorWidth / 640, this.editorHeight / 360);
    
    let translateCount = (( this.scaleCount - 1 ) * 50);
    if(this.scaleCount)
    {
      this.scaleBlock[0].style.transform = 'translate(-50%, -50%) scale('+this.scaleCount+')';
    }
  }

  printSlides(){
  	const printContents = document.getElementById("printContents").innerHTML;
  	this.printService.printPages(printContents);
  }

  getCoverImage(){
      return new Promise((resolve, reject) => {
        html2canvas(document.getElementById('printContents'),{allowTaint: true, useCORS:true, foreignObjectRendering:true}).then(canvas => {
          let dataUrl = canvas.toDataURL();
          resolve(dataUrl);
        });        
      });
  }

  onClose(): void {
    this.dialogRef.close();
  }

}

export interface DialogData {
  infos: any;
}