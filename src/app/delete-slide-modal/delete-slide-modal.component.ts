import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-delete-slide-modal',
  templateUrl: './delete-slide-modal.component.html',
  styleUrls: ['./delete-slide-modal.component.css']
})
export class DeleteSlideModalComponent implements OnInit {

  constructor(
  	public dialogRef: MatDialogRef<DeleteSlideModalComponent>) { }

  ngOnInit() {
  }

  onBtnClick(flag): void {
    this.dialogRef.close(flag);
  }

}
