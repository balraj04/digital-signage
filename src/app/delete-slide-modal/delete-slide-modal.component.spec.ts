import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteSlideModalComponent } from './delete-slide-modal.component';

describe('DeleteSlideModalComponent', () => {
  let component: DeleteSlideModalComponent;
  let fixture: ComponentFixture<DeleteSlideModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteSlideModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteSlideModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
