import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ISubscription } from "rxjs/Subscription";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { CommonBridgeService,SharedService } from '../../services/index';
import { PlaylistSection,SlideSection} from '../../interfaces/index';
import { environment } from '../../../environments/environment';
import * as APPCONSTANTS from '../../app.constants';

const LIMIT = environment.limit;

@Component({
  selector: 'app-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.css']
})
export class SearchListComponent implements OnInit {
  
  public SearchForm: FormGroup;
  public subscription:ISubscription;
  playlists: any = {data:[]};
  slides: any = {data:[]};
  playlistdata: [];
  playlistPage: number = 1;
  length: number = 0;
  errorMsg: any;
  min_dt: any;
  max_dt: any;  
  templateName  : any;
  step = 1;

  constructor(
  	private commonBridgeService: CommonBridgeService,
  	private sharedService: SharedService,
  	private router: Router,
    private route : ActivatedRoute,
  	private fb: FormBuilder,
    private _location: Location) { 
    this.templateName = APPCONSTANTS.TEMPLATES_NAMES;
  }

  ngOnInit() {
  	this.SearchForm = this.fb.group({
      searchkey: this.fb.control('',[Validators.required]),
      playlist_id: this.fb.control([]),
      template: this.fb.control([]),
      start_datetime: this.fb.control(''), 
      end_datetime: this.fb.control('')
    });

    this.route.params.subscribe(params => {
  		this.SearchForm.get('searchkey').setValue(params['searchkey']);
  		this.getSearch();
    });
    this.getPlaylists(this.playlistPage, LIMIT);

  }

  // ACCORDIAN OPEN CLOSE EVENT
  setStep(index: number) {   
    if (index === 0) {
      // this.SearchForm.get('playlist_id').setValidators([Validators.required]);
      // this.SearchForm.get('start_datetime').setValidators([Validators.required]);
      // this.SearchForm.get('end_datetime').setValidators([Validators.required]);
    }
    else if (index === 1) {
      // this.SearchForm.get('playlist_id').clearValidators();
      // this.SearchForm.get('start_datetime').clearValidators();
      // this.SearchForm.get('end_datetime').clearValidators();
    }
    // this.SearchForm.get('playlist_id').updateValueAndValidity();
    // this.SearchForm.get('start_datetime').updateValueAndValidity();
    // this.SearchForm.get('end_datetime').updateValueAndValidity();
    this.step = index;
  }

  getPlaylists(page, limit){
    this.commonBridgeService.getPlaylist()
      .subscribe(response =>
                            {
                              if(response.message == 'success'){
                                this.playlistdata = response.playlists.data;
                                this.length = response.playlists.total;
                              }
                            },
                error => this.errorMsg = error);
  }

  changeMinDTime(){
    if(this.SearchForm.controls['start_datetime'].value){
      this.min_dt = this.SearchForm.controls['start_datetime'].value;
    }
  }

  changeMaxDTime(){
    if(this.SearchForm.controls['end_datetime'].value){
      this.max_dt = this.SearchForm.controls['end_datetime'].value;      
    }
  }

  getSearch() {
    let startTS = this.dateToTimestamp(this.SearchForm.get('start_datetime').value);
    let endTS = this.dateToTimestamp(this.SearchForm.get('end_datetime').value);

    if (isNaN(startTS)) {
      startTS = 0;
    }
    if (isNaN(endTS)) {
      endTS = 0;
    }

  	this.subscription = this.commonBridgeService.getSearchResults(this.SearchForm.get('searchkey').value, 
      this.SearchForm.get('playlist_id').value, this.SearchForm.get('template').value,
      startTS.toString(), endTS.toString()).subscribe(response =>{
      if(response.message == 'success'){
        this.playlists = response.playlists;
        this.slides = response.slides;

      }
    },
    error => console.log(error) );
  }

  dateToTimestamp(dt){ 
    let millitime = new Date(dt).setSeconds(0);
    return millitime;  
  }

  backClicked() {
    this._location.back();
  }
  trackByPlaylist(index: number, playlist: any): number {
    return playlist.playlist_id;
  }
  trackBySlide(index: number, playlist: any): number {
    return playlist.slide_id;
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
