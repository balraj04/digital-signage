import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-global-search',
  templateUrl: './global-search.component.html',
  styleUrls: ['./global-search.component.css']
})
export class GlobalSearchComponent implements OnInit {
  
  public GlobalForm: FormGroup;

  constructor(
  	private router: Router,
  	private route : ActivatedRoute,
  	private fb: FormBuilder
  	) { }

  ngOnInit() {

  	this.GlobalForm = this.fb.group({
      searchkey: this.fb.control('',[Validators.required])
    });

  }

  gotoSearch() {

  	this.router.navigate(['/search', {searchkey:this.GlobalForm.get('searchkey').value}]);
  }

}
