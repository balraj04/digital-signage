// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // URL of development API
  apiUrl: 'http://croasdailevillageportal.com/digisign/ds-api/api/',
  mediaUrl: 'http://croasdailevillageportal.com/digisign/ds-api/',
  limit: 12,
  weatherUrl: 'http://api.openweathermap.org/data/2.5/weather/',
  spDefaultLogo: 'assets/img/sp-logo.png',
  blurDefault: 'assets/img/loading.gif',
  noimageDefault: 'assets/img/default-blur.png',
  imgoffset: 0,
  tinyMceKey:"mfts9k5pj8sn27exiehvkkyr13crnzs3ydjno1f99bghorvh",
  tinyMceCss: "http://croasdailevillageportal.com/digisign/tinymce.css",
  portalDashboard: 'http://croasdailevillageportal.com/wp-admin/',
  playlistURL: 'http://croasdailevillageportal.com/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
