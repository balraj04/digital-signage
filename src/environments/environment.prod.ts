export const environment = {
  production: true, 
  apiUrl: 'https://croasdailevillageportal.com/digisign/ds-api/api/',
  mediaUrl: 'https://croasdailevillageportal.com/digisign/ds-api/',
  limit: 12,
  weatherUrl: 'https://api.openweathermap.org/data/2.5/weather/',
  spDefaultLogo: 'assets/img/sp-logo.png',
  blurDefault: 'assets/img/loading.gif',
  noimageDefault: 'assets/img/default-blur.png',
  imgoffset: 100,
  tinyMceKey:"mfts9k5pj8sn27exiehvkkyr13crnzs3ydjno1f99bghorvh",
  tinyMceCss: "https://croasdailevillageportal.com/digisign/tinymce.css",
  portalDashboard: 'https://croasdailevillageportal.com/wp-admin/',
  playlistURL: 'https://croasdailevillageportal.com/'
};